@extends('layouts.app')
@section('content')
	<section class="page-title">
		<div class="container-1265">
			<div class="inner-page-title">
				<h2>{{ __('Login') }}</h2>
			</div>
		</div>
	</section>
	<section class="form-section login-section">
		<div class="container">
			<div class="form-box-inner max-555">
				{!! Form::open(['route' => 'login']) !!}
				<div class="form-grouph input-design{!! ($errors->has('email') ? ' has-error' : '') !!}">
					{!! Form::email('email', request()->email ?? null, ['class' => 'form-control'.($errors->has('email') ? ' is-invalid' : ''), 'placeholder' => 'Email']) !!}
					{!! $errors->first('email', '<span class="help-block">:message</span>') !!}
				</div>
				<div class="form-grouph input-design{!! ($errors->has('password') ? ' has-error' : '') !!}">
					<input id="password" type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password">
					{!! $errors->first('password', '<span class="help-block">:message</span>') !!}
				</div>
				@if (Route::has('password.request'))
				<div class="form-grouph forget-password text-right">
					<a href="{{ route('password.request') }}">
						{{ __('Forgot Password?') }}
					</a>
				</div>
				@endif
				
				<div class="form-grouph submit-design">
					<button class="submit-btn" type="submit">{{__ ('LOGIN') }}</button>
				</div>
				@if (Route::has('register'))
				<div class="form-grouph signup-txt text-center">
					<p>{{ __('New to Public Domain Marching?') }} 
						<a href="{{ route('register') }}">
							{{ __('Sign Up') }}
						</a>
					</p>
				</div>
				@endif
				
				{!! Form::close() !!}
			</div>
		</div>
	</section>
</main>
@endsection