@extends('layouts.app')
@section('content')
	<section class="page-title">
		<div class="container-1265">
			<div class="inner-page-title">
				<h2>{{ __ ('Create an Account') }}</h2>
			</div>
		</div>
	</section>
	<section class="form-section creat-account-section">
		<div class="container">
			<div class="form-box-inner max-555">
				{!! Form::open(['route' => 'register', 'method'=>'POST']) !!}
				<div class="form-grouph input-design{!! ($errors->has('first_name') ? ' has-error' : '') !!}">
					{!! Form::text('first_name', null, ['class' => 'form-control'.($errors->has('first_name') ? ' is-invalid' : '') , 'placeholder'=>'First Name' ]) !!}
					{!! $errors->first('first_name', '<span class="help-block">:message</span>') !!}
				</div>
				<div class="form-grouph input-design{!! ($errors->has('last_name') ? ' has-error' : '') !!}">
					{!! Form::text('last_name', null, ['class' => 'form-control'.($errors->has('last_name') ? ' is-invalid' : '') , 'placeholder'=>'Last Name' ]) !!}
					{!! $errors->first('last_name', '<span class="help-block">:message</span>') !!}
				</div>
				<div class="form-flex">
					<div class="form-grouph input-design{!! ($errors->has('zipcode') ? ' has-error' : '') !!}">
						{!! Form::text('zipcode', null, ['class' => 'form-control'.($errors->has('zipcode') ? ' is-invalid' : '') , 'placeholder'=>'Zip Code' ]) !!}
						{!! $errors->first('zipcode', '<span class="help-block">:message</span>') !!}
					</div>
					<div class="form-grouph input-design{!! ($errors->has('school') ? ' has-error' : '') !!}">
						{!! Form::text('school', null, ['class' => 'form-control'.($errors->has('school') ? ' is-invalid' : '') , 'placeholder'=>'School' ]) !!}
						{!! $errors->first('school', '<span class="help-block">:message</span>') !!}
					</div>
				</div>
				<div class="form-grouph input-design{!! ($errors->has('email') ? ' has-error' : '') !!}">
					{!! Form::email('email', null, ['class' => 'form-control'.($errors->has('email') ? ' is-invalid' : '') , 'placeholder'=>'Email' ]) !!}
					{!! $errors->first('email', '<span class="help-block">:message</span>') !!}
				</div>
				<div class="form-grouph input-design{!! ($errors->has('phone_number') ? ' has-error' : '') !!}">
					{!! Form::number('phone_number', null, ['class' => 'form-control'.($errors->has('phone_number') ? ' is-invalid' : '') , 'placeholder'=>'Phone Number' ]) !!}
					{!! $errors->first('phone_number', '<span class="help-block">:message</span>') !!}
				</div>
				<div class="form-grouph input-design{!! ($errors->has('password') ? ' has-error' : '') !!}">
					<input type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password">
					{!! $errors->first('password', '<span class="help-block">:message</span>') !!}
					
				</div>
				<div class="form-grouph input-design">
					<input id="password-confirm" type="password" class="form-control" name="password_confirmation" autocomplete="new-password" placeholder="Confirm Password">
				</div>
				<div class="form-grouph checkbox-design">
					<input type="checkbox" required>
					<label>By checking this box and creating an account you agree to all Terms and Conditions by Public Domain Marching. </label>
				</div>
				<div class="form-grouph submit-design">
					<button class="submit-btn" type="submit">{{ __('CREATE ACCOUNT') }}</button>
				</div>
				<div class="form-grouph signup-txt text-center">
					<p>{{ __('Already have an account.') }} <a href="{{ route('login') }}">{{ __('Login') }}</a></p>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</section>
</main>	
@endsection
