@extends('layouts.app')

@section('content')
<main id="inner-body-content" class="padd-t-142 padd-b-60">
	<section class="page-title">
		<div class="container-1265">
			<div class="inner-page-title">
				<h2>{{ __('Reset Password') }}</h2>
			</div>
		</div>
	</section>
	<section class="form-section login-section">
		<div class="container">
			<div class="form-box-inner max-555">
			{!! Form::open(['route' => 'password.email']) !!}
				<div class="form-grouph input-design form-field{!! ($errors->has('email') ? ' has-error' : '') !!}">
					{!! Form::email('email', null, ['class' => 'form-control'.($errors->has('email') ? ' is-invalid' : ''),  'placeholder' => 'E-Mail Address' ]) !!}
					{!! $errors->first('email', '<span class="help-block">:message</span>') !!}
				</div>
				
				<div class="form-grouph submit-design text-center">
					<button class="submit-btn" type="submit">{{ __('Reset Password') }}</button>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</section>
</main>
@endsection