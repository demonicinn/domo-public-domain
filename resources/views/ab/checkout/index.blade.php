@extends('layouts.app')
@section('content')
<section class="page-title">
	<div class="container-1265">
		<div class="inner-page-title">
			<h2>Checkout</h2>
		</div>
	</div>
</section>
<section class="form-section edit-arrangements-section checkout-index">
	<div class="container-1265">	
		@if(!auth()->user())
			@livewire('auth.login')
		@endif
		@livewire('checkout-action')
	</div>
</section>
@endsection

@section('script')
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<script>
	
	var year = "{{date('Y')}}";
	var month = "{{date('m')}}";
	$('.selectvalue').on('change', function() {
	var exp_month = $('#exp_month').val();
	var exp_year = $('#exp_year').val();
	
	if(exp_month<10){
		exp_month = 0 + exp_month;
	}
	
	if(exp_month <= month){
		$('#exp_year option[value="'+year+'"]').attr('disabled', 'disabled');
	}
	else {
		$('#exp_year option[value="'+year+'"]').removeAttr('disabled', 'disabled');
	}
	
});

</script>

@endsection	