@extends('layouts.app')
@section('content')
<section class="page-title">
	<div class="container-1265">
		<div class="inner-page-title">
			<h2>Cart</h2>
		</div>
	</div>
</section>
<section class="form-section edit-arrangements-section">
	<div class="container-1265">
		@livewire('cart-action')
	</div>
</section>
@endsection