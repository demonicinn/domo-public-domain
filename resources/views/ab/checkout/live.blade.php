<x-app-layout>
  <section class="page-title">
	<div class="container-1265">
		<div class="inner-page-title">
			<h2>Checkout</h2>
		</div>
	</div>
</section>
<section class="form-section edit-arrangements-section">
	<div class="container-1265">
		
		@livewire('checkout-action')
		
	</div>
</section>
</x-app-layout>

@endsection

@section('script')

<script src="https://maps.googleapis.com/maps/api/js?key={{ config('services.google.api') }}&libraries=places"></script>

<script>
	$(document).ready(function() {
		// $("#lat_area").addClass("d-none");
		// $("#long_area").addClass("d-none");
		// $("#address_area").addClass("d-none");
	});
</script>

<script>
	
	google.maps.event.addDomListener(window, 'load', initialize);
	
	function initialize() {
		var input = document.getElementById('autocomplete');
		var autocomplete = new google.maps.places.Autocomplete(input);
		autocomplete.addListener('place_changed', function() {
			var place = autocomplete.getPlace();
			// console.log(place.geometry);
			
			var latitude = place.geometry['location'].lat();
			var longitude = place.geometry['location'].lng();
			$('#latitude').val(latitude);
			$('#longitude').val(longitude);
			$('#address').val(place.formatted_address);
			// $("#lat_area").removeClass("d-none");
			// $("#long_area").removeClass("d-none");
			// $("#address_area").removeClass("d-none");
			
			checkLocation(latitude, longitude)
		});
	}
	
	
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	
	
	function checkLocation(latitude, longitude){	
		$.ajax({
			type:'POST',
			url:"{{ route('payment.ajax') }}",
			data:{latitude:latitude, longitude:longitude},
			success:function(data){
				if(data.success==true){
					$('.response-serach').html(data.html);
				}
			}
		});	
		
	}
	
	$("#submitcode").click(function(e){
		e.preventDefault(); 
		var promocode = $('#code').val();
		$.ajax({
			type:'POST',
			url:"{{ route('checkout.code') }}",
			data:{promocode:promocode},
			success:function(data){
				if(data.type == 'success'){
					  setTimeout(function(){
						   location.reload(); 
					  }, 1000); 
				}
				else{
					$('.invalid-promocode').html(data.message);
				}
			}
		});	
	
	})
	
	$("#removecode").click(function(e){
		e.preventDefault(); 
		var promocode = $('#code').val();
		$.ajax({
			type:'POST',
			url:"{{ route('checkout.remove') }}",
			data:{promocode:promocode},
			success:function(data){
				console.log(data)
			}
		});	
	
	})

	
	
	
	
</script>
@endsection