@extends('layouts.app')
@section('content')
<div class="tab-content padding-top-50">
	<table class="table" style="width:100%">
		<thead>
			<tr>
				<th>DATE</th>
				<th>AMOUNT</th>
				<th>ACTION</th>
			</tr>
		</thead>
		<tbody>
			@foreach($orders as $order)
			<tr>
				<td>
					<a href="{{ route('ab.orders.show', $order->id) }}">
						<span class="purchase">Purchase <span class="display-block">
							{{ date('m-d-Y' , strtotime($order->created_at)) }}	
						</span></span>
					</a>
				</td>
				<td>
					<span class="arrangement-value">${{number_format($order->total_price, 2)}}</span>
				</td>
				
				<td>
					<a class="btn brown-btn mt-2" target="_blank" href="{{ route('ab.purchase_orders.invoice', $order->id) }}">VIEW QUOTE</a>
					
					<a class="btn brown-btn mt-2" target="_blank" href="javascript:void(0)" data-toggle="modal" data-target="#viewInvoice{{$order->id}}">VIEW INVOICE</a>
					
					<div id="viewInvoice{{$order->id}}" class="modal fade" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title">VIEW INVOICE</h4>
								</div>
								{!! Form::open(['route' => ['ab.purchase_orders.invoice', $order->id], 'method'=>'GET']) !!}
								<div class="modal-body">
									<div class="form-grouph input-design {!! ($errors->has('date') ? 'has-error' : '') !!}">
										{!! Form::label('date','Date', ['class' => 'control-label']) !!}
										{!! Form::date('date', null, ['class' => 'form-control' . ($errors->has('date') ? ' is-invalid' : ''), 'required'=>'required' ]) !!}
										{!! $errors->first('date', '<span class="help-block">:message</span>') !!}
									</div>
									<div class="form-grouph input-design {!! ($errors->has('po_number') ? 'has-error' : '') !!}">
										{!! Form::label('po_number','PO Number', ['class' => 'control-label']) !!}
										{!! Form::text('po_number', null, ['class' => 'form-control' . ($errors->has('po_number') ? ' is-invalid' : '') ]) !!}
										{!! $errors->first('po_number', '<span class="help-block">:message</span>') !!}
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									<button type="submit" class="submit-btn">GENERATE</button>
								</div>
								{!! Form::close() !!}
							</div>
							
						</div>
					</div>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	<div class="pagination">
		{{ $orders->links() }}
	</div>
</div>
@endsection