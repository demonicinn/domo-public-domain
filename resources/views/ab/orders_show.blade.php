@extends('layouts.app')
@section('content')
<div class="tab-content padding-top-50">
	<table class="table" style="width:100%">
		<thead>
			<tr>
				<th>DATE</th>
				<th>ARRANGEMENT</th>
				<th>ARRANGEMENT DETAILS</th>
				<th>PURCHASER DETAILS</th>
				<th>ACTION</th>
			</tr>
		</thead>
		<tbody>
			@foreach($checkout->products as $order)
			<tr>
				<td><span class="purchase">Purchase <span class="display-block">
					{{ date('m-d-Y' , strtotime($order->created_at)) }}	
					</span></span>
					<span class="purchase">Expire Date <span class="display-block">
					{{ date('m-d-Y' , strtotime($order->arrangementTo->date)) }}
					</span></span>
				</td>
				<td>
					<span class="arrangement-value">{{@$order->arrangementTo->title}}</span>
					@if($order->arrangementtype=='wind_arrangment')
						<span class="display-block light-txt">Wind Arrangement Only</span>
					@else
						<span class="display-block light-txt">Wind + Percussion Arrangement</span>
					@endif
				</td>
				<td>
					@if(@$order->arrangementTo->arrangementTypeTo->title)
					<div class="Instrumentation"> 
						<span class="Instrumentation-value">Arrangement Type:</span>
						<span class="light-txt">{{$order->arrangementTo->arrangementTypeTo->title}}</span>
					</div>
					@endif
					@if(@$order->arrangementTo->difficultyTo->title)
					<div class="Difficulty"> 
						<span class="difficulty-value">Difficulty:</span>
						<span class="light-txt">{{$order->arrangementTo->difficultyTo->title}}</span>
					</div>
					@endif
					@if(@$order->arrangementTo->instrumentationTo->title)
					<div class="Instrumentation"> 
						<span class="Instrumentation-value">Instrumentation:</span>
						<span class="light-txt">{{$order->arrangementTo->instrumentationTo->title}}</span>
					</div>
					@endif
					@if(@$order->arrangementTo->composerTO->title)
					<div class="Composer"> 
						<span class="Composer-Name">Composer Name:</span>
						<span class="light-txt">{{$order->arrangementTo->composerTO->title}}</span>
					</div>
					@endif
					@if(@$order->arrangementTo->windarrangerTo->title)
					<div class="Arranger"> 
						<span class="Arranger-Name">Wind Arranger Name:</span>
						<span class="light-txt">{{$order->arrangementTo->windarrangerTo->title}}</span>
					</div>
					@endif
					@if(@$order->arrangementTo->percussionarrangerTO->title)
					<div class="Arranger"> 
						<span class="Arranger-Name">Percussio Arranger Name:</span>
						<span class="light-txt">{{$order->arrangementTo->percussionarrangerTO->title}}</span>
					</div>
					@endif
					<div class="Amount"> 
						<span class="Time-value">Amount:</span>
						<span class="light-txt">
						${{$order->price}}	
						</span>
					</div>
				</td>
				<td>
					@if($checkout->user)
					<div class="Name"> 
						<span class="Name-value">Name:</span>
						<span class="light-txt">{{$checkout->user->first_name}} {{$checkout->user->last_name}}</span>
					</div>
					<div class="School"> 
						<span class="School-value">School:</span>
						<span class="light-txt">{{$checkout->user->school}}</span>
					</div>
					<div class="Zip-Code"> 
						<span class="Zip-Name">Zip Code:</span>
						<span class="light-txt">{{$checkout->zipcode}}</span>
					</div>
					<div class="Email"> 
						<span class="Email-Name">Email:</span>
						<span class="light-txt">{{$checkout->user->email}}</span>
					</div>
					<div class="Phone-Number"> 
						<span class="phone-value">Phone Number:</span>
						<span class="light-txt">{{$checkout->user->phone_number}}</span>
					</div>
					
					<div class="Address"> 
						<span class="address-value">Address:</span>
						<span class="light-txt">{{$checkout->address}}</span>
					</div>
					<div class="City"> 
						<span class="address-value">City:</span>
						<span class="light-txt">{{$checkout->city}}</span>
					</div>
					<div class="State"> 
						<span class="address-value">State:</span>
						<span class="light-txt">{{$checkout->state}}</span>
					</div>
					@endif
				</td>
				<td>
					@if($checkout->invoice_generated=='1')
					@if($order->arrangementTo->date < date('Y-m-d'))
				        Arrangement is Expired
				    @else
					<a class="btn brown-btn" href="{{ route('ab.zipfile', $order->id) }}">DOWNLOAD ZIP FILE</a>
					@endif
					@endif
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
@endsection