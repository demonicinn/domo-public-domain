@extends('layouts.app')
@section('content')
<div class="tab-content padding-top-50">
	<table class="table" style="width:100%">
		<thead>
			<tr>
				<th>DATE</th>
				<th>AMOUNT</th>
			</tr>
		</thead>
		<tbody>
			@foreach($orders as $order)
			<tr>
				<td>
					<a href="{{ route('ab.arrangements.show', $order->id) }}">
						<span class="purchase">Purchase <span class="display-block">
					{{ date('m-d-Y' , strtotime($order->created_at)) }}	
					</span></span>
					</a>
				</td>
				<td>
					<span class="arrangement-value">${{number_format($order->total_price, 2)}}</span>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	<div class="pagination">
		{{ $orders->links() }}
	</div>
</div>
@endsection