@extends('layouts.app')
@section('content')
<div class="tab-content padding-top-50">
	<div id="PROFILE" class="tab-pane active">
		<div class="form-box-inner max-555">
			{!! Form::model($user, ['route' => 'admin.contactUsForm']) !!}
			<div class="form-grouph input-design {!! ($errors->has('first_name') ? 'has-error' : '') !!}">
				{!! Form::label('first_name','First Name', ['class' => 'control-label']) !!}
				{!! Form::text('first_name', null, ['class' => 'form-control' . ($errors->has('first_name') ? ' is-invalid' : '') ]) !!}
				{!! $errors->first('first_name', '<span class="help-block">:message</span>') !!}
			</div>
			<div class="form-grouph input-design {!! ($errors->has('last_name') ? 'has-error' : '') !!}">
				{!! Form::label('last_name','Last Name', ['class' => 'control-label']) !!}
				{!! Form::text('last_name', null, ['class' => 'form-control' . ($errors->has('last_name') ? ' is-invalid' : '') ]) !!}
				{!! $errors->first('last_name', '<span class="help-block">:message</span>') !!}
			</div>
			<div class="form-flex">
				<div class="form-grouph input-design {!! ($errors->has('zipcode') ? 'has-error' : '') !!}">
					{!! Form::label('zipcode','Zip Code', ['class' => 'control-label']) !!}
					{!! Form::number('zipcode', null, ['class' => 'form-control' . ($errors->has('zipcode') ? ' is-invalid' : '') ]) !!}
					{!! $errors->first('zipcode', '<span class="help-block">:message</span>') !!}
				</div>
				<div class="form-grouph input-design {!! ($errors->has('school') ? 'has-error' : '') !!}">
					{!! Form::label('school','School', ['class' => 'control-label']) !!}
					{!! Form::text('school', null, ['class' => 'form-control' . ($errors->has('school') ? ' is-invalid' : '') ]) !!}
					{!! $errors->first('school', '<span class="help-block">:message</span>') !!}
				</div>
			</div>
			<div class="form-grouph input-design {!! ($errors->has('email') ? 'has-error' : '') !!}">
				{!! Form::label('email','Email', ['class' => 'control-label']) !!}
				{!! Form::email('email', null, ['class' => 'form-control' . ($errors->has('email') ? ' is-invalid' : '') ]) !!}
				{!! $errors->first('email', '<span class="help-block">:message</span>') !!}
			</div>
			<div class="form-grouph input-design {!! ($errors->has('phone_number') ? 'has-error' : '') !!}">
				{!! Form::label('phone_number','Phone Number', ['class' => 'control-label']) !!}
				{!! Form::number('phone_number', null, ['class' => 'form-control' . ($errors->has('phone_number') ? ' is-invalid' : '') ]) !!}
				{!! $errors->first('phone_number', '<span class="help-block">:message</span>') !!}
			</div>
			<div class="form-grouph textarea-design {!! ($errors->has('subject') ? 'has-error' : '') !!}">
				{!! Form::label('subject','Support Questions?', ['class' => 'control-label']) !!}
				{!! Form::textarea('subject', null, ['class' => 'form-control' . ($errors->has('subject') ? ' is-invalid' : ''), 'placeholder' => 'Support Questions?'  ]) !!}
				{!! $errors->first('subject', '<span class="help-block">:message</span>') !!}
			</div>
			<div class="form-grouph submit-design">
				<button type="submit" class="submit-btn">SUBMIT</button>
			</div>
		{!! Form::close() !!}
	</div>
</div>
</div>
@endsection