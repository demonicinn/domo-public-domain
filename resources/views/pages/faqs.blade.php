@extends('layouts.app')
@section('content')
<section class="page-title padd-t-65">
	<div class="container-1195">
		<div class="inner-page-title big-title mrg-b-35">
			<h2>FAQS</h2>
		</div>
	</div>
</section>
<section class="main-faq-section">
	<div class="container-1195">
		<div id="accordion">
			<div class="card">
				<div class="card-header">
                    <a class="card-link" data-toggle="collapse" href="#collapseOne">
						Collapsible Group Item #1
					</a>
				</div>
				<div id="collapseOne" class="collapse show" data-parent="#accordion">
                    <div class="card-body">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-header">
                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
						Collapsible Group Item #2
					</a>
				</div>
				<div id="collapseTwo" class="collapse" data-parent="#accordion">
                    <div class="card-body">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-header">
                    <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">
						Collapsible Group Item #3
					</a>
				</div>
				<div id="collapseThree" class="collapse" data-parent="#accordion">
                    <div class="card-body">
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="more-questions">
			<a href="#" class="btn brown-btn">HAVE MORE QUESTIONS?</a>
		</div>
	</div>
</section>
@endsection
