@extends('layouts.app')
@section('content')
<section class="home-banner-sec" style="background-image: url('images/banner-home.png');">
	<div class="banner-cntnt-container">
		<div class="banner-cntnt-text">
			<h2>Music, Structure & Formations</h2>
			<p>Filling the need for marching bands any size to get the products needed for your performance.</p>
			<div class="banner-btn">
				<a class="btn btn-rectangular brown-btn" href="{{route('product.index') }}">VIEW OUR LIBRARY</a>
			</div>
		</div>
	</div>
</section>
<section class="brand-music-style">
	<div class="music-style-white-box">
		<div class="music-style-search-box">
			<div class="form-heading">
				<h4>Type</h4>
			</div>
			{!! Form::open(['route' => 'product.index', 'method'=>'get']) !!}
			<div class="row align-items-center">
			       <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
                     <div class="music-search-txt">
                       <p>Find your bands music style and type</p>
                     </div>
                   </div>
				   <div class="col-xl-2 col-lg-2 col-md-6 col-sm-12">
                      <div class="form-grouph input-design">
                        <input type="text" name="zipcode" placeholder="Zip Code">
                      </div>
                  </div>
				  <div class="col-xl-2 col-lg-2 col-md-6 col-sm-12">
                    <div class="form-grouph select-design">
                      <select name="arrangement">
                        <option value="">Select Arrangement</option>
						@foreach($arrangementtypes as $i => $type)
                        <option value="{{$type->title}}">{{$type->title}}</option>
						@endforeach
                      </select>
                    </div>
                  </div>
				<div class="col-xl-2 col-lg-2 col-md-6 col-sm-12">
					<div class="form-grouph input-design input-relative {!! ($errors->has('search') ? 'has-error' : '') !!}">
						{!! Form::text('search', request()->search ?? null, ['class' => 'form-control' . ($errors->has('search') ? ' is-invalid' : ''), 'placeholder' => 'Search']) !!}
						{!! $errors->first('search', '<span class="help-block">:message</span>') !!}
						<span class="input-abs-icon"><i class="fas fa-search"></i></span>
					</div>
				</div>
				<div class="col-xl-2 col-lg-2 col-md-6 col-sm-12">
					<button type="submit" class="btn btn-primary">Search</button>
				</div>
			</div>
			{!! Form::close() !!}
		</div>
		<div class="music-selection-slider">
			<section class="center slider">
			@foreach($arrangementtypes as $i => $type)
				<div>
					<a href="{{route('product.index') }}?type={{$type->title}}" class="slider-music-wrapper">
						<div class="music-selection-img">
							<img src="{{ asset('storage/uploads/arrangementstype/'.$type->image) }}">
						</div> 
						<div class="music-selection-cntnt">
							<h5>{{$type->title}}</h5>
						</div>
					</a>
				</div>
			@endforeach
			</section>
			<div class="see-all-link text-right">
				<a href="{{route('product.index') }}">See all</a>
			</div>
		</div>
	</div>
</section>
<section class="helping-school-sec" style="background-image: url('images/helping-school-bg.png');">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
				<div class="helping-school-img">
					<img src="{{ asset('images/helping-school.png') }}">
				</div>
			</div>
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
				<div class="helping-cntnt-right text-right">
					<h2>Helping School Marching Bands</h2>
					<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. </p>
					<a class="btn btn-rectangular brown-btn" href="{{ route('aboutus') }}">MORE ABOUT US</a>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="arrangement-brands">
	<div class="container-brands">
		<div class="row">
			<div class="col-xl-7 col-lg-7 col-md-7 col-sm-12">
				<div class="arraangement-text">
					<h2>Find the arrangements for your band.</h2>
					<p>It is a long established fact that a reader will be distracted by the <br>readable content of a page when looking at its layout. </p>
				</div>
			</div>
			<div class="col-xl-5 col-lg-5 col-md-5 col-sm-12">
				<div class="arraangement-text-btn">
					<a class="btn btn-rectangular white-btn" href="{{route('product.index') }}">SEARCHING MARCHING BAND ARRANGEMENTS</a>                 
				</div>
			</div>
		</div>
	</div>
</section>
<section class="about-us-section">
	<div class="container-brands"> 
		<div class="row align-items-center">
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
				<div class="heading-paragraph-design">
					<h2>About Us</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
					<p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>       
					<div class="about-block-design">
						<a href="{{route('product.index') }}">MUSIC</a>
						<a href="{{route('product.index') }}">STRUCTURE</a>
						<a href="{{route('product.index') }}">FORMATIONS</a>
						<a href="{{route('product.index') }}">ARRANGEMENTS</a>
					</div>
					<div class="about-formation-btn">
						<a class="btn btn-rectangular brown-btn" href="{{ route('aboutus') }}">MORE ABOUT US</a>
					</div>
				</div>
			</div>
			<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
				<div class="about-rght-img">
					<img src="{{ asset('images/about-img.png') }}">
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('script')
<script>
	$(".center").slick({
		dots: true,
		infinite: true,
		centerMode: true,
		slidesToShow: 5,
		dots: false,
		slidesToScroll: 3,
		responsive: [
		{
			breakpoint: 1200,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 1
			}
		},
		{
			breakpoint: 768,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 1
			}
		},
		{
			breakpoint: 575,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		},
		]
	});
</script>
@endsection