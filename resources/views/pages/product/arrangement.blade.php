@extends('layouts.app')
@section('content')
<section class="page-title">
	<div class="container-1265">
		<div class="inner-page-title mrg-b-35">
			<h2>
				@if(request()->segment(1)=='featured-products')
				Featured Arrangement
				@else
				Arrangement
				@endif
			</h2>
		</div>
	</div>
</section>
<section class="invidual-product-section">
	<div class="container-1265">
		@livewire('show-product')
	</div>
</section>
@endsection

@section('script')
<script>

	let audioId = 0;
	$('.play').click(function() {
		//play
		var $this = $(this);
		var data_id = $this.attr('x-data');
		//console.log(audioId)
		if(audioId!=data_id){
			console.log('inner', audioId)
			//stop
			$('.play').html('<span class="onplay"><i class="fas fa-play"></i></span>');
			let checkAudio = $('.play.active').parents('.product-description').find('audio[id^="myAudio"]').get(0);
			if(checkAudio){
				checkAudio.pause();
			}
			audioId = 0;
			$('.play').removeClass('active');
			
			let audio = $this.parents('.product-description').find('audio[id^="myAudio"]').get(0);
			$this.html('<span class="offpause"><i class="fas fa-pause"></i></span>'); 
			$this.addClass('active');
			audioId = data_id;
			audio.play();
		}
		else {
			//stop
			$('.play').html('<span class="onplay"><i class="fas fa-play"></i></span>');
			let checkAudio = $('.play.active').parents('.product-description').find('audio[id^="myAudio"]').get(0);
			if(checkAudio){
				checkAudio.pause();
			}
			audioId = 0;
			$('.play').removeClass('active');	
		}
		
	});	
</script>
@endsection