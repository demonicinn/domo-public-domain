<div>
    <header id="main-header">
		<div class="header-inner-container{{ request()->segment('1')=='' ? '' : ' white-bg-header' }}">
			<nav class="navbar navbar-expand-xl">
				<!-- Brand -->
				<a class="navbar-brand" href="{{ route('home') }}">
					@if(Route::currentRouteName()=='home')
					<img src="{{ asset('/images/logo.png') }}">
					@else 
					<img src="{{ asset('/images/brown-logo.png') }}">
					@endif
				</a>
				
				<!-- Toggler/collapsibe Button -->
				<div class="desktop-hide mobile-cart">
				<ul>
				@if(@$cart=='0')
						<li class="nav-item cart">
							<a class="nav-link" href="{{route('product.index') }}">
								<span class="badget">0</span>
							<i class="fas fa-shopping-cart"></i></a>
						</li>
						@else
						<li class="nav-item cart">
							<a class="nav-link" href="{{route('cart.index') }}" wire:click.prevent="cartList()">
								<span class="badget">{{ $cartProductsCount }}</span>
							<i class="fas fa-shopping-cart"></i></a>
							<div class="dropdown-menu {{ $is_cartList==true?'show' : '' }}">
								<ul class="list-unstyled">
								    @if(@$cartProducts)
									@foreach($cartProducts->products as $product)
									<li class="product-dropdown">
										<a href="{{ route('product.arrangement', [$product->arrangementTo->id, $product->arrangementTo->slug]) }}" class="product-dropdown-flex">
											<div class="product-img">
												<img src="{{ asset('storage/uploads/arrangements/'.$product->arrangementTo->image) }}">
											</div>
											<div class="product-description">
												<h4>{{@$product->arrangementTo->title}}</h4>
												<p class="price">${{$product->price}}</p>
											</div>
										</a>
									</li>
									@endforeach
									@endif
								</ul>
								<a class="nav-link cart" href="{{route('cart.index') }}">
								View Cart
								</a>
							</div>
						</li>
						@endif
						
					</ul>
				</div>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
					<span class="navbar-toggler-icon"><i class="fas fa-bars"></i></span>
				</button>
				
				<!-- Navbar links -->
				<div class="collapse navbar-collapse justify-content-end" id="collapsibleNavbar">
					<ul class="navbar-nav align-items-center">
						<li class="nav-item">
							<a class="nav-link" href="{{route('product.index') }}">SHOP ARRANGEMENTS</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="{{route('product.feature') }}">FEATURED SHOWS</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="{{route('aboutus') }}">ABOUT US</a>
						</li>
						
						@guest
						<li class="nav-item login">
							<a class="nav-link" href="{{ route('login') }}">LOGIN</a>
						</li>
						
						<li class="nav-item create-account">
							<a class="nav-link" href="{{ route('register') }}">CREATE AN ACCOUNT</a>
						</li>
						@else
						<li class="nav-item create-account">
							<a class="nav-link" href="{{ route('profile') }}">Profile</a>
						</li>
						
						<li class="nav-item create-account">
							<a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
								LOGOUT
							</a>
						</li>
						<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
							@csrf
						</form>
						@endguest
						
						@if(@$cart=='0')
						<li class="nav-item cart">
							<a class="nav-link" href="{{route('product.index') }}">
								<span class="badget">0</span>
							<i class="fas fa-shopping-cart"></i></a>
						</li>
						@else
						<li class="nav-item cart">
							<a class="nav-link" href="{{route('cart.index') }}" wire:click.prevent="cartList()">
								<span class="badget">{{ $cartProductsCount }}</span>
							<i class="fas fa-shopping-cart"></i></a>
							<div class="dropdown-menu {{ $is_cartList==true?'show' : '' }}">
								<ul class="list-unstyled">
								    @if(@$cartProducts)
									@foreach($cartProducts->products as $product)
									<li class="product-dropdown">
										<a href="{{ route('product.arrangement', [$product->arrangementTo->id, $product->arrangementTo->slug]) }}" class="product-dropdown-flex">
											<div class="product-img">
												<img src="{{ asset('storage/uploads/arrangements/'.$product->arrangementTo->image) }}">
											</div>
											<div class="product-description">
												<h4>{{@$product->arrangementTo->title}}</h4>
												<p class="price">${{$product->price}}</p>
											</div>
										</a>
									</li>
									@endforeach
									@endif
								</ul>
								<a class="nav-link cart" href="{{route('cart.index') }}">
								View Cart
								</a>
							</div>
						</li>
						@endif
						
					</ul>
				</div>
			</nav>
		</div>
	</header>
</div>
