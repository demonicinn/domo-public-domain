<div>
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
		<div class="cart-summry-box main-cart-page">
			<div class="form-heading">
				<h4>Cart Summary</h4>
			</div>
			
			
			@foreach($checkoutProducts->products as $checkout)
			<div class="cart-smry-flex mb-3">
				
				
				<div class="invidual-product-img">
					<img src="{{ asset('storage/uploads/arrangements/'.$checkout->arrangementTo->image) }}">
				</div>
				
				<div class="product-summry">
					<p class="product-summry-value">{{$checkout->arrangementTo->title}}</p>
					@if($checkout->arrangementtype=='wind_arrangment')
					<p class="product-summry-light-txt">Wind Arrangement Only</p>
					@else
					<p class="product-summry-light-txt">Wind + Percussion Arrangement</p>
					@endif
					<div class="product-summry-price">
					<p class="price">
						${{ number_format($checkout->price, 2) }}
					</p>
					<p class="remove">
						<button type="button" class="btn btn-sm btn-danger" wire:click.prevent="deleteProduct({{$checkout->id}})" wire:loading.attr="disabled">Remove</button>
					</p>
				</div>
				</div>
			</div>
			@endforeach
			
			<div class="prdct-summry-sub-totel">
				<p class="sub-totel">Sub Total: <strong>${{ number_format($checkoutProducts->price, 2) }}</strong></p>
				
				<a href="{{ route('checkout.index') }}" class="btn brown-btn checkout pull-right">Checkout</a>
			</div>
			
			
			
		</div>
	</div>
</div>
