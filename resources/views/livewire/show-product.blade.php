<div>
	<div class="row">
		<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
			<div class="indvidual-product-left-column">
				<div class="invidual-product-img">
					<img src="{{ asset('storage/uploads/arrangements/'.$allArrangements->image) }}">
				</div>
				
				<div class="product-description">
					<div class="audio-heading">
						<h4 class="product-heading"><a href="{{ route('product.arrangement', [$allArrangements->id, $allArrangements->slug]) }}">{{$allArrangements->title}}</a></h4>
						<div class="audio-icon-box">
							<audio id="myAudio">
								<source src="{{ asset('storage/uploads/arrangements/'.$allArrangements->wmp3) }}">
							</audio>
							<button class="play" type="button" x-data="{{$allArrangements->id}}">
								<span class="onplay"><i class="fas fa-play"></i></span>
							</button>
						</div>
					</div>
					<div class="audio-gif">
					<img src="{{ asset('images/music.png') }}" class="png-audio"> 
                      <img src="{{ asset('images/music.gif') }}" style="display: none;" class="gif-audio"> 
                    </div>
				</div>
			</div>
		</div>
		<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
			<div class="indvidual-product-rght-column">
				<div class="product-description">
					<h2 class="indvidual-product-heading"><b>{{$allArrangements->title}}</b></h2>
					<p class="invidual-product-description"><strong>Description: </strong>{{$allArrangements->description}} </p>
					<div class="indvidual-product-details">
						@if(@$allArrangements->composerTO->title)
						<p><storng>Composer:</storng> {{$allArrangements->composerTO->title}}</p>
						@endif
						@if(@$allArrangements->windarrangerTO->title)
						<p><storng>Wind Arranger:</storng> {{$allArrangements->windarrangerTO->title}}</p>
						@endif
						@if(@$allArrangements->difficultyTo->title)
						<p><storng>Difficulty:</storng> {{$allArrangements->difficultyTo->title}}</p>
						@endif
						@if(@$allArrangements->instrumentationTo->title)
						<p><storng>Instrumentation:</storng> {{$allArrangements->instrumentationTo->title}}</p>
						@endif
						@if(@$allArrangements->percussionarrangerTO->title)
						<p><storng>Percussion Arranger:</storng> {{$allArrangements->percussionarrangerTO->title}}</p>
						@endif
						@if(@$allArrangements->arrangementTypeTo->title)
						<p><storng>Arrangement Type:</storng> {{$allArrangements->arrangementTypeTo->title}}</p>
						@endif
						@if(@$allArrangements->date)
						<p><storng>Expire Date:</storng> {{ date('m-d-Y' , strtotime($allArrangements->date)) }}	</p>
						@endif
					</div>
					<div class="invidual-product-btm form-section">
						<p><strong>Cost:</strong></p>
						<div class="indvidual-product-btn">
							<p wire:model="wind_arrangment" class="btn {{$type=='wind_arrangment'?'grey-btn':'activate-btn'}} price-arrangement" wire:click="setType('wind_arrangment')">${{number_format($allArrangements->wind_arrangment, 2)}} - Wind Arrangement Only</p>
							
							<p class="btn {{$type=='percussion_arrangment'?'grey-btn':'activate-btn'}}" wire:model="percussion_arrangment" wire:click="setType('percussion_arrangment')"><strong>${{number_format($allArrangements->percussion_arrangment, 2)}}</strong> - Wind + Percussion Arrangement</p>
							
							@if ($message = Session::get('error'))
							<div class="alert alert-dismissible alert-danger mb-0">
								<button class="close" type="button" data-dismiss="alert">×</button>
								{!! $message !!}
							</div>
							@endif
							@if ($message = Session::get('success'))
							<div class="alert alert-dismissible alert-success mb-0">
								<button class="close" type="button" data-dismiss="alert">×</button>
								{!! $message !!}
								<a href="{{ route('cart.index') }}">View Cart</a>
							</div>
							@endif
							<div class="form-grouph input-design{!! ($errors->has('zip_code') ? ' has-error' : '') !!}">
								{!! Form::label('zip_code','Zip Code') !!}
								<input wire:model.debounce.1000ms="zip_code" type="number" id="zip_code" class="form-control" placeholder="Enter school zip code">
								{!! $errors->first('zip_code', '<span class="help-block">:message</span>') !!}
								<p class="response-serach"></p>					
							</div>
							
							<button type="submit" wire:click="checkout" wire:loading.attr="disabled" class="btn brown-btn checkout">ADD TO CART</button>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	@push('scripts')
	<script>
		window.addEventListener('checkCartCount', event => {
			$('.nav-item.cart .badget').html(event.detail.count);
		})
	</script>
	@endpush
</div>
