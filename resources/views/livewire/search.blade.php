<div>
	{{--
	<div class="loader-main" wire:loading.delay>
		<img class="loader" src="{{ asset('images/loader.gif') }}">
	</div>
	--}}
	<div class="row">
		<div class="col-xl-3 col-lg-3 col-md-3 col-sm-12">
			<div class="product-left-bar form-section">
				<div class="form-grouph input-design input-relative">
					<input type="text" wire:model.debounce.1000ms="zipcode" placeholder="Zipcode">
				</div>
				<div class="form-grouph input-design input-relative">
					<input type="text" wire:model.debounce.1000ms="search" placeholder="Search">
					<span class="abs-icon-input"><i class="fas fa-search"></i></span>
				</div>
				
				<div class="form-grouph drop-design {{ $is_arrangementtype==true?'show' : '' }}">
					<button type="button" class="dropdown-toggle" wire:click.prevent="sortBy('is_arrangementtype')">
						Arrangement Type
					</button>
					<div class="dropdown-menu {{ $is_arrangementtype==true?'show' : '' }}">
						<ul class="list-unstyled">
							<li class="dropdown-item">
								<div class="checkbox-design-dropdown">
									@foreach(App\Models\Arrangement::arrangementtypes() as $type)
									<div class="inner-checkbox">
										<input wire:model="arrangement_type" type="checkbox" value="{{$type->id}}">
										<label>{{$type->title}}</label>
									</div>
									@endforeach
								</div>
							</li>
						</ul>
					</div>
				</div>
				
				<div class="form-grouph drop-design {{ $is_difficulty==true?'show' : '' }}">
					<button type="button" class="dropdown-toggle" wire:click.prevent="sortBy('is_difficulty')">
						Difficulty
					</button>
					<div class="dropdown-menu {{ $is_difficulty==true?'show' : '' }}">
						<ul class="list-unstyled">
							<li class="dropdown-item">
								<div class="checkbox-design-dropdown">
									@foreach(App\Models\Arrangement::difficulties() as $difficulty)
									<div class="inner-checkbox">
										<input wire:model="difficulty" type="checkbox" value="{{$difficulty->id}}">
										<label>{{$difficulty->title}}</label>
									</div>
									@endforeach
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="form-grouph drop-design {{ $is_instrumentation==true?'show' : '' }}">
					<button type="button" class="dropdown-toggle" wire:click.prevent="sortBy('is_instrumentation')">
						Instrumentation
					</button>
					<div class="dropdown-menu {{ $is_instrumentation==true?'show' : '' }}">
						<ul class="list-unstyled">
							<li class="dropdown-item">
								<div class="checkbox-design-dropdown">
									@foreach(App\Models\Arrangement::instrumentations() as $instrumentation)
									<div class="inner-checkbox">
										<input wire:model="instrumentation" type="checkbox" value="{{$instrumentation->id}}">
										<label>{{$instrumentation->title}}</label>
									</div>
									@endforeach
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="form-grouph drop-design {{ $is_composer==true?'show' : '' }}">
					<button type="button" class="dropdown-toggle" wire:click.prevent="sortBy('is_composer')">
						Composer
					</button>
					<div class="dropdown-menu {{ $is_composer==true?'show' : '' }}">
						<ul class="list-unstyled">
							<li class="dropdown-item">
								<div class="checkbox-design-dropdown">
									@foreach(App\Models\Arrangement::composers() as $composer)
									<div class="inner-checkbox">
										<input wire:model="composer" type="checkbox" value="{{$composer->id}}">
										<label>{{$composer->title}}</label>
									</div>
									@endforeach
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="form-grouph drop-design {{ $is_wind_arranger==true?'show' : '' }}">
					<button type="button" class="dropdown-toggle" wire:click.prevent="sortBy('is_wind_arranger')">
						Wind Arranger
					</button>
					<div class="dropdown-menu {{ $is_wind_arranger==true?'show' : '' }}">
						<ul class="list-unstyled">
							<li class="dropdown-item">
								<div class="checkbox-design-dropdown">
									@foreach(App\Models\Arrangement::wind_arrangers() as $arranger)
									<div class="inner-checkbox">
										<input wire:model="wind_arranger" type="checkbox" value="{{$arranger->id}}">
										<label>{{$arranger->title}}</label>
									</div>
									@endforeach
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="form-grouph drop-design {{ $is_percussion_arranger==true?'show' : '' }}">
					<button type="button" class="dropdown-toggle" wire:click.prevent="sortBy('is_percussion_arranger')">
						Percussion Arranger
					</button>
					<div class="dropdown-menu {{ $is_percussion_arranger==true?'show' : '' }}">
						<ul class="list-unstyled">
							<li class="dropdown-item">
								<div class="checkbox-design-dropdown">
									@foreach(App\Models\Arrangement::percussion_arrangers() as $percussion)
									<div class="inner-checkbox">
										<input wire:model="percussion_arranger" type="checkbox" value="{{$percussion->id}}">
										<label>{{$percussion->title}}</label>
									</div>
									@endforeach
								</div>
							</li>
						</ul>
					</div>
				</div>
				
				<button type="button" class="btn btn-primary btn-block" wire:click.prevent="resetFilter()">
						Reset
					</button>
				
			</div>
		</div>
		<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12">
			<div class="product-collection-box">
				<div class="row">
					@foreach($arrangements as $arrangement)
					<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
						<div class="produc-box">
							<div class="product-img">
								<img src="{{ asset('storage/uploads/arrangements/'.$arrangement->image) }}">
							</div>
							<div class="product-description">
							<div class="audio-heading">
								<h4 class="product-heading"><a href="{{ route('product.arrangement', [$arrangement->id, $arrangement->slug]) }}">{{$arrangement->title}}</a></h4>
							    {{--<div class="audio-icon-box">
								<audio id="myAudio">
									<source src="{{ asset('storage/uploads/arrangements/'.$arrangement->wmp3) }}">
								</audio>
								<button class="play" type="button" x-data="{{$arrangement->id}}">
								<span class="onplay"><i class="fas fa-play"></i></span>
								</button>
                               </div>--}}
                               </div>
								<div class="product-details">
									@if(@$arrangement->composerTO->title)
									<p><storng>Composer:</storng> {{$arrangement->composerTO->title}}</p>
									@endif
									@if(@$arrangement->windarrangerTO->title)
									<p><storng>Wind Arranger:</storng> {{$arrangement->windarrangerTO->title}}</p>
									@endif
									@if(@$arrangement->difficultyTo->title)
									<p><storng>Difficulty:</storng> {{$arrangement->difficultyTo->title}}</p>
									@endif
									@if(@$arrangement->instrumentationTo->title)
									<p><storng>Instrumentation:</storng> {{$arrangement->instrumentationTo->title}}</p>
									@endif
									@if(@$arrangement->percussionarrangerTO->title)
									<p><storng>Percussion Arranger:</storng> {{$arrangement->percussionarrangerTO->title}}</p>
									@endif
									@if(@$arrangement->arrangementTypeTo->title)
									<p><storng>Arrangement Type:</storng> {{$arrangement->arrangementTypeTo->title}}</p>
									@endif
									@if(@$arrangement->date)
									<p><storng>Expire Date:</storng> {{ date('m-d-Y' , strtotime($arrangement->date)) }}	</p>
									@endif
								</div>
								<div class="product-btn">
									<a class="btn brown-btn" href="{{ route('product.arrangement', [$arrangement->id, $arrangement->slug]) }}">VIEW ARRANGEMENT</a>
								</div>
							</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>
		<div class="pagination">
			{{ $arrangements->links() }}
		</div>
	</div>
	
</div>
