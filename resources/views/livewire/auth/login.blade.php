<div>
	<button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#loginModal">Login</button>
	<p class="field-text">Or fill out the form below </p>
	<div wire:ignore.self class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="modalDelete" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="loginModalLabel">Login</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<section class="form-section login-section">
					<div class="container">
						
						@if ($message = Session::get('error'))
						<div class="alert alert-dismissible alert-danger mb-0">
							<button class="close" type="button" data-dismiss="alert">×</button>
							{!! $message !!}
						</div>
						@endif
					
						<div class="form-box-inner max-555">
							<div class="form-grouph input-design{!! ($errors->has('email') ? ' has-error' : '') !!}">
								<input wire:model="email" type="email" placeholder="Email" id="email" class="form-control">
								{!! $errors->first('email', '<span class="help-block">:message</span>') !!}
							</div>
							<input type="hidden" wire:model="redirect" value="true">
							
							<div class="form-grouph input-design{!! ($errors->has('password') ? ' has-error' : '') !!}">
								<input wire:model="password" type="password" id="password" placeholder="Password" class="form-control">
								{!! $errors->first('password', '<span class="help-block">:message</span>') !!}
							</div>
							<div class="form-grouph submit-design">
								<button class="submit-btn" wire:click="login" wire:loading.attr="disabled" type="submit">{{__ ('LOGIN') }}</button>
							</div>
							
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
	
	@push('scripts')
	<script>
		window.livewire.on('modalHide', () => {
            $('#modalFormVisible').modal('hide');
        });
	</script>
	@endpush	
		
		
</div>

