<div>
	<div class="row">
		<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
			<div class="payement-details-column">
				<div class="form-heading">
					<h4>Billing Detail</h4>
				</div>
				
				<div class="form-grouph input-design {!! ($errors->has('first_name') ? ' has-error' : '') !!}">
					<input type="text" wire:model="first_name" placeholder="First Name">
					{!! $errors->first('first_name', '<span class="help-block">:message</span>') !!}
				</div>
				<div class="form-grouph input-design {!! ($errors->has('last_name') ? ' has-error' : '') !!}">
					<input type="text" wire:model="last_name" placeholder="Last Name">
					{!! $errors->first('last_name', '<span class="help-block">:message</span>') !!}
				</div>
				
				<div class="form-flex">
					<div class="form-grouph input-design {!! ($errors->has('zipcode') ? ' has-error' : '') !!}">
						<input wire:model="zipcode" type="number" placeholder="Zip Code" readonly>
						{!! $errors->first('zipcode', '<span class="help-block">:message</span>') !!}
					</div>
					<div class="form-grouph input-design {!! ($errors->has('school') ? ' has-error' : '') !!}">
						<input wire:model="school" type="text" placeholder="School">
						{!! $errors->first('school', '<span class="help-block">:message</span>') !!}
					</div>
				</div>
				
				@if(!auth()->check())
				<div class="form-grouph input-design input-design {!! ($errors->has('email') ? ' has-error' : '') !!}">
					<input wire:model="email" type="email" placeholder="Email">
					{!! $errors->first('email', '<span class="help-block">:message</span>') !!}
				</div>
				<div class="form-grouph input-design input-design {!! ($errors->has('password') ? ' has-error' : '') !!}">
					<input wire:model="password" type="password" placeholder="Password">
					{!! $errors->first('password', '<span class="help-block">:message</span>') !!}
				</div>
				@endif
				
				<div class="form-grouph input-design {!! ($errors->has('phone_number') ? ' has-error' : '') !!}">
					<input wire:model="phone_number" type="number" placeholder="Phone Number">
					{!! $errors->first('phone_number', '<span class="help-block">:message</span>') !!}
				</div>
				
				<div class="form-grouph input-design {!! ($errors->has('address') ? ' has-error' : '') !!}">
					<input wire:model="address" type="text" placeholder="Address">
					{!! $errors->first('address', '<span class="help-block">:message</span>') !!}
				</div>
				
				
				<div class="form-flex">
					<div class="form-grouph input-design {!! ($errors->has('city') ? ' has-error' : '') !!}">
						<input wire:model="city" type="text" placeholder="City">
						{!! $errors->first('city', '<span class="help-block">:message</span>') !!}
					</div>
					<div class="form-grouph select-design label-design {!! ($errors->has('state') ? ' has-error' : '') !!}">
						<select wire:model="state" wire:change="stateChange($event.target.value)">
							<option value="">Select State</option>
							@foreach(\App\Models\UsStates::all() as $k => $value)
							<option value="{{ $value->state_name }}">{{ $value->state_name }}</option>
							@endforeach
						</select>
					</div>
				</div>
				
				
				<div class="form-heading div-space">
					<h4>Payment Method</h4>
				</div>
				
				<div class="form-grouph">
					<input wire:model="order" type="radio" name="order" value="payment" {{ $order=='payment' ? 'checked' : '' }}>
					<label for="card_name">Pay with Credit Card</label>
				</div>
				
				<div class="form-grouph">
					<input wire:model="order" type="radio" name="order" value="purchase" {{ $order=='purchase' ? 'checked' : '' }}>
					<label for="card_name">Purchase Order</label>
				</div>
				
				@if($order=='payment')
				<div class="form-grouph input-design input-relative label-design {!! ($errors->has('card_name') ? ' has-error' : '') !!}">
					<label for="card_name">Card Name</label>
					<input class="" wire:model="card_name" type="text" id="card_name">
					{!! $errors->first('card_name', '<span class="help-block">:message</span>') !!}
				</div>
				
				<div class="form-grouph input-design input-relative label-design {!! ($errors->has('card_number') ? ' has-error' : '') !!}">
					<label for="card_number">Card Number</label>
					<input class="" maxlength="16" wire:model="card_number" type="number" id="card_number">
					{!! $errors->first('card_number', '<span class="help-block">:message</span>') !!}
				</div>
				
				<div class="form-flex flex-3">
					
					<div class="form-grouph select-design label-design {!! ($errors->has('exp_month') ? ' has-error' : '') !!}">
						<label for="exp_month">Exp Month</label>
						<select class="" id="exp_month" wire:model="exp_month">
							<option value="">Select Month</option>
							@for ($i = 1; $i <=12; $i++)
							<option value="{{ $i<=9 ? '0'.$i : $i }}">{{ date('F', mktime(0,0,0,$i)) }}</option>
							@endfor
						</select>
						{!! $errors->first('exp_month', '<span class="help-block">:message</span>') !!}
					</div>					
					<div class="form-grouph select-design {!! ($errors->has('exp_year') ? ' has-error' : '') !!}">
						<label for="exp_year">Year</label>
						<select class="" id="exp_year" wire:model="exp_year">
							<option value="">Select Year</option>
							@for ($i = 0; $i <10; $i++)
							@php $year = date('Y') + $i; @endphp
							<option value="{{$year}}">{{$year}}</option>
							@endfor
						</select>
						{!! $errors->first('exp_year', '<span class="help-block">:message</span>') !!}
					</div>
					<div class="form-grouph input-design label-design {!! ($errors->has('cvv') ? ' has-error' : '') !!}">
						<label for="cvv">Security Code</label>
						<input placeholder="CVV" class="" maxlength="4" wire:model="cvv" type="number" id="cvv">	
						{!! $errors->first('cvv', '<span class="help-block">:message</span>') !!}
					</div>
				</div>
				@endif
				
			</div>
		</div>
		
		<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
			<div class="cart-summry-box">
				<div class="form-heading">
					<h4>Cart Summary</h4>
				</div>				
				
				@foreach($checkoutProducts->products as $checkout)
				<div class="cart-smry-flex">
					<div class="product-summry">
						<p class="product-summry-value">{{$checkout->arrangementTo->title}}</p>						
						@if($checkout->arrangementtype=='wind_arrangment')
						<p class="product-summry-light-txt">Wind Arrangement Only</p>
						@else
						<p class="product-summry-light-txt">Wind + Percussion Arrangement</p>
						@endif
					</div>
					<div class="product-summry-price">
						<p class="price">
							${{ number_format($checkout->price, 2) }}
						</p>
						{{--
						<p class="remove">
							<button type="button" class="btn btn-sm btn-danger" wire:click.prevent="deleteProduct({{$checkout->id}})" wire:loading.attr="disabled">Remove</button>
						</p>
						--}}
					</div>
				</div>
				<div class="product-summry-btm-details">
					@if(@$checkout->arrangementTo->arrangementTypeTo->title)
					<div class="Arrangement"> 
						<span class="arrangement-value">Arrangement Type:</span>
						<span class="light-txt">{{$checkout->arrangementTo->arrangementTypeTo->title}}</span>
					</div>
					@endif
					@if(@$checkout->arrangementTo->difficultyTo->title)
					<div class="Difficulty"> 
						<span class="difficulty-value">Difficulty:</span>
						<span class="light-txt">{{$checkout->arrangementTo->difficultyTo->title}}</span>
					</div>
					@endif
					@if(@$checkout->arrangementTo->instrumentationTo->title)
					<div class="Instrumentation"> 
						<span class="Instrumentation-value">Instrumentation:</span>
						<span class="light-txt">{{$checkout->arrangementTo->instrumentationTo->title}}</span>
					</div>
					@endif
					@if(@$checkout->arrangementTo->composerTO->title)
					<div class="Composer"> 
						<span class="Composer-Name">Composer Name:</span>
						<span class="light-txt">{{$checkout->arrangementTo->composerTO->title}}</span>
					</div>
					@endif
					@if(@$checkout->arrangementTo->windarrangerTo->title)
					<div class="Arranger"> 
						<span class="Arranger-Name">Wind Arranger Name:</span>
						<span class="light-txt">{{$checkout->arrangementTo->windarrangerTo->title}}</span>
					</div>
					@endif
					@if(@$checkout->arrangementTo->percussionarrangerTO->title)
					<div class="Arranger"> 
						<span class="Arranger-Name">Percussion Arranger Name:</span>
						<span class="light-txt">{{$checkout->arrangementTo->percussionarrangerTO->title}}</span>
					</div>
					@endif
				</div>
				@endforeach
				
				<div class="prdct-summry-sub-totel">
					<p class="sub-totel">Subtotal: <strong>${{ number_format($checkoutProducts->price, 2) }}</strong></p>
				</div>
				
				<div class="prdct-summry-sub-totel">
					<p class="sub-totel">Sales Tax({{$tax}}%): <strong>${{ number_format($checkoutProducts->tax, 2) }}</strong></p>
				</div>
				
				
				@if($checkoutProducts->total_price)
				<div class="prdct-summry-sub-totel">
					@if($checkoutProducts->discount)
					<p class="sub-totel">Discount: 
						<strong>{{$checkoutProducts->promocode_type=='fixed'?'$':''}}{{$checkoutProducts->discount}}{{$checkoutProducts->promocode_type=='percentage'?'%':''}}</strong>
					</p>
					@endif
					<p class="sub-totel">Total: <strong>${{number_format($checkoutProducts->total_price, 2)}}</strong></p>
				</div>
				@endif
				
				@if ($message = Session::get('promocodeError'))
				<div class="alert alert-dismissible alert-danger mb-0">
					<button class="close" type="button" data-dismiss="alert">×</button>
					{!! $message !!}
				</div>
				@endif
				@if ($message = Session::get('promocodeSucces'))
				<div class="alert alert-dismissible alert-success  mb-0">
					<button class="close" type="button" data-dismiss="alert">×</button>
					{!! $message !!}
				</div>
				@endif
				@if(!$checkoutProducts->discount)
					<div class="form-group {!! ($errors->has('promocode') ? ' has-error' : '') !!}">
						{!! Form::label('promocode','Apply Promocode') !!}
						<input class="form-control" wire:model="promocode" type="text" id="code" placeholder="Promocode">
						{!! $errors->first('promocode', '<span class="help-block">:message</span>') !!}
						<span class="invalid-promocode"></span>
					</div> 
					
					<button type="submit" class="btn btn-success btn-sm" wire:click="applyCode" wire:loading.attr="disabled" >Apply Promocode</button>
				@else
					<button type="submit" class="btn btn-danger" wire:click="removeCode" wire:loading.attr="disabled">Remove Promocode</button>
				@endif
				
			</div>
		</div>
	</div>
	
	<div class="form-grouph twobtns-flex">
		@if($order=='payment')
		<button type="submit" class="submit-btn" wire:click="paymentAction" wire:loading.attr="disabled">Pay</button>
		@else
		<button type="submit" class="submit-btn" wire:click="purchaseAction" wire:loading.attr="disabled">Purchased Order</button>
		@endif
	</div>
	
	
	@push('scripts')
	<script>
		window.livewire.on('modalHidePromocode', () => {
			$('#modalPromocode').modal('hide');
		});
	</script>
	@endpush
</div>
