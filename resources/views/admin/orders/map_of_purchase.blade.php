@extends('layouts.app')
@section('content')
<section class="map-main-section">
	<div id="map" style="width: 100%; height: 400px;"></div>
</section>

@endsection

@section('script')
<script src="https://maps.googleapis.com/maps/api/js?key={{ config('services.google.api') }}&libraries=places"></script>
<script>
	
		var map;
		var bounds = new google.maps.LatLngBounds();
		var mapOptions = {
			mapTypeId: 'roadmap'
		};
		
		// Display a map on the page
		map = new google.maps.Map(document.getElementById("map"), mapOptions);
		//map.setTilt(45);
        
		// Multiple Markers		
		var markers = [
			@foreach($orders as $order)
			{address:'{!!$order->address!!}', lat:{{$order->latitude}}, lng:{{$order->longitude}}},
			@endforeach
		];
		
		
        // Info Window Content
		var infoWindowContent = [
			@foreach($orders as $order)
			['<div class="info_content">' +
			'<p><a href="{{ route("admin.orders.show", $order->id) }}" target="_blank">{{$order->address}}</a></p>' +
			'<p>Date: {{ date("m-d-Y" , strtotime($order->created_at)) }}</p>' +
			'</div>'],
			@endforeach
		];
		
		
		// Display multiple markers on a map
		var infoWindow = new google.maps.InfoWindow(), marker, i;
		
		// Loop through our array of markers & place each one on the map  
		for( i = 0; i < markers.length; i++ ) {
			var position = new google.maps.LatLng(markers[i].lat, markers[i].lng);
			bounds.extend(position);
			marker = new google.maps.Marker({
				position: position,
				map: map,
				title: markers[i].address
			});
			
			// Allow each marker to have an info window    
			google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
					infoWindow.setContent(infoWindowContent[i][0]);
					infoWindow.open(map, marker);
				}
			})(marker, i));
			
			// Automatically center the map fitting all markers on the screen
			map.fitBounds(bounds);
		}
		
		// Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
		var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
			this.setZoom(14);
			google.maps.event.removeListener(boundsListener);
		});
		
</script>
@endsection