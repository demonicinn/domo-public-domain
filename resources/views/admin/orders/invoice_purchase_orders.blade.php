<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Public Domain Marching</title>
		<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet">
		<style>
			#invoice{
			padding: 30px;
			}
			
			.invoice {
			position: relative;
			background-color: #FFF;
			min-height: 680px;
			padding: 15px
			}
			
			.invoice header {
			padding: 10px 0;
			margin-bottom: 20px;
			border-bottom: 1px solid #3989c6
			}
			
			.invoice .company-details {
			text-align: right
			}
			
			.invoice .company-details .name {
			margin-top: 0;
			margin-bottom: 0
			}
			
			.invoice .contacts {
			margin-bottom: 20px
			}
			
			.invoice .invoice-to {
			text-align: left
			}
			
			.invoice .invoice-to .to {
			margin-top: 0;
			margin-bottom: 0
			}
			
			.invoice .invoice-details {
			text-align: right
			}
			
			.invoice .invoice-details .invoice-id {
			margin-top: 0;
			color: #3989c6
			}
			
			.invoice main {
			padding-bottom: 50px
			}
			
			.invoice main .thanks {
			margin-top: -100px;
			font-size: 2em;
			margin-bottom: 50px
			}
			
			.invoice main .notices {
			padding-left: 6px;
			border-left: 6px solid #3989c6
			}
			
			.invoice main .notices .notice {
			font-size: 1.2em
			}
			
			.invoice table {
			width: 100%;
			border-collapse: collapse;
			border-spacing: 0;
			margin-bottom: 20px
			}
			
			.invoice table td,.invoice table th {
			padding: 15px;
			background: #eee;
			border-bottom: 1px solid #fff
			}
			
			.invoice table th {
			white-space: nowrap;
			font-weight: 400;
			font-size: 16px
			}
			
			.invoice table td h3 {
			margin: 0;
			font-weight: 400;
			color: #3989c6;
			font-size: 1.2em
			}
			
			.invoice table .qty,.invoice table .total,.invoice table .unit {
			text-align: right;
			font-size: 1.2em
			}
			
			.invoice table .no {
			color: #fff;
			font-size: 1.6em;
			background: #3989c6
			}
			
			.invoice table .unit {
			background: #ddd
			}
			
			.invoice table .total {
			background: #3989c6;
			color: #fff
			}
			
			.invoice table tbody tr:last-child td {
			border: none
			}
			
			.invoice table tfoot td {
			background: 0 0;
			border-bottom: none;
			white-space: nowrap;
			text-align: right;
			padding: 10px 20px;
			font-size: 1.2em;
			border-top: 1px solid #aaa
			}
			
			.invoice table tfoot tr:first-child td {
			border-top: none
			}
			
			.invoice table tfoot tr:last-child td {
			color: #3989c6;
			font-size: 1.4em;
			border-top: 1px solid #3989c6
			}
			
			.invoice table tfoot tr td:first-child {
			border: none
			}
			
			.invoice footer {
			width: 100%;
			text-align: center;
			color: #777;
			border-top: 1px solid #aaa;
			padding: 8px 0
			}
			
			@media print {
			.invoice {
			font-size: 11px!important;
			overflow: hidden!important
			}
			
			.invoice footer {
			position: absolute;
			bottom: 10px;
			page-break-after: always
			}
			
			.invoice>div:last-child {
			page-break-before: always
			}
		}</style>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
	</head>
	<body>
		<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		
		<div id="invoice">
			<div class="invoice overflow-auto">
				<div style="min-width: 600px">
					<header>
						<div class="row">
							<div class="col">
								<a target="_blank" href="{{ route('home') }}">
									<img src="{{ public_path('brown-logo.png') }}">
								</a>
							</div>
							<div class="col company-details">
								<h2 class="name">
									<a target="_blank" href="{{ route('home') }}">
										Public Domain Marching
									</a>
								</h2>
								<div>455 Foggy Heights, AZ 85004, US</div>
								<div>(123) 456-789</div>
								<div>company@example.com</div>
							</div>
						</div>
					</header>
					<main>
						<div class="row contacts">
							<div class="col invoice-to">
								<div class="text-gray-light">INVOICE TO:</div>
								<h2 class="to">{{$order->user->first_name}} {{$order->user->last_name}}</h2>
								<div class="address">{{$order->address}}</div>
								<div class="email"><a href="mailto:{{$order->user->email}}">{{$order->user->email}}</a></div>
							</div>
							<div class="col invoice-details">
								<h1 class="invoice-id">INVOICE 3-2-1</h1>
								<div class="date">Date of Invoice: {{ date('m-d-Y' , strtotime($date ?? $order->created_at)) }}</div>
								@if(@$po_number)
								<div class="date">PO Number: {{ $po_number }}</div>
								@endif
							</div>
						</div>
						<table border="0" cellspacing="0" cellpadding="0">
							<thead>
								<tr>
									<th>#</th>
									<th class="text-left">ARRANGEMENT</th>
									<th class="text-left">ARRANGEMENT DETAILS</th>
									<th class="text-right">TOTAL</th>
								</tr>
							</thead>
							<tbody>
							@foreach($order->products as $i => $checkout)
								<tr>
									<td class="no">{{ $i+1 }}</td>
									<td class="text-left">
										
										<p class="arrangement-value">{{$checkout->arrangementTo->title}}</p>
										@if($checkout->arrangementtype=='wind_arrangment')
										<p class="display-block light-txt">Wind Arrangement Only</p>
										@else
										<p class="display-block light-txt">Wind + Percussion Arrangement</p>
										@endif
										<p class="light-txt">
											Purchase: {{ date('d-m-Y' , strtotime($checkout->created_at)) }}	
										</p>
									</td>
									<td class="text-left">
										@if(@$order->arrangementTo->arrangementTypeTo->title)
										<div class="Instrumentation"> 
											<span class="Instrumentation-value">Arrangement Type:</span>
											<span class="light-txt">{{$order->arrangementTo->arrangementTypeTo->title}}</span>
										</div>
										@endif
										@if(@$order->arrangementTo->difficultyTo->title)
										<div class="Difficulty"> 
											<span class="difficulty-value">Difficulty:</span>
											<span class="light-txt">{{$order->arrangementTo->difficultyTo->title}}</span>
										</div>
										@endif
										@if(@$order->arrangementTo->instrumentationTo->title)
										<div class="Instrumentation"> 
											<span class="Instrumentation-value">Instrumentation:</span>
											<span class="light-txt">{{$order->arrangementTo->instrumentationTo->title}}</span>
										</div>
										@endif
										@if(@$order->arrangementTo->composerTO->title)
										<div class="Composer"> 
											<span class="Composer-Name">Composer Name:</span>
											<span class="light-txt">{{$order->arrangementTo->composerTO->title}}</span>
										</div>
										@endif
										@if(@$order->arrangementTo->windarrangerTo->title)
										<div class="Arranger"> 
											<span class="Arranger-Name">Wind Arranger Name:</span>
											<span class="light-txt">{{$order->arrangementTo->windarrangerTo->title}}</span>
										</div>
										@endif
										@if(@$order->arrangementTo->percussionarrangerTO->title)
										<div class="Arranger"> 
											<span class="Arranger-Name">Percussio Arranger Name:</span>
											<span class="light-txt">{{$order->arrangementTo->percussionarrangerTO->title}}</span>
										</div>
										@endif
									</td>
									<td class="total">
										${{ number_format($checkout->price, 2) }}
									</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<td colspan="2"></td>
									<td>Sub Total</td>
									<td>${{ number_format($order->price, 2) }}</td>
								</tr>
								<tr>
									<td colspan="2"></td>
									<td>Sales Tax(8.25%)</td>
									<td>${{ number_format($order->tax, 2) }}</td>
								</tr>
								@if(@$order->discount)
								<tr>
									<td colspan="2"></td>
									<td>Discount</td>
									<td>{{$order->promocode_type=='fixed'?'$':''}}{{$order->discount}}{{$order->promocode_type=='percentage'?'%':''}}</td>
								</tr>
								@endif
								<tr>
									<td colspan="2"></td>
									<td>TOTAL</td>
									<td>${{ number_format($order->total_price, 2) }}</td>
								</tr>
							</tfoot>
						</table>
						<div class="notices">
							<div>NOTICE:</div>
							<div class="notice">Account Details</div>
						</div>
					</main>
					<footer>
						Invoice was created on a computer and is valid without the signature and seal.
					</footer>
				</div>
			</div>
		</div>
		
	</body>
</html>