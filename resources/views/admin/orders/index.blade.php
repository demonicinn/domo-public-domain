@extends('layouts.app')
@section('content')
<div class="tab-content">
	<table class="table" style="width:100%">
		<thead>
			<tr>
				<th>DATE</th>
				<th>ZIPCODE</th>
				<th>ADDRESS</th>
				<th>City</th>
				<th>State</th>
			</tr>
		</thead>
		<tbody>
			@foreach($orders as $order)
			<tr>
				<td>
					<a href="{{ route('admin.orders.show', $order->id) }}">
					<span class="purchase">Purchase <span class="display-block">
					{{ date('m-d-Y' , strtotime($order->created_at)) }}	
					</span></span>
					</a>
				</td>
				<td>
					<span class="arrangement-value">{{@$order->zipcode}}</span>
				</td>
				<td>
					<span class="arrangement-value">{{@$order->address}}</span>
				</td>
				<td>
					<span class="arrangement-value">{{@$order->city}}</span>
				</td>
				<td>
					<span class="arrangement-value">{{@$order->state}}</span>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	<div class="pagination">
		{{ $orders->links() }}
	</div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="exampleModalLabel">Are you sure?</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			{!! Form::model(['route' => ['paymentCancel'], 'method' => 'POST']) !!}
				<div class="modal-body">
				<h5 class="modal-title" id="exampleModalLabel">Are you sure to cancel this order?</h5>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
					<button type="submit" class="btn btn-success">Yes</button>
			
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

@endsection