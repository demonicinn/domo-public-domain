@extends('layouts.app')
@section('content')
<div class="tab-content">
	<table class="table" style="width:100%">
		<thead>
			<tr>
				<th>DATE</th>
				<th>ZIPCODE</th>
				<th>ADDRESS</th>
				<th>City</th>
				<th>State</th>
				<th>ACTION</th>
			</tr>
		</thead>
		<tbody>
			@foreach($orders as $order)
			<tr>
				<td>
					<a href="{{ route('admin.purchase_orders.show', $order->id) }}">
					<span class="purchase">Purchase <span class="display-block">
					{{ date('m-d-Y' , strtotime($order->created_at)) }}	
					</span></span>
					</a>
				</td>
				<td>
					<span class="arrangement-value">{{@$order->zipcode}}</span>
				</td>
				<td>
					<span class="arrangement-value">{{@$order->address}}</span>
				</td>
				<td>
					<span class="arrangement-value">{{@$order->city}}</span>
				</td>
				<td>
					<span class="arrangement-value">{{@$order->state}}</span>
				</td>
				
				<td>
					
						<a class="btn brown-btn mt-2" target="_blank" href="{{ route('admin.purchase_orders.invoice', $order->id) }}">VIEW INVOICE</a>
						
						@if($order->status=='purchase_orders')
							<a class="btn brown-btn mt-2" href="javascript:void(0)" onclick="event.preventDefault(); document.getElementById('payment_completed_{{$order->id}}').submit();">COMPLETE PAYMENT</a>
							<form id="payment_completed_{{$order->id}}" action="{{ route('admin.purchase_orders.payment_completed') }}" method="POST" style="display: none;">
								<input type="hidden" name="order_id" value="{{$order->id}}">
								@csrf
							</form>						
						@else
							<span class="btn btn-success">PAYMENT COMPLETED</span>
						@endif
					{{--
					@if($order->invoice_generated=='1')
					@else
						<a class="btn brown-btn mt-2" href="javascript:void(0)" onclick="event.preventDefault(); document.getElementById('invoice_generated_{{$order->id}}').submit();">GENERATE INVOICE</a>
						<form id="invoice_generated_{{$order->id}}" action="{{ route('admin.purchase_orders.generate') }}" method="POST" style="display: none;">
							<input type="hidden" name="order_id" value="{{$order->id}}">
							@csrf
						</form>
					@endif
					--}}
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	<div class="pagination">
		{{ $orders->links() }}
	</div>
</div>

@endsection