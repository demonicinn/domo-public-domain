@extends('layouts.app')
@section('content')
<section class="form-section account-section">
	<div class="container-1315">
		<div class="tab-wrappers">
			
			<div class="tab-content">
				<div class="form-box-inner max-555">
					{!! Form::open(['route' => ['admin.profile.passwordUpdate']]) !!}
					
					<div class="form-grouph input-design {!! ($errors->has('current-password') ? 'has-error' : '') !!}">
						{!! Form::label('current-password','Current Password', ['class' => 'control-label']) !!}
						<input name="current-password" type="password" class="form-control {!! ($errors->has('current-password') ? ' is-invalid' : '') !!}" id="current-password">
						{!! $errors->first('current-password', '<span class="help-block">:message</span>') !!}
					</div>
					
					<div class="form-grouph input-design {!! ($errors->has('new-password') ? 'has-error' : '') !!}">
						{!! Form::label('new-password','New Password', ['class' => 'control-label']) !!}
						<input name="new-password" type="password" class="form-control {!! ($errors->has('new-password') ? ' is-invalid' : '') !!}" id="new-password">
						{!! $errors->first('new-password', '<span class="help-block">:message</span>') !!}
					</div>
					
					<div class="form-grouph input-design {!! ($errors->has('new-password_confirmed') ? 'has-error' : '') !!}">
						{!! Form::label('new-password_confirmed','Confirm Password', ['class' => 'control-label']) !!}
						<input name="new-password_confirmed" type="password" class="form-control {!! ($errors->has('new-password_confirmed') ? ' is-invalid' : '') !!}" id="new-password_confirmed">
						{!! $errors->first('new-password_confirmed', '<span class="help-block">:message</span>') !!}
					</div>
						
						<div class="form-grouph submit-design twobtns-flex">
							<button type="submit" class="submit-btn">UPDATE</button>
						</div>
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection