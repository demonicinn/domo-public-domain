@extends('layouts.app')
@section('content')
<section class="form-section account-section">
	<div class="container-1315">
		<div class="tab-wrappers">
			<div class="tab-content">
				<div class="form-box-inner">
					{!! Form::model($user, ['route' => 'admin.profile.update']) !!}
						
						<div class="form-grouph input-design {!! ($errors->has('first_name') ? 'has-error' : '') !!}">
							{!! Form::label('first_name','First Name', ['class' => 'control-label']) !!}
							{!! Form::text('first_name', null, ['class' => 'form-control' . ($errors->has('first_name') ? ' is-invalid' : '') ]) !!}
							{!! $errors->first('first_name', '<span class="help-block">:message</span>') !!}
						</div>
						
						<div class="form-grouph input-design {!! ($errors->has('last_name') ? 'has-error' : '') !!}">
							{!! Form::label('last_name','Last Name', ['class' => 'control-label']) !!}
							{!! Form::text('last_name', null, ['class' => 'form-control' . ($errors->has('last_name') ? ' is-invalid' : '') ]) !!}
							{!! $errors->first('last_name', '<span class="help-block">:message</span>') !!}
						</div>						
						
						<div class="form-grouph input-design {!! ($errors->has('email') ? 'has-error' : '') !!}">
							{!! Form::label('email','Email', ['class' => 'control-label']) !!}
							{!! Form::email('email', null, ['class' => 'form-control' . ($errors->has('email') ? ' is-invalid' : '') ]) !!}
							{!! $errors->first('email', '<span class="help-block">:message</span>') !!}
						</div>						
						
						<div class="form-grouph submit-design twobtns-flex">
							<a href="{{ route('admin.profile.password') }}" class="submit-btn btn-bordered">Change Password</a>
							<button type="submit" class="submit-btn">UPDATE</button>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</section>
@endsection