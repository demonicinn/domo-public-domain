@extends('layouts.app')
@section('content')

<section class="form-section account-section">
	<div class="container-1315">
		<div class="tab-wrappers">
			<div class="tab-content">
				<div class="form-box-inner max-555">
					{!! Form::open(['route' => 'admin.wind-arranger.store']) !!}
					
					@include('admin.windarranger.partials.form')
					
					<div class="form-grouph submit-design twobtns-flex">
						<button class="submit-btn">Submit</button>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</section>
	
@endsection	