@extends('layouts.app')
@section('content')

<section class="form-section account-section">
	<div class="pull-right">
		<a href="{{ route('admin.wind-arranger.index') }}" title="Back" class="btn btn-warning btn-icon pull-right"><i class="fa fa-arrow-left"></i></a>
	</div>
	<div class="container-1315">
		<div class="tab-wrappers">
			<div class="tab-content">
				<div class="form-box-inner max-555">
					{!! Form::model($windarranger, ['route' => ['admin.wind-arranger.update', $windarranger->id], 'method' => 'PATCH', ]) !!}
					
					@include('admin.windarranger.partials.form')
					
					<div class="form-grouph submit-design twobtns-flex">
						<button class="submit-btn">Update</button>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
			</div>
	</div>
</section>

@endsection