@extends('layouts.app')
@section('content')

<section class="user-information-section">
	<div class="container-1350">
		
		<div class="user-informtion-table tab-wrappers" id="table-show-sorting">	
			<table class="table" style="width:100%">
                <thead>
                    <tr>
                        <th>Sr. No.</th>
                        <th>Name</th>
                        <th>Tax</th>
                        <th>Action</th>
					</tr>
				</thead>
                <tbody>	
					@foreach($states as $i => $state)
					<tr>
						<td>{{ $i+1 }}</td>
						<td>{{ $state->state_name }}</td>
						<td>{{ $state->tax }}%</td>
						<td>
							<div class="btn-icon-list">
								<a href="javascript:void(0)" class="btn btn-warning btn-icon" data-toggle="modal" data-target="#editState_{{$state->id}}">
									<i class="fa fa-edit"></i>
								</a>
							</div>
							
							<div id="editState_{{$state->id}}" class="modal fade" role="dialog">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<h4 class="modal-title">Edit State Tax</h4>
											<button type="button" class="close" data-dismiss="modal">&times;</button>
										</div>
										
										{!! Form::model($state, ['route' => ['admin.states.update', $state->id], 'method' => 'PATCH', ]) !!}
										<div class="modal-body">
											<div class="form-grouph input-design {!! ($errors->has('state_name') ? 'has-error' : '') !!}">
												{!! Form::label('state_name','State Name', ['class' => 'form-label']) !!}
												{!! Form::text('state_name', null, ['class' => 'form-control' . ($errors->has('state_name') ? ' is-invalid' : ''), 'readonly' ]) !!}
												{!! $errors->first('state_name', '<span class="help-block">:message</span>') !!}
											</div>
											
											<div class="form-grouph input-design {!! ($errors->has('tax') ? 'has-error' : '') !!}">
												{!! Form::label('tax','Tax %', ['class' => 'form-label']) !!}
												{!! Form::text('tax', null, ['class' => 'form-control' . ($errors->has('tax') ? ' is-invalid' : ''), 'required'=>'required' ]) !!}
												{!! $errors->first('tax', '<span class="help-block">:message</span>') !!}
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											<button type="submit" class="btn btn-warning">Update</button>
										</div>
										{!! Form::close() !!}
									</div>									
								</div>
							</div>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="pagination">
			{{ $states->links() }}
		</div>
	</div>
</section>

@endsection			