@extends('layouts.app')
@section('content')

<section class="form-section account-section">
	<div class="pull-right">
		<a href="{{ route('admin.promocode.index') }}" title="Back" class="btn btn-warning btn-icon pull-right"><i class="fa fa-arrow-left"></i></a>
	</div>
	<div class="container-1315">
		<div class="tab-wrappers">
			<div class="tab-content">
				<div class="form-box-inner max-555">
					{!! Form::model($code, ['route' => ['admin.promocode.update', $code->id], 'method' => 'PATCH', ]) !!}
					
					@include('admin.promocode.partials.form')
					
					<div class="form-grouph submit-design twobtns-flex">
						<button type="submit" class="submit-btn">Update</button>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
			</div>
	</div>
</section>

@endsection