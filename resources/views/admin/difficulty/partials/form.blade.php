<div class="form-grouph input-design {!! ($errors->has('title') ? 'has-error' : '') !!}">
	{!! Form::text('title', null, ['class' => 'form-control' . ($errors->has('title') ? ' is-invalid' : ''), 'placeholder' => 'Title']) !!}
	{!! $errors->first('title', '<span class="help-block">:message</span>') !!}
	</div>

<div class="form-grouph {!! ($errors->has('status') ? 'has-error' : '') !!}">
	{!! Form::label('status','Status', ['class' => 'form-label']) !!}
	<div class="row">
		<div class="col-md-2">
			{!! Form::radio('status', '1') !!} Active
		</div>
		<div class="col-md-3">
			{!! Form::radio('status', '0') !!} De-active
		</div>
	</div>
	</br>
	{!! $errors->first('status', '<span class="help-block">:message</span>') !!}
</div>

