<div class="form-grouph input-design {!! ($errors->has('title') ? 'has-error' : '') !!}">
	{!! Form::label('title','Title') !!}
	{!! Form::text('title', null, ['class' => 'form-control' . ($errors->has('title') ? ' is-invalid' : ''), 'placeholder' => 'Title']) !!}
	{!! $errors->first('title', '<span class="help-block">:message</span>') !!}
</div>


<div class="form-flex mrg-t-57">
	<div class="form-grouph label-design">
		<p>Upload Image File</p>
	</div>
	<div class="inner-uload-btn form-grouph input-design">
		{!! Form::hidden('image', @$arrangementtype ? $arrangementtype->image : null) !!}
		{!! Form::file(null, ['class' => 'fileUpload', 'accept' => 'image/*',  'data-name' => 'image' ]) !!}
		<button class="uploadbtn">UPLOAD IMAGE</button>
		<div class="file">
			@if(@$arrangementtype->image)
				<img target="_blank" class="imageContent" src="{{ asset('storage/uploads/arrangementstype/'.$arrangementtype->image) }}"  />
			@endif
		</div>
		{!! $errors->first('image', '<span class="help-block">:message</span>') !!}
	</div>
</div>

<div class="form-grouph {!! ($errors->has('status') ? 'has-error' : '') !!}">
	{!! Form::label('status','Status', ['class' => 'form-label']) !!}
	<div class="row">
		<div class="col-md-2">
			{!! Form::radio('status', '1') !!} Active
		</div>
		<div class="col-md-3">
			{!! Form::radio('status', '0') !!} De-active
		</div>
	</div>
	</br>
	{!! $errors->first('status', '<span class="help-block">:message</span>') !!}
</div>

@section('script')
<script>
	var base_url = jQuery('meta[name="site-url"]').attr('content');
	
	var postUrl = "{{ route('admin.upload') }}";
	var img_url = base_url+'/storage/uploads/arrangementstype/';
	
	jQuery(document).on('change', '.fileUpload', function (event) {
		var $this = jQuery(this);
		var data_name = $this.attr('data-name');
		
		var formData = new FormData();
		formData.append("file", this.files[0]);
		formData.append("path", "arrangementstype");
		formData.append("name", data_name);
		
		ajaxRequest(formData, postUrl, function(res) {
			//processing the data
			$this.val('');
			if(res.response===true){
				$('input[name='+data_name+']').val(res.filename);
				
				var file = img_url + res.filename;
				if(data_name=='image'){
					$this.parent('.inner-uload-btn').find('.file').html('\
					<img target="_blank" class="imageContent"  src="'+file+'" />\
					');
				}
				else {
					$this.parent('.inner-uload-btn').find('.file').html('\
					<a target="_blank" href="'+file+'">'+res.filename+'</a>\
					');
				}
			}
		});
		
	})
	
	
</script>
<script src="{{ asset('js/file_upload.js') }}"></script>
@endsection