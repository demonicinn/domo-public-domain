@extends('layouts.app')
@section('content')

<section class="form-section account-section">
	<div class="pull-right">
		<a href="{{ route('admin.users') }}" title="Back" class="btn btn-warning btn-icon pull-right"><i class="fa fa-arrow-left"></i></a>
	</div>
	<div class="container-1315">
		<div class="tab-wrappers">
			<div class="tab-content">
				<div class="form-box-inner max-555">
					{!! Form::model($user, ['route' => ['admin.users.update', $user->id], 'method' => 'POST', ]) !!}
					
					<div class="form-grouph input-design {!! ($errors->has('first_name') ? 'has-error' : '') !!}">
						{!! Form::label('first_name','First Name', ['class' => 'control-label']) !!}
						{!! Form::text('first_name', null, ['class' => 'form-control' . ($errors->has('first_name') ? ' is-invalid' : '') ]) !!}
						{!! $errors->first('first_name', '<span class="help-block">:message</span>') !!}
					</div>
					
					<div class="form-grouph input-design {!! ($errors->has('last_name') ? 'has-error' : '') !!}">
						{!! Form::label('last_name','Last Name', ['class' => 'control-label']) !!}
						{!! Form::text('last_name', null, ['class' => 'form-control' . ($errors->has('last_name') ? ' is-invalid' : '') ]) !!}
						{!! $errors->first('last_name', '<span class="help-block">:message</span>') !!}
					</div>
					
					
					<div class="form-grouph input-design {!! ($errors->has('email') ? 'has-error' : '') !!}">
						{!! Form::label('email','Email', ['class' => 'control-label']) !!}
						{!! Form::text('email', null, ['class' => 'form-control' . ($errors->has('email') ? ' is-invalid' : ''), 'readonly' ]) !!}
						{!! $errors->first('email', '<span class="help-block">:message</span>') !!}
					</div>
					
					<div class="form-grouph input-design {!! ($errors->has('phone_number') ? 'has-error' : '') !!}">
						{!! Form::label('phone_number','Phone Number', ['class' => 'control-label']) !!}
						{!! Form::number('phone_number', null, ['class' => 'form-control' . ($errors->has('phone_number') ? ' is-invalid' : ''), 'readonly' ]) !!}
						{!! $errors->first('phone_number', '<span class="help-block">:message</span>') !!}
					</div>
					
					
					<div class="form-flex">
						<div class="form-grouph input-design {!! ($errors->has('zipcode') ? 'has-error' : '') !!}">
							{!! Form::label('zipcode','Zip Code', ['class' => 'control-label']) !!}
							{!! Form::number('zipcode', null, ['class' => 'form-control' . ($errors->has('zipcode') ? ' is-invalid' : '') ]) !!}
							{!! $errors->first('zipcode', '<span class="help-block">:message</span>') !!}
						</div>
						
						<div class="form-grouph input-design {!! ($errors->has('school') ? 'has-error' : '') !!}">
							{!! Form::label('school','School', ['class' => 'control-label']) !!}
							{!! Form::text('school', null, ['class' => 'form-control' . ($errors->has('school') ? ' is-invalid' : '') ]) !!}
							{!! $errors->first('school', '<span class="help-block">:message</span>') !!}
						</div>
					</div>
					
					<div class="form-grouph {!! ($errors->has('status') ? 'has-error' : '') !!}">
						{!! Form::label('status','Status', ['class' => 'form-label']) !!}
						<div class="row">
							<div class="col-md-2">
								{!! Form::radio('status', '1') !!} Active
							</div>
							<div class="col-md-3">
								{!! Form::radio('status', '0') !!} De-active
							</div>
							<div class="col-md-3">
								{!! Form::radio('status', '2') !!} Block
							</div>
						</div>
						</br>
						{!! $errors->first('status', '<span class="help-block">:message</span>') !!}
					</div>
					
					
					
					<div class="form-grouph submit-design twobtns-flex">
						<button class="submit-btn">Update</button>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</section>

@endsection