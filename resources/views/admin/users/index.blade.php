@extends('layouts.app')
@section('content')
<section class="user-information-section">
	<div class="container-1350">
		{!! Form::open(['route' => 'admin.users', 'method'=>'get']) !!}
		<div class="row">
			<div class="col-md-4 form-group">
				{!! Form::text('search', request()->search ?? null, ['class' => 'form-control' . ($errors->has('search') ? ' is-invalid' : ''), 'placeholder'=>'Search' ]) !!}
			</div>
			<div class="col-md-3 form-group">
				{!! Form::select('status', ['1'=>'Active', '0'=>'De-active', '2'=>'Block'], request()->status ?? null, ['class' => 'form-control' . ($errors->has('status') ? ' is-invalid' : ''), 'placeholder'=>'Select status' ]) !!}
			</div>
			<div class="col-md-3 form-group">
				{!! Form::select('archive', ['0'=>'No', '1'=>'Yes'], request()->archive ?? null, ['class' => 'form-control' . ($errors->has('archive') ? ' is-invalid' : ''), 'placeholder'=>'Archive' ]) !!}
			</div>
			<div class="col-md-2 form-group">
				<button class="btn btn-primary" type="submit">Search</button>
			</div>
		</div>
		{!! Form::close() !!}
		
		<div class="user-informtion-table tab-wrappers" id="table-show-sorting">
			<table class="table" style="width:100%">
                <thead>
                    <tr>
                        <th>NAME</th>
                        <th>ZIP CODE</th>
                        <th>SCHOOL NAME</th>
                        <th>Status</th>
                        <th>Action</th>
					</tr>
				</thead>
                <tbody>
					@foreach($users as $i => $user)
						<tr>
							<td>{{$user->first_name}} {{$user->last_name}}</td>
							<td>{{$user->zipcode}}</td>
							<td>{{$user->school}}</td>
							<td>
								@if(@$user->status=='1')
								<span class="label label-success">Active</span>
								@elseif(@$user->status=='2')
								<span class="label label-danger">Block</span>
								@else
								<span class="label label-warning">De-active</span>
								@endif
							</td>
							<td>
								<a href="{{ route('admin.users.edit', $user->id) }}" class="btn btn-warning btn-icon">
								<i class="fa fa-edit"></i>
								</a>
								@if($user->is_delete=='1')
								<a data-method="Delete" data-confirm="Are you sure to retrieve?" href="{{ route('admin.users.destroy', $user->id) }}" class="btn btn-danger btn-icon">
									<i class="fa fa-reply"></i>
								</a>
								@else
								<a data-method="Delete" data-confirm="Are you sure to delete?" href="{{ route('admin.users.destroy', $user->id) }}" class="btn btn-danger btn-icon">
									<i class="fa fa-trash"></i>
								</a>
								@endif
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="pagination">
			@if(@request()->search || request()->status || request()->archive)
				{{ $users->appends([
					'search' => request()->search,
					'status' => request()->status,
					'archive' => request()->archive,
				])->links() }}
			@else
			{{ $users->links() }}
			@endif
		</div>
	</div>
</section>
@endsection