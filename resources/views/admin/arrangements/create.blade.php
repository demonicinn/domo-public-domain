@extends('layouts.app')
@section('content')

<section class="form-section edit-arrangements-section">
	<div class="container">
		<div class="form-box-inner max-555">
			{!! Form::open(['route' => 'admin.arrangements.store', 'enctype' => 'multipart/form-data']) !!}
				
				@include('admin.arrangements.partials.form')
				
				<div class="form-grouph submit-design twobtns-flex flex-column-center">
					<button type="submit" class="submit-btn">CREATE</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</section>
@endsection					