@extends('layouts.app')
@section('content')

<section class="form-section edit-arrangements-section">
	<div class="pull-right">
		<a href="{{ route('admin.arrangements.index') }}" title="Back" class="btn btn-warning btn-icon pull-right"><i class="fa fa-arrow-left"></i></a>
	</div>
	<div class="container">
		<div class="form-box-inner max-555">
			{!! Form::model($arrangement, ['route' => ['admin.arrangements.update', $arrangement->id], 'method' => 'PATCH', 'enctype' => 'multipart/form-data']) !!}
				
				@include('admin.arrangements.partials.form')
				
				<div class="form-grouph submit-design twobtns-flex flex-column-center">
					  <button type="submit" class="submit-btn btn-bordered">UPDATE</button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</section>
@endsection					