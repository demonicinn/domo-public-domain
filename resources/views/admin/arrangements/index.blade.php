@extends('layouts.app')
@section('content')

<section class="form-section edit-arrangements-section">
	<div class="container">
		{!! Form::open(['route' => 'admin.arrangements.index', 'method'=>'get']) !!}
		<div class="row">
			<div class="col-lg-3 col-md-6 form-group">
				{!! Form::text('search', request()->search ?? null, ['class' => 'form-control' . ($errors->has('search') ? ' is-invalid' : ''), 'placeholder'=>'Search' ]) !!}
			</div>
			<div class="col-lg-3 col-md-6 form-group">
				{!! Form::select('status', ['1'=>'Active', '0'=>'De-active'], request()->status ?? null, ['class' => 'form-control' . ($errors->has('status') ? ' is-invalid' : ''), 'placeholder'=>'Select status' ]) !!}
			</div>
			<div class="col-lg-2 col-md-4 form-group">
				{!! Form::select('archive', ['0'=>'No', '1'=>'Yes'], request()->archive ?? null, ['class' => 'form-control' . ($errors->has('archive') ? ' is-invalid' : ''), 'placeholder'=>'Archive' ]) !!}
			</div>
			<div class="col-lg-2 col-md-4 form-group">
				<button class="btn btn-primary" type="submit">Search</button>
			</div>
			<div class="col-lg-2 col-md-4 form-group">
				<a href="{{ route('admin.arrangements.create') }}" title="Add" class="btn btn-success"><i class="fa fa-plus"></i> Add</a>
				</div>
		</div>
		{!! Form::close() !!}
		
		<div class="user-informtion-table tab-wrappers arrangment-spacing-td" id="table-show-sorting">
			<table class="table" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Wind Arrangements Only</th>
                        <th>Wind + Percussion Arrangements</th>
                        <th>Status</th>
                        <th>Featured</th>
                        <th>Action</th>
					</tr>
				</thead>
                <tbody>
				@foreach($arrangements as $i => $arrangement)
				<tr>
					<td>{{ $i+1 }}</td>
					<td class="arrangment-TitlE">{{$arrangement->title}}</td>
					<td>${{$arrangement->wind_arrangment}}</td>
					<td>${{$arrangement->percussion_arrangment}}</td>
					<td>
						@if(@$arrangement->status=='1')
						<span class="label label-success">Active</span>
						@else
						<span class="label label-danger">De-active</span>
						@endif
					</td>
					<td>
						@if(@$arrangement->is_featured=='1')
						<span class="label label-success">Yes</span>
						@endif
					</td>
					<td>
						<div class="btn-icon-list">
							<a href="{{ route('admin.arrangements.edit', $arrangement->id) }}" class="btn btn-warning btn-icon">
							<i class="fa fa-edit"></i>
							</a>
							
							@if($arrangement->is_delete=='1')
							<a data-method="Delete" data-confirm="Are you sure to retrieve?" href="{{ route('admin.arrangements.destroy', $arrangement->id) }}" class="btn btn-danger btn-icon">
								<i class="fa fa-reply"></i>
							</a>
							@else
							<a data-method="Delete" data-confirm="Are you sure to delete?" href="{{ route('admin.arrangements.destroy', $arrangement->id) }}" class="btn btn-danger btn-icon">
								<i class="fa fa-trash"></i>
							</a>
							@endif
						</div>
					</td>
				</tr>
				@endforeach
				</tbody>
			</table>
		</div>
		<div class="pagination">
			@if(@request()->search || request()->status || request()->archive)
				{{ $arrangements->appends([
					'search' => request()->search,
					'status' => request()->status,
					'archive' => request()->archive,
				])->links() }}
			@else
				{{ $arrangements->links() }}
			@endif
		</div>
	</div>
</section>
@endsection					