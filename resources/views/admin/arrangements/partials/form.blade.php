<div class="form-grouph input-design {!! ($errors->has('title') ? 'has-error' : '') !!}">
	{!! Form::text('title', null, ['class' => 'form-control' . ($errors->has('title') ? ' is-invalid' : ''), 'placeholder'=>'Title' ]) !!}
	{!! $errors->first('title', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-grouph textarea-design {!! ($errors->has('description') ? 'has-error' : '') !!}">
	{!! Form::textarea('description', null, ['class' => 'form-control' . ($errors->has('description') ? ' is-invalid' : ''), 'placeholder'=>'Product Description', 'rows'=>3 ]) !!}
	{!! $errors->first('description', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-flex">
	<div class="form-grouph select-design{!! ($errors->has('difficulty') ? ' has-error' : '') !!}">
		{!! Form::select('difficulty', $difficulties, null, ['class' => ($errors->has('difficulty') ? ' is-invalid' : ''), 'placeholder'=>'Select Difficulty' ]) !!}
		{!! $errors->first('difficulty', '<span class="help-block">:message</span>') !!}
	</div>
	<div class="form-grouph select-design{!! ($errors->has('instrumentation') ? ' has-error' : '') !!}">
		{!! Form::select('instrumentation', $instrumentations, null, ['class' =>  ($errors->has('instrumentation') ? ' is-invalid' : ''), 'placeholder'=>'Select Instrumentation' ]) !!}
		{!! $errors->first('instrumentation', '<span class="help-block">:message</span>') !!}
	</div>
</div>
<div class="form-flex">
	<div class="form-grouph select-design{!! ($errors->has('composer') ? ' has-error' : '') !!}">
		{!! Form::select('composer', $composers, null, ['class' => ($errors->has('composer') ? ' is-invalid' : ''), 'placeholder'=>'Select Composer' ]) !!}
		{!! $errors->first('composer', '<span class="help-block">:message</span>') !!}
	</div>
	
	<div class="form-grouph select-design{!! ($errors->has('windarranger') ? ' has-error' : '') !!}">
		{!! Form::select('windarranger', $wind_arrangers, null, ['class' => ($errors->has('windarranger') ? ' is-invalid' : ''), 'placeholder'=>'Select  Wind Arranger' ]) !!}
		{!! $errors->first('windarranger', '<span class="help-block">:message</span>') !!}
	</div>
</div>
<div class="form-flex">
	<div class="form-grouph select-design{!! ($errors->has('percussionarranger') ? ' has-error' : '') !!}">
		{!! Form::select('percussionarranger', $percussion_arrangers, null, ['class' => ($errors->has('percussionarranger') ? ' is-invalid' : ''), 'placeholder'=>'Select  Percussion Arranger' ]) !!}
		{!! $errors->first('percussionarranger', '<span class="help-block">:message</span>') !!}
	</div>
	
	<div class="form-grouph select-design{!! ($errors->has('arrangementtype') ? ' has-error' : '') !!}">
		{!! Form::select('arrangementtype', $arrangementtypes, null, ['class' => ($errors->has('arrangementtype') ? ' is-invalid' : ''), 'placeholder'=>'Select  Arrangement Type' ]) !!}
		{!! $errors->first('arrangementtype', '<span class="help-block">:message</span>') !!}
	</div>
</div>

<div class="form-grouph input-design {!! ($errors->has('date') ? 'has-error' : '') !!}">
	{!! Form::label('date', 'Expiry Date') !!}
	{!! Form::date('date', null, ['class' => 'form-control' . ($errors->has('date') ? ' is-invalid' : ''), 'placeholder'=>'Expire Date' ]) !!}
	{!! $errors->first('date', '<span class="help-block">:message</span>') !!}
</div>
	
<div class="form-flex">
	<div class="form-grouph label-design">
		<p>Wind Arrangements Only</p>
	</div>
	<div class="form-grouph input-design {!! ($errors->has('wind_arrangment') ? 'has-error' : '') !!}">
		{!! Form::number('wind_arrangment', null, ['class' => 'form-control' . ($errors->has('wind_arrangment') ? ' is-invalid' : ''), 'placeholder'=>'$', 'min'=>'1' ]) !!}
		{!! $errors->first('wind_arrangment', '<span class="help-block">:message</span>') !!}
	</div>
</div>
<div class="form-grouph upload-btn upload-btn-flex max-365">
	<div class="inner-uload-btn">
		{!! Form::hidden('wpdf', @$arrangement ? $arrangement->wpdf : null) !!}
		{!! Form::file(null, ['class' => 'fileUpload', 'accept' => 'application/pdf',  'data-name' => 'wpdf' ]) !!}
		<button class="uploadbtn">UPLOAD PDF FILE</button>
		<div class="file">
			@if(@$arrangement->wpdf)
			<a target="_blank" href="{{ asset('storage/uploads/arrangements/'.$arrangement->wpdf) }}">{{$arrangement->wpdf}}</a>
			@endif
		</div>
		{!! $errors->first('wpdf', '<span class="help-block">:message</span>') !!}
	</div>
	<div class="inner-uload-btn">
		{!! Form::hidden('wmp3', @$arrangement ? $arrangement->wmp3 : null) !!}
		{!! Form::file(null, ['class' => 'fileUpload', 'accept' => '.mp3',  'data-name' => 'wmp3' ]) !!}
		<button class="uploadbtn">UPLOAD MP3 SOUND FILE</button>
		<div class="file">
			@if(@$arrangement->wmp3)
			<a target="_blank" href="{{ asset('storage/uploads/arrangements/'.$arrangement->wmp3) }}">{{$arrangement->wmp3}}</a>
			@endif
		</div>
		{!! $errors->first('wmp3', '<span class="help-block">:message</span>') !!}
	</div>
</div>
<div class="form-flex">
	<div class="form-grouph label-design">
		<p>Wind + Percussion Arrangements</p>
	</div>
	<div class="form-grouph input-design {!! ($errors->has('percussion_arrangment') ? 'has-error' : '') !!}">
		{!! Form::number('percussion_arrangment', null, ['class' => 'form-control' . ($errors->has('percussion_arrangment') ? ' is-invalid' : ''), 'placeholder'=>'$', 'min'=>'1' ]) !!}
		{!! $errors->first('percussion_arrangment', '<span class="help-block">:message</span>') !!}
	</div>
</div>
<div class="form-grouph upload-btn upload-btn-flex">
	<div class="inner-uload-btn">
		{!! Form::hidden('wppdf', @$arrangement ? $arrangement->wppdf : null) !!}
		{!! Form::file(null, ['class' => 'fileUpload', 'accept' => 'application/pdf',  'data-name' => 'wppdf' ]) !!}
		<button class="uploadbtn">UPLOAD PDF FILE</button>
		<div class="file">
			@if(@$arrangement->wppdf)
			<a target="_blank" href="{{ asset('storage/uploads/arrangements/'.$arrangement->wppdf) }}">{{$arrangement->wppdf}}</a>
			@endif
		</div>
		{!! $errors->first('wppdf', '<span class="help-block">:message</span>') !!}
	</div>
	<div class="inner-uload-btn">
		{!! Form::hidden('wpmp3', @$arrangement ? $arrangement->wpmp3 : null) !!}
		{!! Form::file(null, ['class' => 'fileUpload', 'accept' => '.mp3',  'data-name' => 'wpmp3' ]) !!}
		<button class="uploadbtn">UPLOAD MP3 SOUND FILE</button>
		<div class="file">
			@if(@$arrangement->wpmp3)
			<a target="_blank" href="{{ asset('storage/uploads/arrangements/'.$arrangement->wpmp3) }}">{{$arrangement->wpmp3}}</a>
			@endif
		</div>
		{!! $errors->first('wpmp3', '<span class="help-block">:message</span>') !!}
	</div>
	{{--
	<div class="inner-uload-btn">
		{!! Form::hidden('wpzip', @$arrangement ? $arrangement->wpzip : null) !!}
		{!! Form::file(null, ['class' => 'fileUpload', 'accept' => '.zip',  'data-name' => 'wpzip' ]) !!}
		<button class="uploadbtn">UPLOAD ZIP FILE</button>
		<div class="file">
			@if(@$arrangement->wpzip)
			<a target="_blank" href="{{ asset('storage/uploads/arrangements/'.$arrangement->wpzip) }}">{{$arrangement->wpzip}}</a>
			@endif
		</div>
		{!! $errors->first('wpzip', '<span class="help-block">:message</span>') !!}
	</div>
	--}}
</div>

<div class="form-flex">
	<div class="form-grouph label-design">
		<p>Upload Image File</p>
	</div>
	<div class="inner-uload-btn form-grouph input-design">
		{!! Form::hidden('image', @$arrangement ? $arrangement->image : null) !!}
		{!! Form::file(null, ['class' => 'fileUpload', 'accept' => 'image/*',  'data-name' => 'image' ]) !!}
		<button class="uploadbtn">UPLOAD IMAGE</button>
		<div class="file">
			@if(@$arrangement->image)
				<img target="_blank" class="imageContent" src="{{ asset('storage/uploads/arrangements/'.$arrangement->image) }}"  />
			@endif
		</div>
		{!! $errors->first('wpzip', '<span class="help-block">:message</span>') !!}
	</div>
</div>

<div class="form-group {!! ($errors->has('is_featured') ? 'has-error' : '') !!}">
	<input name="is_featured" type="hidden" value="0">
	<input name="is_featured" type="checkbox" value="1" {{ @($arrangement->is_featured=='1') ? 'checked' : '' }}>
	{!! Form::label('is_featured','Is Featured', ['class' => 'control-label']) !!}
	</br>
    {!! $errors->first('is_featured', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-grouph {!! ($errors->has('status') ? 'has-error' : '') !!}">
	{!! Form::label('status','Status', ['class' => 'form-label']) !!}
	<div class="row">
		<div class="col-md-2">
			{!! Form::radio('status', '1') !!} Active
		</div>
		<div class="col-md-3">
			{!! Form::radio('status', '0') !!} De-active
		</div>
	</div>
	</br>
	{!! $errors->first('status', '<span class="help-block">:message</span>') !!}
</div>

@section('script')
<script>
	var base_url = jQuery('meta[name="site-url"]').attr('content');
	
	var postUrl = "{{ route('admin.upload') }}";
	var img_url = base_url+'/storage/uploads/arrangements/';
	
	jQuery(document).on('change', '.fileUpload', function (event) {
		var $this = jQuery(this);
		var data_name = $this.attr('data-name');
		
		var formData = new FormData();
		formData.append("file", this.files[0]);
		formData.append("path", "arrangements");
		formData.append("name", data_name);
		
		ajaxRequest(formData, postUrl, function(res) {
			//processing the data
			$this.val('');
			if(res.response===true){
				$('input[name='+data_name+']').val(res.filename);
				
				var file = img_url + res.filename;
				if(data_name=='image'){
					$this.parent('.inner-uload-btn').find('.file').html('\
					<img target="_blank" class="imageContent"  src="'+file+'" />\
					');
				}
				else {
					$this.parent('.inner-uload-btn').find('.file').html('\
					<a target="_blank" href="'+file+'">'+res.filename+'</a>\
					');
				}
			}
		});
		
	})
	
	
</script>
<script src="{{ asset('js/file_upload.js') }}"></script>
@endsection