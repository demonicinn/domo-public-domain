@extends('layouts.app')
@section('content')

<section class="user-information-section">
	<div class="container-1350">
		{!! Form::open(['route' => 'admin.composer.index', 'method'=>'get']) !!}
		<div class="row">
			<div class="col-lg-3 col-md-6 form-group">
				{!! Form::text('search', request()->search ?? null, ['class' => 'form-control' . ($errors->has('search') ? ' is-invalid' : ''), 'placeholder'=>'Search' ]) !!}
			</div>
			<div class="col-lg-3 col-md-6 form-group">
				{!! Form::select('status', ['1'=>'Active', '0'=>'De-active'], request()->status ?? null, ['class' => 'form-control' . ($errors->has('status') ? ' is-invalid' : ''), 'placeholder'=>'Select status' ]) !!}
			</div>
			<div class="col-lg-2 col-md-4 form-group">
				{!! Form::select('archive', ['0'=>'No', '1'=>'Yes'], request()->archive ?? null, ['class' => 'form-control' . ($errors->has('archive') ? ' is-invalid' : ''), 'placeholder'=>'Archive' ]) !!}
			</div>
			<div class="col-lg-2 col-md-4 form-group">
				<button class="btn btn-primary" type="submit">Search</button>
			</div>
			<div class="col-lg-2 col-md-4 form-group">
				<a href="{{ route('admin.composer.create') }}" title="Add" class="btn btn-success"><i class="fa fa-plus"></i> Add</a>
				</div>
		</div>
		{!! Form::close() !!}
		
		<div class="user-informtion-table tab-wrappers" id="table-show-sorting">	
			<table class="table" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Status</th>
                        <th>Action</th>
					</tr>
				</thead>
                <tbody>	
					@foreach($composers as $i => $composer)
					<tr>
						<td>{{ $i+1 }}</td>
						<td>{{ $composer->title }}</td>
						<td>
							@if(@$composer->status=='1')
							<span class="label label-success">Active</span>
							@else
							<span class="label label-danger">De-active</span>
							@endif
						</td>
						<td>
							<div class="btn-icon-list">
								<a href="{{ route('admin.composer.edit', $composer->id) }}" class="btn btn-warning btn-icon">
									<i class="fa fa-edit"></i>
								</a>
								
								@if($composer->is_delete=='1')
								<a data-method="Delete" data-confirm="Are you sure to retrieve?" href="{{ route('admin.composer.destroy', $composer->id) }}" class="btn btn-danger btn-icon">
									<i class="fa fa-reply"></i>
								</a>
								@else
								<a data-method="Delete" data-confirm="Are you sure to delete?" href="{{ route('admin.composer.destroy', $composer->id) }}" class="btn btn-danger btn-icon">
									<i class="fa fa-trash"></i>
								</a>
								@endif
							</div>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="pagination">
			@if(@request()->search || request()->status || request()->archive)
				{{ $composers->appends([
					'search' => request()->search,
					'status' => request()->status,
					'archive' => request()->archive,
				])->links() }}
			@else
				{{ $composers->links() }}
			@endif
		</div>
	</div>
</section>

@endsection			