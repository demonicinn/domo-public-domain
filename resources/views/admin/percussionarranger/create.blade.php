@extends('layouts.app')
@section('content')

<section class="form-section account-section">
	<div class="container-1315">
		<div class="tab-wrappers">
			<div class="tab-content">
				<div class="form-box-inner max-555">
					{!! Form::open(['route' => 'admin.percussion-arranger.store']) !!}
					
					@include('admin.percussionarranger.partials.form')
					
					<div class="form-grouph submit-design twobtns-flex">
						<button type="submit" class="submit-btn">Submit</button>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</section>
	
@endsection	