<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="site-url" content="{{ url('/') }}">
		<title>{{ $title['title'] ?? 'Dashboard' }} | Marching</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		
		<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
		<link rel="stylesheet" href="{{ asset('css/fontawesome.css') }}">
		<link rel="stylesheet" href="{{ asset('css/fontawesome.min.css') }}">
		<link rel="stylesheet" href="{{ asset('css/all.css') }}">
		<link rel="stylesheet" href="{{ asset('css/slick.css') }}">
		<link rel="stylesheet" href="{{ asset('css/slick-theme.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('css/animate.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('css/ui.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('css/datatable.css') }}">
		@livewireStyles  
		@yield('style')
	</head>
	
	<body>
		
		{{--@include('layouts.includes.header')--}}
		@livewire('header-cart')
		
		<main id="inner-body-content" class="{{ request()->segment('1')=='' ? '' : ' padd-t-142 padd-b-60' }}">
			<div class="my-account-wrapper admin-sec">
				@if(request()->segment('1')=='admin')
				<div class="container-1350">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-3">
								@include('layouts.includes.sidebar')
							</div>
							<div class="col-md-9">	
								@include('layouts.includes.alerts')
								<section class="page-title">
									<div class="container-1265">
										<div class="inner-page-title mrg-b-35">
											<h2>{{ @$title['title'] }}</h2>
										</div>
									</div>
								</section>	
								@yield('content')
							</div>
						</div>
					</div>
				</div>
				@elseif(request()->segment('1')=='ab')	
				<section class="page-title">
					<div class="container-1265">
						<div class="inner-page-title mrg-b-35">
							<h2>Account</h2>
						</div>
					</div>
				</section>
				<section class="form-section account-section">
					<div class="container-1315">
						@include('layouts.includes.alerts')
						<div class="tab-wrappers">								
							@include('layouts.includes.usermenu')		
							@yield('content')
						</div>
					</div>
				</section>
				@else
				@include('layouts.includes.alerts')
				@yield('content')
				@endif
				
			</div>
		</main>
		@include('layouts.includes.footer')
		
		<script src="{{ asset('js/jquery.min.js') }}"></script>
		<script src="{{ asset('js/popper.min.js') }}"></script>
		<script src="{{ asset('js/bootstrap.min.js') }}"></script>
		<script src="{{ asset('js/fontawsome.js') }}"></script>
		<script src="{{ asset('js/script.js') }}"></script>
		<script src="{{ asset('js/slick.js') }}"></script>
		<script src="{{ asset('js/delete.js') }}"></script>
		<script src="{{ asset('js/datatable-bootstrap.js') }}"></script>
		<script src="{{ asset('js/datatable-jquery.js') }}"></script>
		<script src="{{ asset('js/datatable-responsive-bootstrap.js') }}"></script>
		<script src="{{ asset('js/datatable-responsive.js') }}"></script>
		<script>
			$(document).ready( function () {
			$('#user-info-table').DataTable();
			$('#arrangment-table').DataTable();
			} );
		</script>
		<script>
			$('button.play').click(function(){
				$(this).parents('.product-description').toggleClass('description-active');
			});
		</script>
		@livewireScripts
		@yield('script')
		@stack('scripts')
	</body>
</html>