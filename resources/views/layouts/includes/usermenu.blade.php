<ul class="nav nav-tabs">
	<li class="nav-item">
		<a class="nav-link{{ @$title['active']=='profile' ? ' active' : '' }}" href="{{ route('ab.profile') }}">PROFILE</a>
	</li>
	<li class="nav-item">
		<a class="nav-link{{ @$title['active']=='arrangements' ? ' active' : '' }}" href="{{ route('ab.arrangements') }}">ARRANGEMENTS</a>
	</li>
	<li class="nav-item">
		<a class="nav-link{{ @$title['active']=='orders' ? ' active' : '' }}" href="{{ route('ab.orders') }}">PURCHASE ORDERS</a>
	</li>
	<li class="nav-item">
		<a class="nav-link{{ @$title['active']=='support' ? ' active' : '' }}" href="{{ route('ab.support') }}">SUPPORT</a>
	</li>
</ul>