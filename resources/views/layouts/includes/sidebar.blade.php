<div class="jumbotron admin-profile-sidebar">
	<ul>
		<li class="{{ @$title['active']=='profile' ? ' active' : '' }}">
			<a href="{{ route('admin.profile') }}">Profile</a>
		</li>
		<li class="{{ @$title['active']=='users' ? ' active' : '' }}">
			<a href="{{ route('admin.users') }}">Users</a>
		</li>
		<li class="{{ @$title['active']=='order' ? ' active' : '' }}">
			<a href="{{ route('admin.orders.index') }}">Orders</a>
		</li>
		<li class="{{ @$title['active']=='purchase_orders' ? ' active' : '' }}">
			<a href="{{ route('admin.purchase_orders.index') }}">Purchase Orders</a>
		</li>
		<li class="{{ @$title['active']=='mapOfPurchase' ? ' active' : '' }}">
			<a href="{{ route('admin.mapOfPurchase') }}">Map of Purchases</a>
		</li>
		
		<li class="{{ @$title['active']=='difficulty' ? ' active' : '' }}">
			<a href="{{ route('admin.difficulty.index') }}">Difficulty</a>
		</li>
		<li class="{{ @$title['active']=='instrumentation' ? ' active' : '' }}">
			<a href="{{ route('admin.instrumentation.index') }}">Instrumentation</a>
		</li>
		<li class="{{ @$title['active']=='composer' ? ' active' : '' }}">
			<a href="{{ route('admin.composer.index') }}">Composer</a>
		</li> 
		<li class="{{ @$title['active']=='windArranger' ? ' active' : '' }}">
			<a href="{{ route('admin.wind-arranger.index') }}">Wind Arranger</a>
		</li>
		<li class="{{ @$title['active']=='percussionArranger' ? ' active' : '' }}">
			<a href="{{ route('admin.percussion-arranger.index') }}">Percussion Arranger</a>
		</li>
		<li class="{{ @$title['active']=='arrangementType' ? ' active' : '' }}">
			<a href="{{ route('admin.arrangement-type.index') }}">ArrangemntsType</a>
		</li>
		<li class="{{ @$title['active']=='arrangements' ? ' active' : '' }}">
			<a href="{{ route('admin.arrangements.index') }}">Arrangements</a>
		</li>
		
		
		
		<li class="{{ @$title['active']=='states' ? ' active' : '' }}">
			<a href="{{ route('admin.states.index') }}">States</a>
		</li>
		<li class="{{ @$title['active']=='promocode' ? ' active' : '' }}">
			<a href="{{ route('admin.promocode.index') }}">Promocode</a>
		</li>
	</ul>
</div>