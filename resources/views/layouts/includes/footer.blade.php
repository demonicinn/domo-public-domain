<footer id="main-footer">
	<div class="container">
		<div class="footer-logo text-center">
			<a href="{{ route('home') }}">
			<img src="{{ asset('images/logo.png') }}">
			</a>
		</div>
		<div class="footer-navigation-wrapper">
			<ul class="list-unstyled footer-nav-flex">
				<li><a href="{{route('faqs') }}">FAQS</a></li>
				<li><a href="{{route('aboutus') }}">ABOUT US</a></li>
				<li><a href="{{route('product.index') }}">SHOP ARRANGEMENTS</a></li>
				<li><a href="{{route('support') }}">CONTACT US</a></li>
				@guest
				<li><a href="{{route('login') }}">LOGIN</a></li>
				@endguest
			</ul>
		</div>
		<div class="copy-right-text text-center">
			<p>PUBLIC DOMAIN MARCHING  © 2020</p>
		</div>
	</div>
</footer>