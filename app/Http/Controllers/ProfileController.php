<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    //...
	public function index(){
		$title = array(
			'title' => 'Profile',
			'active' => 'profile'
		);
		
		//admin
		if(auth()->user()->role=='admin'){
			if(request()->segment(1)!='admin'){
				return redirect()->route("admin.profile");
			}
			$user = User::find(auth()->user()->id);
			return view('admin.profile.index', compact('title', 'user'));
		}
		
		//user
		if(request()->segment(1)!='ab'){
			return redirect()->route("ab.profile");
		}
		$user = User::find(auth()->user()->id);
		return view('ab.profile.index', compact('title', 'user'));
	}
	
	//user profile
	public function update(Request $request)
	{
		$user = User::find(auth()->user()->id);
		$validated = $request->validate([
			'first_name' => 'required|string|max:100',
			'last_name' => 'required|string|max:100',
			'zipcode' => 'required|max:10',
			'school' => 'required|max:255',
			'email' => 'required|max:100|unique:users,email,'.$user->id,
			'phone_number' => 'required|numeric|digits_between:10,15',
        ]);
		
		$user->first_name = $request->first_name;
		$user->last_name = $request->last_name;
		$user->email = $request->email;
		$user->zipcode = $request->zipcode;
		$user->school = $request->school;
		$user->phone_number = $request->phone_number;
		$user->save();
		$request->session()->flash('success', "Profile updated successfully");
		return redirect()->route('ab.profile');
	
	}
	
	//admin profile
	public function adminProfileupdate(Request $request)
	{
		$user = User::find(auth()->user()->id);
		$validated = $request->validate([
			'first_name' => 'required|string|max:100',
			'last_name' => 'required|string|max:100',
			'email' => 'required|max:100|unique:users,email,'.$user->id
        ]);

		$user->first_name = $request->first_name;
		$user->last_name = $request->last_name;
		$user->email = $request->email;
		$user->save();
		
		$request->session()->flash('success', "Profile updated successfully");
		return redirect()->route('admin.profile');
	}
	
	//password update
	
	
	public function password()
    {
		$title = array(
			'title' => 'Change Password',
			'active' => 'profile'
		);
		if(auth()->user()->role=='admin'){
			if(request()->segment(1)!='admin'){
				return redirect()->route("admin.profile.password");
			}
        return view('admin.profile.password', compact('title'));
		}
		return view('ab.profile.password', compact('title'));
    }
	
	public function passwordUpdate(Request $request)
	{		
		$validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|same:new-password',
            'new-password_confirmed' => 'required|same:new-password',
        ]);

		if (!(Hash::check($request->get('current-password'), auth()->user()->password))) {
            // The passwords matches
			$request->session()->flash('danger', "Your current password does not matches with the password you provided. Please try again.");
			return redirect()->back();
        }
 
        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
			$request->session()->flash('warning', "New Password cannot be same as your current password. Please choose a different password.");
			return redirect()->back();
        }        
 
        //Change Password
        $user = auth()->user();
        $user->password = Hash::make($request->get('new-password'));
        $user->save();
 
		$request->session()->flash('success', "Password changed successfully");
		return redirect()->route('profile');
	}
	
	
	
	
	
	
}
