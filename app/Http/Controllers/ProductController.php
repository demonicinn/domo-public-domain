<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Difficulty;
use App\Models\Instrumentation;
use App\Models\Composer;
use App\Models\WindArranger;
use App\Models\PercussionArranger;
use App\Models\Arrangement;
use Cookie;

class ProductController extends Controller
{

	public function index(Request $request)
    {
		
		$title = array(
			'title' => 'Products',
			'active' => 'product'
		);
		
		//checkbox dropdown
		$difficulties = Difficulty::whereStatus('1')->whereIsDelete('0')->orderBy('id', 'desc')->get();
		
		$instrumentations = Instrumentation::whereStatus('1')->whereIsDelete('0')->orderBy('id', 'desc')->get();
		
		$composers = Composer::whereStatus('1')->whereIsDelete('0')->orderBy('id', 'desc')->get();
		
		$wind_arrangers = WindArranger::whereStatus('1')->whereIsDelete('0')->orderBy('id', 'desc')->get();
		
		$percussion_arrangers = PercussionArranger::whereStatus('1')->whereIsDelete('0')->orderBy('id', 'desc')->get();
		
		return view('pages.product.index', compact('title', 'difficulties', 'instrumentations', 'composers', 'wind_arrangers', 'percussion_arrangers'));
	}
	
	public function showProduct(Request $request)
    {
		$cookie_value = md5(uniqid() . time());
		$cookie_name = "guest_uid";	
		$guest_uid = Cookie::get($cookie_name);
		if(@$guest_uid){
			$cookie_value = $guest_uid;
		}			
		Cookie::queue($cookie_name, $cookie_value, time() + (86400 * 30 * 12));
	
		$title = array(
			'title' => 'Products',
			'active' => 'product'
		);

		return view('pages.product.arrangement', compact('title'));
	}
}
