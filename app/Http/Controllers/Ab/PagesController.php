<?php

namespace App\Http\Controllers\Ab;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Checkout;
use Cookie;

class PagesController extends Controller
{	
	//... 
	public function Arrangements(){
		$title = array(
			'title' => 'Arrangements',
			'active' => 'arrangements'
		);
		$uuid = Cookie::get('guest_uid');
		$orders = Checkout::where('user_id', auth()->user()->id)
			->where('status', 'paid')
			//->where('is_purchase_orders', '0')
			->orderBy('id', 'desc')->paginate(pagi());
			
		return view('ab.arrangements', compact('title', 'orders'));
	}
	public function arrangementShow(Checkout $order){
		$title = array(
			'title' => 'Arrangement Details',
			'active' => 'arrangements'
		);
		$checkout = $order;
		return view('ab.arrangements_show', compact('title', 'checkout'));
	}
	
	//...
	public function orders(){
		$title = array(
			'title' => 'Purchase Orders',
			'active' => 'orders'
		);
		$uuid = Cookie::get('guest_uid');
		$orders = Checkout::where('user_id', auth()->user()->id)
			->where(function ($query) {
				$query->where('status', 'purchase_orders');
				//$query->orWhere('status', 'paid');
			})
			//->where('is_purchase_orders', '1')
			->orderBy('id', 'desc')->paginate(pagi());
			
		return view('ab.orders', compact('title', 'orders'));
	}
	public function orderShow(Checkout $order){
		$title = array(
			'title' => 'Purchase Orders Details',
			'active' => 'orders'
		);
		$checkout = $order;
		return view('ab.orders_show', compact('title', 'checkout'));
	}
	
	//...
	public function support(){
		$title = array(
			'title' => 'Support',
			'active' => 'support'
		);
		$user = User::find(auth()->user()->id);
		return view('ab.support', compact('title', 'user'));
	}
	
		
	//...
	public function contactus(){
		$title = array(
			'title' => 'Contact Us',
			'active' => 'contact'
		);
		$user = User::find(auth()->user()->id);
		return view('ab.support', compact('title', 'user'));
	}


}
