<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Notification;
use App\Notifications\Contactus;
use App\Models\Instrumentation;
use App\Models\Arrangement;
use App\Models\ArrangementType;

class PagesController extends Controller
{
	public function home(Request $request)
    {
		$title = array(
		'title' => 'Home',
		'active' => 'home'
		);
	
		//instrumentations
		$instrumentations = Instrumentation::whereStatus('1')->whereIsDelete('0')->orderBy('id', 'desc')->pluck('title', 'id');	
		$arrangementtypes = ArrangementType::where('status', '1')->where('is_delete', '0')->orderBy('id', 'desc')->get();
		
		return view('pages.home', compact('title', 'instrumentations','arrangementtypes'));
    }
	
	public function aboutus()
    {
		$title = array(
		'title' => 'About us',
		'active' => 'about_us'
		);
		return view('pages.aboutus', compact('title'));
    }
	
	public function faqs()
    {
		$title = array(
		'title' => 'FAQS',
		'active' => 'faqs'
		);
		return view('pages.faqs', compact('title'));
    }
	
	
	
	//contact Us form submit
    public function contactUsForm(Request $request){
		
		
		$request->validate([
			'first_name' => 'required|min:3|max:100',
			'last_name' => 'required|min:3|max:100',
			'email' => 'required|string|email|min:3|max:100',
			'phone_number' => 'required|numeric|digits_between:10,12',
			'subject' => 'required|min:3|max:200',
		]);
		
		//send email
		Notification::route('mail', config('services.admin.email'))
		->notify(new Contactus($request->all()));
		
		$request->session()->flash('success', "We Got your Query, will contact you soon");
		//return redirect()->route('ab.support');
		return redirect()->back();

	}
	
}
