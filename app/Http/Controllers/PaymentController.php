<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Checkout;
use App\Models\Payment;
use App\Models\PromoCode;
use Stripe\Stripe;
use Illuminate\Support\Facades\Validator;

class PaymentController extends Controller
{

	public function paymentCancel(Request $request)
	{

		$payment = Payment::where('id', $request->id)->orderBy('id', 'desc')->first();

			try{
				$payment->is_cancel = '1';
				
				//get transaction history
				$strip = \Stripe\Charge::retrieve(
					$payment->transaction_id,
					[]
				);
				
				//refund order
				if(@$strip->amount_refunded=='0'){
					$refund = \Stripe\Refund::create(array(
						"amount" => $payment->total_price * 100,
						"charge" => $payment->transaction_id,
					));
					//...
					$checkRefund = \Stripe\Charge::retrieve(
						$payment->transaction_id,
						[]
					);
					if(@$checkRefund->amount_refunded!='0'){
						$payment->is_refund = '1';
					}
				}
				
				
				$payment->save();
				
				$request->session()->flash('success', "Order Cancelled successfully");
				return redirect()->back();
			
			}catch(Exception $e){
				$error = $e->getMessage();
			}
			
			$request->session()->flash('success', "Already Cancelled this order");
			return redirect()->back();

	}
}
