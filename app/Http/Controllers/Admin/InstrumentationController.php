<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Instrumentation;

class InstrumentationController extends Controller
{
	public function index(Request $request)
    {
		$title = array(
			'title' => 'Instrumentation',
			'active' => 'instrumentation'
		);

		$instrumentations = Instrumentation::query();
		if(@$request->search){
			$instrumentations = $instrumentations->where('title', 'like', '%'.$request->search.'%');
		}
		if(@$request->status){
			$instrumentations = $instrumentations->where('status', $request->status);
		}
		if(@$request->archive){
			$instrumentations = $instrumentations->where('is_delete', $request->archive);
		}
		else {
			$instrumentations = $instrumentations->where('is_delete', '0');
		}
		
		$instrumentations = $instrumentations->orderBy('id', 'desc')->paginate(pagi());
		
		return view('admin.instrumentation.index', compact('title', 'instrumentations'));
    }
	
	public function create()
    {
		$title = array(
			'title' => 'Instrumentation Create',
			'active' => 'instrumentation'
		);
		return view('admin.instrumentation.create', compact('title'));
    }
	
	public function store(Request $request)
    {
		$validated = $request->validate([
			'title' => 'required|string|unique:instrumentations,title|max:150',
			'status' => 'required',
		]);
		
		$store = new Instrumentation;
		$store->title = $request->title;
		$store->status = $request->status;
		$store->save();
		$request->session()->flash('success', "Instrumentation Created successfully");
        return redirect()->route('admin.instrumentation.index');
	
	
    }
	
	public function edit(Instrumentation $instrumentation)
    {
		$title = array(
			'title' => 'Instrumentation Edit',
			'active' => 'instrumentation'
		);
		return view('admin.instrumentation.edit', compact('title', 'instrumentation'));
    }
	
	
	public function update(Request $request, Instrumentation $instrumentation)
    {
		$validated = $request->validate([
			'title' => 'required|string|max:150|unique:instrumentations,title,'.$instrumentation->id,
			'status' => 'required',
		]);
		
		$instrumentation->title = $request->title;
		$instrumentation->status = $request->status;
		$instrumentation->save();
		
		$request->session()->flash('success', "Instrumentation Updated successfully");
		return redirect()->route('admin.instrumentation.index');
    }
	
	public function destroy(Request $request, Instrumentation $instrumentation)
    {
		if($instrumentation->is_delete=='1'){
			$instrumentation->is_delete = '0';
			$message = "Instrumentation Retrieve";
		}
		else {
			$instrumentation->is_delete = '1';
			$message = "Instrumentation Deleted";
		}
		$instrumentation->save();
		$request->session()->flash('success', $message);
		return redirect()->route("admin.instrumentation.index");
    }
}
