<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UsStates;

class StatesController extends Controller
{
    public function index(Request $request)
	{
		$title = array(
			'title' => 'States',
			'active' => 'states'
		);
		
		$states = UsStates::orderBy('id', 'desc')->paginate(pagi());
		
		return view('admin.states.index', compact('title', 'states'));
	}
	
	
	public function update(Request $request, UsStates $state)
	{
		$state->tax = $request->tax;
		$state->save();
		
		$request->session()->flash('success', 'Tax Updated Successfully');
		return redirect()->route("admin.states.index");
	}
}
