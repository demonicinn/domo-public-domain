<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Difficulty;

class DifficultyController extends Controller
{
   public function index(Request $request)
    {
		$title = array(
			'title' => 'Difficulty',
			'active' => 'difficulty'
		);
		
		$difficulties = Difficulty::query();
		if(@$request->search){
			$difficulties = $difficulties->where('title', 'like', '%'.$request->search.'%');
		}
		if(@$request->status){
			$difficulties = $difficulties->where('status', $request->status);
		}
		if(@$request->archive){
			$difficulties = $difficulties->where('is_delete', $request->archive);
		}
		else {
			$difficulties = $difficulties->where('is_delete', '0');
		}
		
		$difficulties = $difficulties->orderBy('id', 'desc')->paginate(pagi());
		
		return view('admin.difficulty.index', compact('title', 'difficulties'));
    }
	
	public function create()
    {
		$title = array(
			'title' => 'Difficulty Create',
			'active' => 'difficulty'
		);
		return view('admin.difficulty.create', compact('title'));
    }
	
	public function store(Request $request)
    {
		
		$validated = $request->validate([
			'title' => 'required|string|unique:difficulties,title|max:150',
			'status' => 'required',
		]);
		
		$store = new Difficulty;
		$store->title = $request->title;
		$store->status = $request->status;
		$store->save();
		$request->session()->flash('success', "Difficulty Created successfully");
        return redirect()->route('admin.difficulty.index');
	
	
    }
	
	public function edit(Difficulty $difficulty)
    {
		$title = array(
			'title' => 'Difficulty Edit',
			'active' => 'difficulty'
		);
		return view('admin.difficulty.edit', compact('title', 'difficulty'));
    }
	
	
	public function update(Request $request, Difficulty $difficulty)
    {
		$validated = $request->validate([
			'title' => 'required|string|max:150|unique:difficulties,title,'.$difficulty->id,
			'status' => 'required',
		]);
		
		$difficulty->title = $request->title;
		$difficulty->status = $request->status;
		$difficulty->save();
	  $request->session()->flash('success', "Difficulty Updated successfully");
	  return redirect()->route('admin.difficulty.index');
    }
	
	public function destroy(Request $request, Difficulty $difficulty)
    {
		if($difficulty->is_delete=='1'){
			$difficulty->is_delete = '0';
			$message = "Difficulty Retrieve";
		}
		else {
			$difficulty->is_delete = '1';
			$message = "Difficulty Deleted";
		}
		$difficulty->save();
		$request->session()->flash('success', $message);
		return redirect()->route("admin.difficulty.index");
		
    }
}
