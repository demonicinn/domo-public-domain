<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;

class UploadController extends Controller
{
   	
	public function upload(Request $request)
    {
		if ($request->file('file')) {			
			$file = $request->file('file');
			
			$path = 'storage/uploads/'.$request->path;
			if(!is_dir($path)) {
				mkdir($path, 0775, true);
				chown($path, exec('whoami'));
			}
	
			$new_file = $request->name .'-'. auth()->user()->id . uniqid(time()) . '.' . $file->getClientOriginalExtension();
			$file->move($path, $new_file);	
			
			return response()->json(['message'=>'File uploaded', 'filename'=>$new_file, 'response'=>true]);
		}
		return response()->json(['message'=>'File not selected', 'response'=>false]);
    }
}
