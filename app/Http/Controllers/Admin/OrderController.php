<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Checkout;
use App\Models\CheckoutProducts;
use PDF;
use ZipArchive;

use Illuminate\Support\Facades\Notification;
use App\Notifications\PurchaseOrderInvoiceEmail;
use App\Notifications\PurchaseOrderPaymentCompleted;

class OrderController extends Controller
{
	public function index(){
		$title = array(
			'title' => 'Orders Detail',
			'active' => 'order'
		);
		$orders = Checkout::where('status', 'paid')
			//->where('is_purchase_orders', '0')
			->orderBy('id', 'desc')->paginate(pagi());

		return view('admin.orders.index', compact('title', 'orders'));
	}
	public function show(Checkout $order){
		$title = array(
			'title' => 'Arrangement Details',
			'active' => 'arrangements'
		);
		$checkout = $order;
		return view('admin.orders.show', compact('title', 'checkout'));
	}
	
	
	public function mapOfPurchase(){
		$title = array(
			'title' => 'Map of Purchases',
			'active' => 'mapOfPurchase'
		);
		$orders = $orders = Checkout::where('status', 'paid')->get();

		return view('admin.orders.map_of_purchase', compact('title', 'orders'));
	}
	
	
	public function ordersPurchase(){
		$title = array(
			'title' => 'Purchase Orders',
			'active' => 'purchase_orders'
		);
		$orders = Checkout::where(function ($query) {
				$query->where('status', 'purchase_orders');
				//$query->orWhere('status', 'paid');
			})
			//->where('is_purchase_orders', '1')
			->orderBy('id', 'desc')->paginate(pagi());

		return view('admin.orders.purchase_orders', compact('title', 'orders'));
	}
	public function ordersPurchaseShow(Checkout $order){
		$title = array(
			'title' => 'Purchase Orders Details',
			'active' => 'orders'
		);
		$checkout = $order;
		return view('admin.orders.purchase_orders_show', compact('title', 'checkout'));
	}
	
	public function ordersPurchaseGenerate(Request $request){
		$orders = Checkout::find($request->order_id);
		if(@$orders){
			$orders->invoice_generated = '1';
			$orders->save();
			
			//send email
			Notification::route('mail', $orders->user->email)
				->notify(new PurchaseOrderInvoiceEmail($orders));
			
			$request->session()->flash('success', "Invoice Generated");
			return redirect()->route('admin.purchase_orders.index');
		}
		$request->session()->flash('error', "Invalid Request");
        return redirect()->route('admin.purchase_orders.index');
	}
	
	public function ordersPurchaseInvoice(Request $request, Checkout $order)
    {
		$filename = date('Y_m_d_h_i');
		
		
		//return view('admin.orders.invoice_purchase_orders', compact('order'));
		
		$pdf = PDF::loadView('admin.orders.invoice_purchase_orders', ['order' => $order, 'date'=>$request->date, 'po_number'=>$request->po_number]);
		
		return $pdf->download($filename.'.pdf');
	}
	

	public function ordersPurchasePaymentComplete(Request $request){		
		$orders = Checkout::find($request->order_id);
		if(@$orders){
			$orders->status = 'paid';
			$orders->save();
			
			//send email
			Notification::route('mail', $orders->user->email)
				->notify(new PurchaseOrderPaymentCompleted($orders));
			
			$request->session()->flash('success', "Payment Completed");
			return redirect()->route('admin.purchase_orders.index');
		}
		$request->session()->flash('error', "Invalid Request");
        return redirect()->route('admin.purchase_orders.index');
	}
	
	
	public function ordersZipFile(Request $request, CheckoutProducts $order)
    {
		$zip = new ZipArchive;
		$timeName = time();
		$path = 'storage/uploads/zip';
		
		if(!is_dir($path)) {
			mkdir($path, 0775, true);
			chown($path, exec('whoami'));
		}
			
		$zipFileName = $path . '/' . $timeName . '.zip';
		
		$zipPath = asset($zipFileName);
		if ($zip->open(($zipFileName), ZipArchive::CREATE) === true) {
		
			if($order->checkout->type=='wind'){
				$zip->addFile(public_path('storage/uploads/arrangements/'.$order->arrangementTo->wpdf));
				$zip->addFile(public_path('storage/uploads/arrangements/'.$order->arrangementTo->wmp3));
			}
			
			if($order->checkout->type=='percussion'){
				$zip->addFile(public_path('storage/uploads/arrangements/'.$order->arrangementTo->wppdf));
				$zip->addFile(public_path('storage/uploads/arrangements/'.$order->arrangementTo->wpmp3));
			}			
			
			$zip->close();
			
			if ($zip->open($zipFileName) === true) {
				return redirect($zipPath);
			} else {
				return false;
			}
		}
		return false;
	}
}
