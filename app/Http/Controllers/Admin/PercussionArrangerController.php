<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\PercussionArranger;

class PercussionArrangerController extends Controller
{
   public function index(Request $request)
    {
		$title = array(
			'title' => 'Percussion Arranger',
			'active' => 'percussionArranger'
		);

		$percussionarrangers = PercussionArranger::query();
		if(@$request->search){
			$percussionarrangers = $percussionarrangers->where('title', 'like', '%'.$request->search.'%');
		}
		if(@$request->status){
			$percussionarrangers = $percussionarrangers->where('status', $request->status);
		}
		if(@$request->archive){
			$percussionarrangers = $percussionarrangers->where('is_delete', $request->archive);
		}
		else {
			$percussionarrangers = $percussionarrangers->where('is_delete', '0');
		}
		
		$percussionarrangers = $percussionarrangers->orderBy('id', 'desc')->paginate(pagi());
		
		return view('admin.percussionarranger.index', compact('title', 'percussionarrangers'));
    }
	
	public function create()
    {
		$title = array(
			'title' => 'Percussion Arranger Create',
			'active' => 'percussionArranger'
		);
		return view('admin.percussionarranger.create', compact('title'));
    }
	
	public function store(Request $request)
    {
		$validated = $request->validate([
			'title' => 'required|string|unique:percussion_arrangers,title|max:150',
			'status' => 'required',
		]);
		
		$store = new PercussionArranger;
		$store->title = $request->title;
		$store->status = $request->status;
		$store->save();
		$request->session()->flash('success', "Percussion Arranger Created successfully");
        return redirect()->route('admin.percussion-arranger.index');
	
	
    }
	
	public function edit(PercussionArranger $percussion_arranger)
    {
		$title = array(
			'title' => 'Percussion Arranger Edit',
			'active' => 'percussionArranger'
		);
		
		$percussionarranger = $percussion_arranger;
		return view('admin.percussionarranger.edit', compact('title', 'percussionarranger'));
    }
	
	
	public function update(Request $request, PercussionArranger $percussion_arranger)
    {
		$validated = $request->validate([
			'title' => 'required|string|max:150|unique:percussion_arrangers,title,'.$percussion_arranger->id,
			'status' => 'required',
		]);
		$percussionarranger = $percussion_arranger;
		$percussionarranger->title = $request->title;
		$percussionarranger->status = $request->status;
		$percussionarranger->save();
		
		$request->session()->flash('success', "Percussion Arranger Updated successfully");
		return redirect()->route('admin.percussion-arranger.index');
    }
	
	public function destroy(Request $request, PercussionArranger $percussion_arranger)
    {
		$percussionarranger = $percussion_arranger;
		if($percussionarranger->is_delete=='1'){
			$percussionarranger->is_delete = '0';
			$message = "Percussion Arrangers Retrieve";
		}
		else {
			$percussionarranger->is_delete = '1';
			$message = "Percussion Arrangers Deleted";
		}
		$percussionarranger->save();
		$request->session()->flash('success', $message);
		return redirect()->route("admin.percussion-arranger.index");
    }
}
