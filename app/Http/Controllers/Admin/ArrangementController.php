<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Difficulty;
use App\Models\Instrumentation;
use App\Models\Composer;
use App\Models\WindArranger;
use App\Models\PercussionArranger;
use App\Models\Arrangement;
use App\Models\ArrangementType;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;

class ArrangementController extends Controller
{
    public function index(Request $request)
    {
		$title = array(
			'title' => 'Arrangements',
			'active' => 'arrangements'
		);
		
		$arrangements = Arrangement::query();
		
		if(@$request->search){
			$arrangements = $arrangements->where('title', 'like', '%'.$request->search.'%');
		}
		if(@$request->status){
			$arrangements = $arrangements->where('status', $request->status);
		}
		if(@$request->archive){
			$arrangements = $arrangements->where('is_delete', $request->archive);
		}
		else {
			$arrangements = $arrangements->where('is_delete', '0');
		}
		
		$arrangements = $arrangements->orderBy('id', 'desc')->paginate(pagi());
		
		return view('admin.arrangements.index', compact('title', 'arrangements'));
		
	}
	
	public function create()
    {
		$title = array(
			'title' => 'Arrangements Create',
			'active' => 'arrangements'
		);
		
		$difficulties = Difficulty::whereStatus('1')->whereIsDelete('0')->orderBy('id', 'desc')->pluck('title', 'id');
		
		$instrumentations = Instrumentation::whereStatus('1')->whereIsDelete('0')->orderBy('id', 'desc')->pluck('title', 'id');
		
		$composers = Composer::whereStatus('1')->whereIsDelete('0')->orderBy('id', 'desc')->pluck('title', 'id');
		
		$wind_arrangers = WindArranger::whereStatus('1')->whereIsDelete('0')->orderBy('id', 'desc')->pluck('title', 'id');
		
		$percussion_arrangers = PercussionArranger::whereStatus('1')->whereIsDelete('0')->orderBy('id', 'desc')->pluck('title', 'id');
		
		$arrangementtypes = ArrangementType::whereStatus('1')->whereIsDelete('0')->orderBy('id', 'desc')->pluck('title', 'id');
		
		return view('admin.arrangements.create', compact('title', 'difficulties', 'instrumentations', 'composers', 'wind_arrangers', 'percussion_arrangers', 'arrangementtypes'));
    }
	
	public function store(Request $request)
    {

		$validated = $request->validate([
			'title' => 'required|string|unique:arrangements,title|max:150',
			'description' => 'required',
			'difficulty' => 'required',
			'instrumentation' => 'required',
			'composer' => 'required',
			'windarranger' => 'required',
			'percussionarranger' => 'required',
			'arrangementtype' => 'required',
			'date' => 'required',
			'image' => 'required',
			'wind_arrangment' => 'required',
			'percussion_arrangment' => 'required',
			'wpdf' => 'required',
			'wmp3' => 'required',
			'wppdf' => 'required',
			'wpmp3' => 'required',
			'status' => 'required',
		]);
		
		
		
		$store = new Arrangement;		
		
		$store->title = $request->title;
		$store->slug = str_slug($request->title, '-');
		$store->description = $request->description;
		$store->difficulty = $request->difficulty;
		$store->instrumentation = $request->instrumentation;
		$store->composer = $request->composer;
		$store->windarranger = $request->windarranger;
		$store->percussionarranger = $request->percussionarranger;
		$store->arrangementtype = $request->arrangementtype;
		$store->date = $request->date;
		$store->image = $request->image;
		$store->wind_arrangment = $request->wind_arrangment;
		$store->wpdf = $request->wpdf;
		$store->wmp3 = $request->wmp3;
		$store->percussion_arrangment = $request->percussion_arrangment;
		$store->wppdf = $request->wppdf;
		$store->wpmp3 = $request->wpmp3;
		$store->is_featured = $request->is_featured;
		$store->status = $request->status;
		$store->save();
		
		$request->session()->flash('success', "Arrangement Created successfully");
		return redirect()->route('admin.arrangements.index');
	}
	
	public function edit(Arrangement $arrangement)
    {
		$title = array(
			'title' => 'Arrangements Edit',
			'active' => 'arrangements'
		);
		
		$difficulties = Difficulty::whereStatus('1')->whereIsDelete('0')->orderBy('id', 'desc')->pluck('title', 'id');
		
		$instrumentations = Instrumentation::whereStatus('1')->whereIsDelete('0')->orderBy('id', 'desc')->pluck('title', 'id');
		
		$composers = Composer::whereStatus('1')->whereIsDelete('0')->orderBy('id', 'desc')->pluck('title', 'id');
		
		$wind_arrangers = WindArranger::whereStatus('1')->whereIsDelete('0')->orderBy('id', 'desc')->pluck('title', 'id');
		
		$percussion_arrangers = PercussionArranger::whereStatus('1')->whereIsDelete('0')->orderBy('id', 'desc')->pluck('title', 'id');
		
		
		$arrangementtypes = ArrangementType::whereStatus('1')->whereIsDelete('0')->orderBy('id', 'desc')->pluck('title', 'id');
		
		return view('admin.arrangements.edit', compact('title', 'arrangement', 'difficulties', 'instrumentations', 'composers', 'wind_arrangers', 'percussion_arrangers', 'arrangementtypes'));
	
	}
	
	
	public function update(Request $request, Arrangement $arrangement)
    {
		
		$validated = $request->validate([
			'title' => 'required|string|max:150|unique:composers,title,'.$arrangement->id,
			'description' => 'required',
			'difficulty' => 'required',
			'instrumentation' => 'required',
			'composer' => 'required',
			'windarranger' => 'required',
			'percussionarranger' => 'required',
			'arrangementtype' => 'required',
			'date' => 'required',
			'image' => 'required',
			'wind_arrangment' => 'required',
			'percussion_arrangment' => 'required',
			'wpdf' => 'required',
			'wmp3' => 'required',
			'wppdf' => 'required',
			'wpmp3' => 'required',
			'status' => 'required',
		]);
		
		$arrangement->title = $request->title;
		$arrangement->description = $request->description;
		$arrangement->difficulty = $request->difficulty;
		$arrangement->instrumentation = $request->instrumentation;
		$arrangement->composer = $request->composer;
		$arrangement->windarranger = $request->windarranger;
		$arrangement->percussionarranger = $request->percussionarranger;
		$arrangement->arrangementtype = $request->arrangementtype;
		$arrangement->date = $request->date;
		$arrangement->image = $request->image;
		$arrangement->wind_arrangment = $request->wind_arrangment;
		$arrangement->wpdf = $request->wpdf;
		$arrangement->wmp3 = $request->wmp3;
		$arrangement->percussion_arrangment = $request->percussion_arrangment;
		$arrangement->wppdf = $request->wppdf;
		$arrangement->wpmp3 = $request->wpmp3;
		$arrangement->is_featured = $request->is_featured;
		$arrangement->status = $request->status;
		$arrangement->save();
		
		$request->session()->flash('success', "Arrangement Updated successfully");
		return redirect()->route('admin.arrangements.index');
	
	}
	
	public function destroy(Request $request, Arrangement $arrangement)
    {
		if($arrangement->is_delete=='1'){
			$arrangement->is_delete = '0';
			$message = "Arrangement Retrieve";
		}
		else {
			$arrangement->is_delete = '1';
			$message = "Arrangement Deleted";
		}
		$arrangement->save();
		$request->session()->flash('success', $message);
		return redirect()->route("admin.arrangements.index");
    }
	
}
