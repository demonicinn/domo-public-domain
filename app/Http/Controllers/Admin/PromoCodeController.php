<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PromoCode;
use App\Models\User;

class PromoCodeController extends Controller
{
	public function index(Request $request){
		$title = array(
			'title' => 'Promocodes',
			'active' => 'promocode'
		);
	
		$promocode = PromoCode::query();
		if(@$request->promocode){
			$promocode = $promocode->where('promocode', $request->promocode);
		}
		if(@$request->status){
			$promocode = $promocode->where('status', $request->status);
		}
		if(@$request->archive){
			$promocode = $promocode->where('is_delete', $request->archive);
		}
		else {
			$promocode = $promocode->where('is_delete', '0');
		}
		
		$promocode = $promocode->orderBy('id', 'desc')->paginate(pagi());
		
		return view('admin.promocode.index', compact('title', 'promocode'));
	}
	
	public function create(){
		$title = array(
			'title' => 'Promocodes',
			'active' => 'promocode'
		);
		
		$users = User::where('role', 'user')->pluck('phone_number', 'id');
		return view('admin.promocode.create',  compact('title', 'users'));
	}
	
	//store
	public function store(Request $request)
    {
		$request->validate([
			'promocode' => 'required|unique:promo_codes,promocode',
			'start_date' => 'required',
			'expire_date' => 'required',
			'coupon_type' => 'required',
			'discount' => 'required',
			'uses_limit' => 'required',
			'status' => 'required',
		]);
		
		$store = new PromoCode;
		$store->promocode = $request->promocode;
		$store->users = @$request->users ? json_encode($request->users) : '';
		$store->start_date = $request->start_date;
		$store->expire_date = $request->expire_date;
		$store->coupon_type = $request->coupon_type;
		$store->discount = $request->discount;
		$store->uses_limit = $request->uses_limit;
		$store->status = $request->status;
		if(@$request->new_users=='yes'){
			$store->new_users = 'yes';
		}
		else {
			$store->new_users = 'no';
		}
		$store->save();
		
		$request->session()->flash('success', "Promocode Created Successfully");
		return redirect()->route("admin.promocode.index");		
	}
	
	//edit
	public function edit(Request $request, PromoCode $promocode)
    {
	// echo '<pre>';
	// print_r($code);
	// die;
		$code = $promocode;
		$title = array(
			'title' => 'Promocodes',
			'active' => 'promocode'
		);
		
		$users = User::where('role', 'user')->pluck('phone_number', 'id');
		return view('admin.promocode.edit', compact('title', 'code', 'users'));
    }
	
	//update
	public function update(Request $request, PromoCode $promocode)
    {
		$code = $promocode;
		$request->validate([
			'promocode' => 'required|unique:promo_codes,promocode,'.$code->id,
			'start_date' => 'required',
			'expire_date' => 'required',
			'coupon_type' => 'required',
			'discount' => 'required',
			'uses_limit' => 'required',
			'status' => 'required',
		]);
		
		$code->promocode = $request->promocode;
		$code->users = @$request->users ? json_encode($request->users) : '';
		$code->start_date = $request->start_date;
		$code->expire_date = $request->expire_date;
		$code->coupon_type = $request->coupon_type;
		$code->discount = $request->discount;
		$code->uses_limit = $request->uses_limit;
		$code->status = $request->status;
		if(@$request->new_users=='yes'){
			$code->new_users = 'yes';
		}
		else {
			$code->new_users = 'no';
		}
		$code->save();
		
		$request->session()->flash('success', "Promocode Updated Successfully");
		return redirect()->route("admin.promocode.index");
	}
	
	//delete
	public function destroy(Request $request, PromoCode $promocode)
    {
		$code = $promocode;
		if($code->is_delete=='1'){
			$code->is_delete = '0';
			$message = "Promocode Retrieve";
		}
		else {
			$code->is_delete = '1';
			$message = "Promocode Deleted";
		}
		$code->save();
		$request->session()->flash('success', $message);
		return redirect()->route("admin.promocode.index");
    }
}
