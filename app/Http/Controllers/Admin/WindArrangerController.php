<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\WindArranger;

class WindArrangerController extends Controller
{
    public function index(Request $request)
    {
		$title = array(
			'title' => 'WindArranger',
			'active' => 'windArranger'
		);

		$windarrangers = WindArranger::query();
		if(@$request->search){
			$windarrangers = $windarrangers->where('title', 'like', '%'.$request->search.'%');
		}
		if(@$request->status){
			$windarrangers = $windarrangers->where('status', $request->status);
		}
		if(@$request->archive){
			$windarrangers = $windarrangers->where('is_delete', $request->archive);
		}
		else {
			$windarrangers = $windarrangers->where('is_delete', '0');
		}
		
		$windarrangers = $windarrangers->orderBy('id', 'desc')->paginate(pagi());
		
		return view('admin.windarranger.index', compact('title', 'windarrangers'));
    }
	
	public function create()
    {
		$title = array(
			'title' => 'WindaArranger Create',
			'active' => 'windaArranger'
		);
		return view('admin.windarranger.create', compact('title'));
    }
	
	public function store(Request $request)
    {
		$validated = $request->validate([
			'title' => 'required|string|unique:wind_arrangers,title|max:150',
			'status' => 'required',
		]);
		
		$store = new WindArranger;
		$store->title = $request->title;
		$store->status = $request->status;
		$store->save();
		$request->session()->flash('success', "Wind Arranger Created successfully");
        return redirect()->route('admin.wind-arranger.index');
	
	
    }
	
	public function edit(WindArranger $wind_arranger)
    {
		$title = array(
			'title' => 'WindaArranger Edit',
			'active' => 'windaArranger'
		);
		$windarranger = $wind_arranger;
		return view('admin.windarranger.edit', compact('title', 'windarranger'));
    }
	
	
	public function update(Request $request, WindArranger $wind_arranger)
    {
		$validated = $request->validate([
			'title' => 'required|string|max:150|unique:wind_arrangers,title,'.$wind_arranger->id,
			'status' => 'required',
		]);
		$windarranger = $wind_arranger;
		$windarranger->title = $request->title;
		$windarranger->status = $request->status;
		$windarranger->save();
		
		$request->session()->flash('success', "Wind Arranger Updated successfully");
		return redirect()->route('admin.wind-arranger.index');
    }
	
	public function destroy(Request $request, WindArranger $wind_arranger)
    {
		$windarranger = $wind_arranger;
		if($windarranger->is_delete=='1'){
			$windarranger->is_delete = '0';
			$message = "WindaArranger Retrieve";
		}
		else {
			$windarranger->is_delete = '1';
			$message = "WindaArranger Deleted";
		}
		$windarranger->save();
		$request->session()->flash('success', $message);
		return redirect()->route("admin.wind-arranger.index");
    }
}
