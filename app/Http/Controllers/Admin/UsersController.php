<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class UsersController extends Controller
{
    //
	public function index(Request $request)
    {
		$title = array(
			'title' => 'Users',
			'active' => 'users'
		);
		
		$users = User::query();
		if($request->search){
			$search = $request->search;
			$users = $users->where(function ($query) use ($search) {
				$query->where('first_name', 'like', '%' .$search. '%');
				$query->orWhere('last_name', 'like', '%' .$search. '%');
				$query->orWhere('zipcode', 'like', '%' .$search. '%');
				$query->orWhere('school', 'like', '%' .$search. '%');
			});
		}
		if(@$request->status){
			$users = $users->where('status', $request->status);
		}
		if(@$request->archive){
			$users = $users->where('is_delete', $request->archive);
		}
		else {
			$users = $users->where('is_delete', '0');
		}
		
		$users = $users->whereRole('user')->orderBy('id', 'desc')->paginate(pagi());
		return view('admin.users.index', compact('title', 'users'));
    }
	
	public function edit(Request $request, User $user)
    {
		$title = array(
			'title' => 'Users Edit',
			'active' => 'users'
		);
		
		return view('admin.users.edit', compact('title', 'user'));
	
	}
	
	public function update(Request $request, User $user)
    {
		$title = array(
			'title' => 'Users Edit',
			'active' => 'users'
		);
		
		$user->first_name = $request->first_name;
		$user->last_name = $request->last_name;
		$user->zipcode = $request->zipcode;
		$user->school = $request->school;
		$user->status = $request->status;
		$user->save();
		$request->session()->flash('success', "Updated successfully");
		return redirect()->route('admin.users');
	
	}
	
	public function destroy(Request $request, User $user)
    {
		if($user->is_delete=='1'){
			$user->is_delete = '0';
			$message = "User Retrieve";
		}
		else {
			$user->is_delete = '1';
			$message = "User Deleted";
		}
		$user->save();
		$request->session()->flash('success', $message);
		return redirect()->route("admin.users");
		
    }
}
