<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Composer;

class ComposerController extends Controller
{
    public function index(Request $request)
    {
		$title = array(
			'title' => 'Composer',
			'active' => 'composer'
		);

		$composers = Composer::query();
		if(@$request->search){
			$composers = $composers->where('title', 'like', '%'.$request->search.'%');
		}
		if(@$request->status){
			$composers = $composers->where('status', $request->status);
		}
		if(@$request->archive){
			$composers = $composers->where('is_delete', $request->archive);
		}
		else {
			$composers = $composers->where('is_delete', '0');
		}
		
		$composers = $composers->orderBy('id', 'desc')->paginate(pagi());
		
		return view('admin.composer.index', compact('title', 'composers'));
    }
	
	public function create()
    {
		$title = array(
			'title' => 'Composer Create',
			'active' => 'composer'
		);
		return view('admin.composer.create', compact('title'));
    }
	
	public function store(Request $request)
    {
		$validated = $request->validate([
			'title' => 'required|string|unique:composers,title|max:150',
			'status' => 'required',
		]);
		
		$store = new Composer;
		$store->title = $request->title;
		$store->status = $request->status;
		$store->save();
		$request->session()->flash('success', "Composer Created successfully");
        return redirect()->route('admin.composer.index');
    }
	
	public function edit(Composer $composer)
    {
		$title = array(
			'title' => 'Composer Edit',
			'active' => 'composer'
		);
		return view('admin.composer.edit', compact('title', 'composer'));
    }
	
	
	public function update(Request $request, Composer $composer)
    {
		$validated = $request->validate([
			'title' => 'required|string|max:150|unique:composers,title,'.$composer->id,
			'status' => 'required',
		]);
		
		$composer->title = $request->title;
		$composer->status = $request->status;
		$composer->save();
		
		$request->session()->flash('success', "Composer Updated successfully");
		return redirect()->route('admin.composer.index');
    }
	
	public function destroy(Request $request, Composer $composer)
    {
		if($composer->is_delete=='1'){
			$composer->is_delete = '0';
			$message = "Composer Retrieve";
		}
		else {
			$composer->is_delete = '1';
			$message = "Composer Deleted";
		}
		$composer->save();
		$request->session()->flash('success', $message);
		return redirect()->route("admin.composer.index");
    }
}
