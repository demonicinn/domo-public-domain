<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ArrangementType;
use Intervention\Image\ImageManagerStatic as Image;

class ArrangementTypeController extends Controller
{
   public function index(Request $request)
    {
		$title = array(
			'title' => 'Arrangement Type',
			'active' => 'arrangementType'
		);
		
		$arrangementtypes = ArrangementType::query();
		if(@$request->search){
			$arrangementtypes = $arrangementtypes->where('title', 'like', '%'.$request->search.'%');
		}
		if(@$request->status){
			$arrangementtypes = $arrangementtypes->where('status', $request->status);
		}
		if(@$request->archive){
			$arrangementtypes = $arrangementtypes->where('is_delete', $request->archive);
		}
		else {
			$arrangementtypes = $arrangementtypes->where('is_delete', '0');
		}
		
		$arrangementtypes = $arrangementtypes->orderBy('id', 'desc')->paginate(pagi());
		
		return view('admin.arrangementtype.index', compact('title', 'arrangementtypes'));
		
	}
	
	public function create()
    {
		$title = array(
			'title' => 'Arrangement Type Create',
			'active' => 'arrangementType'
		);
		return view('admin.arrangementtype.create', compact('title'));
    }
	
	public function store(Request $request)
    {
		$validated = $request->validate([
			'title' => 'required|string|unique:arrangement_types,title|max:150',
			'image' => 'required',
			'status' => 'required',
		]);
		
		$store = new ArrangementType;
		$store->title = $request->title;
		$store->status = $request->status;
		$store->image = $request->image;
		$store->save();
		$request->session()->flash('success', "Arrangement Type Created successfully");
        return redirect()->route('admin.arrangement-type.index');
    }
	public function edit(ArrangementType $arrangement_type)
    {
		$title = array(
			'title' => 'Arrangement Type Edit',
			'active' => 'arrangementType'
		);
		$arrangementtype = $arrangement_type;
		return view('admin.arrangementtype.edit', compact('title', 'arrangementtype'));
    }
	
	public function update(Request $request, ArrangementType $arrangement_type)
    {
		$validated = $request->validate([
			'title' => 'required|string|max:150|unique:arrangement_types,title,'.$arrangement_type->id,
			'image' => 'required',
			'status' => 'required',
		]);
		$arrangementtype = $arrangement_type;
		$arrangementtype->title = $request->title;
		$arrangementtype->status = $request->status;
		$arrangementtype->image = $request->image;
		$arrangementtype->save();
		
		$request->session()->flash('success', "Arrangement Type Updated successfully");
		return redirect()->route('admin.arrangement-type.index');
    }
	
	public function destroy(Request $request, ArrangementType $arrangement_type)
    {
		$arrangementtype = $arrangement_type;
		if($arrangementtype->is_delete=='1'){
			$arrangementtype->is_delete = '0';
			$message = "Arrangement Type Retrieve";
		}
		else {
			$arrangementtype->is_delete = '1';
			$message = "Arrangement Type Deleted";
		}
		$arrangementtype->save();
		$request->session()->flash('success', $message);
		return redirect()->route("admin.arrangement-type.index");
    }
}
