<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Checkout;
use App\Models\Payment;
use App\Models\PromoCode;
use Cookie;

class CheckoutController extends Controller
{
   
	public function index(Request $request)
    {
		$title = array(
			'title' => 'Checkout',
			'active' => 'checkout'
		);
		$uuid = Cookie::get('guest_uid');
		
		if(cart() > 0){
			return view('ab.checkout.index', compact('title'));
		}
		return redirect()->route('product.index');
	}
	
	
	public function cart(Request $request)
    {
		$title = array(
			'title' => 'Cart',
			'active' => 'cart'
		);
		$uuid = Cookie::get('guest_uid');
		
		if(cart() > 0){
			return view('ab.checkout.cart', compact('title'));
		}
		return redirect()->route('product.index');
	}
	
}
