<?php

	//check session route permissions accordingly user role
	function checkPermission($permissions){
		foreach ($permissions as $key => $value) {
			if($value == auth()->user()->role){				
				return true;
			}
		}
		return false;
	}

	//pagination
	function pagi(){
		return 10;
	}
	
	//cart
	function cart(){
	
		$uuid = Cookie::get('guest_uid');
		
		$checkout = \App\Models\Checkout::where(function ($query) use ($uuid) {
				$query->where('guest_uid', $uuid);
				if(auth()->check()==true){
					$query->orWhere('user_id', auth()->user()->id);
				}
			})
			->where('status', 'cart')
			->first();
			
			if($checkout){
				return $checkout->products()->count();
			}
		return $checkout;
	}
	
	//cartProducts
	function cartProducts(){
	
		$uuid = Cookie::get('guest_uid');
		
		$checkout = \App\Models\Checkout::where(function ($query) use ($uuid) {
				$query->where('guest_uid', $uuid);
				if(auth()->check()==true){
					$query->orWhere('user_id', auth()->user()->id);
				}
			})
			->where('status', 'cart')
			->orderBy('id', 'desc')
			->first();
		return	$checkout;
	}
	
	//get lat-long from zipcode
	function getLatLong($code){
		$mapsApiKey = config('services.google.api');
		$query = "https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($code)."&sensor=false&key=".$mapsApiKey;
		
		$result_string = file_get_contents($query);
		$result = json_decode($result_string, true);
		//dd($result);
		if(!empty($result['results'])){
			$lat = $result['results'][0]['geometry']['location']['lat'];
			$lng = $result['results'][0]['geometry']['location']['lng'];
			$address = $result['results'][0]['formatted_address'];
			return array('latitude'=>$lat,'longitude'=>$lng, 'address'=>$address);
		}
		 else {
			return false;
		}
	}
	
	
	
	function sumCheckout(){
		//...
		$uuid = Cookie::get('guest_uid');
		
		$checkUser = \App\Models\Checkout::where(function ($query) use ($uuid) {
				$query->where('guest_uid', $uuid);
				if(auth()->check()==true){
					$query->orWhere('user_id', auth()->user()->id);
				}
			})
			->where('status', 'cart')
			->first();

		$productsSum = \App\Models\CheckoutProducts::where('checkouts_id', $checkUser->id)->sum('price');
		//...
		$checkUser->price = $productsSum;
		$checkUser->total_price = $productsSum;
		$checkUser->save();
	}
	
	