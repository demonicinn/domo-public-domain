<?php

namespace App\Http\Responses;

use Laravel\Fortify\Contracts\LoginResponse as LoginResponseContract;

class LoginResponse implements LoginResponseContract {

    public function toResponse($request) {		
	
		
		if (auth()->check() && auth()->user()->status!='1') {
			if (auth()->user()->status == '2') {
				$message = 'Your account has been Blocked. Please contact administrator.';
				$request->session()->flash('error', $message);
            } else {
				$message = 'Your account has been suspended. Please contact administrator.';
				$request->session()->flash('error', $message);
            }
			auth()->logout();
			return redirect()->route('login');
		}
		
		//update user id to cart
		$uuid = \Cookie::get('guest_uid');
		
		//...
		$oldcart = \App\Models\Checkout::where('user_id', auth()->user()->id)
			->where('status', 'cart')
			->orderBy('id', 'desc')
			->first();
		
		
		//...
		$checkout = \App\Models\Checkout::where(function ($query) use ($uuid) {
				$query->where('guest_uid', $uuid);
			})
			->where('status', 'cart')
			->orderBy('id', 'desc')
			->first();
		
		if(@$oldcart && $checkout){
			$oldcart->delete();
		}
		
		if(@$checkout){
			$checkout->user_id = auth()->user()->id;
			$checkout->save();
		}
		
		$redirectUrl = session('link');
		session(['link' => '']);
		return redirect($redirectUrl);
    }
}