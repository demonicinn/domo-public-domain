<?php

namespace App\Http\Responses;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Laravel\Fortify\Contracts\LogoutResponse as LogoutResponseContract;
use Symfony\Component\HttpFoundation\Response;

class LogoutResponse implements LogoutResponseContract {

    public function toResponse($request) {		
		\Cookie::queue('guest_uid', 1, 1);
		
		return $request->wantsJson()
                    ? new JsonResponse('', 204)
                    : redirect('/');
    }
}