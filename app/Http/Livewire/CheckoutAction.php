<?php

namespace App\Http\Livewire;

use Livewire\Component;

use App\Models\User;
use App\Models\Checkout;
use App\Models\CheckoutProducts;
use App\Models\Payment;
use App\Models\PromoCode;
use App\Models\UsStates;
use Cookie;
use Stripe\Stripe;
use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Notification;
use App\Notifications\ReceiptsEmail;
use App\Notifications\PurchaseOrderEmail;


class CheckoutAction extends Component
{
	public $first_name, $last_name, $zipcode, $school, $email, $phone_number, $card_name, $card_number, $exp_month, $exp_year, $cvv, $password, $promocode, $address, $checkoutData, $checkoutTotalPrice, $city, $state;
	
	public $tax = 0;
	
	public $order='payment';
	
	public function __construct()
    {
        Stripe::setApiKey(config('services.stripe.secret')); 
    }
	
	public function mount(){
		if(auth()->check()){
			$user = auth()->user();
			$this->first_name = $user->first_name;
			$this->last_name = $user->last_name;
			//$this->zipcode = $user->zipcode;
			$this->school = $user->school;
			$this->phone_number = $user->phone_number;
		}
		
		//...
		$uuid = Cookie::get('guest_uid');
	
		$checkout = Checkout::where(function ($query) use ($uuid) {
			$query->where('guest_uid', $uuid);
			if(auth()->check()==true){
				$query->orWhere('user_id', auth()->user()->id);
			}
		})
		->where('status', 'cart')
		->first();
		
		$date = date('Y-m-d');
		foreach($checkout->products as $product){
			if($product->arrangementTo < $date){
				$product->delete();
			}
		}
		
		sumCheckout();
		
		//...................
		$to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $checkout->updated_at);
		
		$from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', date('Y-m-d H:s:i'));

		$diff_in_minutes = $to->diffInMinutes($from);
		if($diff_in_minutes > 5){
			$checkout->promocode = '';
			$checkout->promocode_type = '';
			$checkout->discount = '';
			$checkout->save();
		}
		
		
		//$checkout = Checkout::find($checkout->id);
		//...
		// $price = $checkout->total_price;
		// $tax = $price * $this->tax / 100;
		
		// $newprice = $tax + $checkout->price;
		// if(@$checkout->discount){
			// $newprice = $newprice - $checkout->discount;
		// }
		
		// if($checkout->coupon_type=='percentage'){
			// $newprice = ($checkout->price - ($checkout->price * $checkout->discount / 100)) + $tax;
		// }
		
		// Checkout::find($checkout->id)->update([ 'tax' => $tax, 'total_price' => $newprice ]);
		
		$checkout = Checkout::find($checkout->id);
		
		$this->checkoutData = $checkout;
		$this->zipcode = $checkout->zipcode;
		$this->address = $checkout->address;
		$this->state = $checkout->state;
		
		if($checkout->state){
			$state = UsStates::where('state_name', $checkout->state)->first();
			if($state)
				$this->tax = $state->tax;
		}
	}
	
	
	public function stateChange($state){
		$this->state = $state;
		$state = UsStates::where('state_name', $state)->first();
		
		$this->tax = $state->tax;
		$checkout = Checkout::find($this->checkoutData->id);
		
		//.....
		$price = $checkout->price;
		$tax = $price * $this->tax / 100;
		
		$newprice = $tax + $checkout->price;
		if(@$checkout->discount){
			$newprice = $newprice - $checkout->discount;
		}
		
		if($checkout->coupon_type=='percentage'){
			$newprice = ($checkout->price - ($checkout->price * $checkout->discount / 100)) + $tax;
		}
		
		Checkout::find($checkout->id)->update([ 'tax' => $tax, 'total_price' => $newprice, 'state'=> $state->state_name]);
		
		$checkout = Checkout::find($checkout->id);
		
		$this->checkoutData = $checkout;
		$this->zipcode = $checkout->zipcode;
		$this->address = $checkout->address;
	}
	
	public function rules() {
		if($this->order=='payment'){
			if(auth()->check()){
				return [
					'first_name' => 'required|min:1|max:100|string',
					'last_name' => 'required|min:1|max:100|string',
					'zipcode' => 'required|min:1|max:10',
					'school' => 'required|min:1|max:225|string',
					'phone_number' => 'required|numeric|digits_between:10,12',
					'card_name' => 'required',
					'card_number' => 'numeric|digits:16',
					'exp_month' => 'required',
					'exp_year' => 'required',
					'address' => 'required',
					'city' => 'required',
					'state' => 'required',
					'cvv' => 'numeric|digits_between:3,4',
				];
			}
		
			return [
				'first_name' => 'required|min:1|max:100|string',
				'last_name' => 'required|min:1|max:100|string',
				'email' => 'required|min:1|max:100|unique:users',
				'password' => 'required|min:8',
				'zipcode' => 'required|min:1|max:10',
				'school' => 'required|min:1|max:225|string',
				'phone_number' => 'required|numeric|digits_between:10,12',
				'card_name' => 'required',
				'card_number' => 'numeric|digits:16',
				'exp_month' => 'required',
				'exp_year' => 'required',
				'address' => 'required',
				'city' => 'required',
				'state' => 'required',
				'cvv' => 'numeric|digits_between:3,4',
			];
		}
		
		if(auth()->check()){
			return [
				'first_name' => 'required|min:1|max:100|string',
				'last_name' => 'required|min:1|max:100|string',
				'zipcode' => 'required|min:1|max:10',
				'school' => 'required|min:1|max:225|string',
				'phone_number' => 'required|numeric|digits_between:10,12',
				'address' => 'required',
				'city' => 'required',
				'state' => 'required',
			];
		}
		
		return [
			'first_name' => 'required|min:1|max:100|string',
			'last_name' => 'required|min:1|max:100|string',
			'email' => 'required|min:1|max:100|unique:users',
			'password' => 'required|min:8',
			'zipcode' => 'required|min:1|max:10',
			'school' => 'required|min:1|max:225|string',
			'phone_number' => 'required|numeric|digits_between:10,12',
			'address' => 'required',
			'city' => 'required',
			'state' => 'required',
		];
    }
	
	
	//apply promocode
	public function applyCode()
    {
		$this->validate([
            'promocode' => 'required',
        ]);
		
		$date = date('Y-m-d');
		
		$code = PromoCode::where('promocode', $this->promocode)
		->where('start_date', '<=', $date)
		->where('expire_date', '>=', $date)
		->where('status', '1')
		->where('is_delete', '0')
		->first();	
		
		$uuid = Cookie::get('guest_uid');
		$this->promocode = '';
		if(@$code){
			$checkPromo = Checkout::where('promocode', $code->promocode)
				->where(function ($query) use ($uuid) {
					$query->where('guest_uid', $uuid);
					if(auth()->check()==true){
						$query->orWhere('user_id', auth()->user()->id);
					}
				})
				->where('status', 'paid')
				->count();			
			
			
			if($code->new_users=='yes' && $checkPromo > 0){
				request()->session()->flash('promocodeError', 'Invalid Promo Code');
				return redirect()->back();
			}
			
			if(@$checkPromo >= $code->uses_limit){
				request()->session()->flash('promocodeError', 'Invalid Promo Code');
				return redirect()->back();
			}
			
			if(auth()->check()){
				if(@$code->users && !in_array(auth()->user()->id, json_decode($code->users))){
					request()->session()->flash('promocodeError', 'Invalid Promo Code');
					return redirect()->back();
				}
			}
			
			
			//---apply code
			$check = Checkout::where(function ($query) use ($uuid) {
				$query->where('guest_uid', $uuid);
				if(auth()->check()==true){
					$query->orWhere('user_id', auth()->user()->id);
				}
			})
			->orderBy('id', 'desc')->first();
			
			//apply discount
			if(auth()->check()==true){
				$check->user_id = auth()->user()->id;
			}
			
			$check->promocode = $code->promocode;
			$check->promocode_type = $code->coupon_type;
			$check->discount = $code->discount;			
			
			if($code->coupon_type=='percentage'){
				$check->total_price =  ($check->price - ($check->price * $code->discount / 100)) + $check->tax;
			}
			else{
				$check->total_price = ($check->price + $check->tax) - $code->discount;
			}
			$check->save();
			
			
			$this->checkoutData = $check;
			
			request()->session()->flash('promocodeSucces', 'Promo Code Applied');
			return response(['code' => $code], 200);
		}
		request()->session()->flash('promocodeError', 'Invalid Promo Code');		
		return redirect()->back();
	}
	
	//remove promocode
	public function removeCode()
    {
		$uuid = Cookie::get('guest_uid');
		$check = Checkout::where(function ($query) use ($uuid) {
			$query->where('guest_uid', $uuid);
			if(auth()->check()==true){
				$query->orWhere('user_id', auth()->user()->id);
			}
		})
		->where('status', 'cart')
		->orderBy('id', 'desc')->first();
		
		$check->promocode = null;
		$check->promocode_type = '';
		$check->discount = '';
		$check->total_price = $check->price + $check->tax;
		$check->save();
		
		$this->checkoutData = $check;
		
		request()->session()->flash('promocodeSucces', 'Promo Code Removed');
	}	
	
	//pay with card
	public function paymentAction()
	{
		$this->validate();
		
		$user = new User;
		if(auth()->check()){
			$user->id = auth()->user()->id;
			$user->exists = true;
		}
		else {
			$user->email_verified_at = date('Y-m-d');
			$user->email = $this->email;
			$user->password = Hash::make($this->password);
			$user->role = 'user';
		}
		$user->first_name = $this->first_name;
		$user->last_name = $this->last_name;
		$user->zipcode = $this->zipcode;
		$user->school = $this->school;
		$user->phone_number = $this->phone_number;
		$user->save();
		
		if(!auth()->check()){
			auth()->login($user);
		}
		
		$uuid = Cookie::get('guest_uid');
		
		$user = auth()->user();
	
		$checkout = Checkout::where(function ($query) use ($uuid) {
				$query->where('guest_uid', $uuid);
				$query->orWhere('user_id', auth()->user()->id);
			})
			->where('status', 'cart')
			->orderBy('id', 'desc')->first();
			
		try {
			//create token
			$token = \Stripe\Token::create([
				"card" => array(
					"name" => $this->card_name,
					"number" => $this->card_number,
					"exp_month" => $this->exp_month,
					"exp_year" => $this->exp_year,
					"cvc" => $this->cvv
				),
			]);
			
			//get customer id
			if(!$user->customer_id) {
				$customer = \Stripe\Customer::create([
					'source' => $token['id'],
					'email' =>  $user->email,
					'description' => 'My name is '. $user->first_name.' '.$user->last_name,
				]);
				
				$customer_id = $customer['id'];
				//update customer id
				$user->customer_id = $customer_id;
				$user->save();
			}
		
		
			//make payment
			$fee = $checkout->total_price;
			
			$charge = \Stripe\Charge::create([
				'currency' => 'USD',
				'customer' => $user->customer_id,
				'amount' =>  $fee * 100,
			]);
			
			//save payment transaction details
			$payment = new Payment;
			$payment->user_id = $user->id;
			$payment->checkout_id = $checkout->id;
			$payment->transaction_id = $charge->id;
			$payment->balance_transaction = $charge->balance_transaction;
			$payment->customer = $charge->customer;
			$payment->currency = $charge->currency;
			$payment->amount = $fee;
			$payment->payment_status = $charge->status;
			$payment->save();
			
			//delete checkout entry
			$checkout->user_id = $user->id;
			$checkout->address = $this->address;
			$checkout->city = $this->city;
			$checkout->state = $this->state;
			$checkout->status = 'paid';
			$checkout->save();
			
			//send email
			Notification::route('mail', $user->email)
				->notify(new ReceiptsEmail($payment));
				
			
			request()->session()->flash('success', "Payment Success");
			return redirect()->route('ab.arrangements');
		
		} catch (\Stripe\Error\RateLimit $e) {
			$error = $e->getMessage();
		} catch (\Stripe\Error\InvalidRequest $e) {
			$error = $e->getMessage();
		} catch (\Stripe\Error\Authentication $e) {
			$error = $e->getMessage();
		} catch (\Stripe\Error\ApiConnection $e) {
			$error = $e->getMessage();
		} catch (\Stripe\Error\Base $e) {
			$error = $e->getMessage();
		} catch (Exception $e) {
			$error = $e->getMessage();
		}
		request()->session()->flash('error', $error);
		return redirect()->back();
	}
	
	
	
	
	
	//Purchased Order
	public function purchaseAction(){
		$this->validate();
		
		$user = new User;
		if(auth()->check()){
			$user->id = auth()->user()->id;
			$user->exists = true;
		}
		else {
			$user->email_verified_at = date('Y-m-d');
			$user->email = $this->email;
			$user->password = Hash::make($this->password);
			$user->role = 'user';
		}
		$user->first_name = $this->first_name;
		$user->last_name = $this->last_name;
		$user->zipcode = $this->zipcode;
		$user->school = $this->school;
		$user->phone_number = $this->phone_number;
		$user->save();
		
		if(!auth()->check()){
			auth()->login($user);
		}
		
		$uuid = Cookie::get('guest_uid');
		
		$checkout = Checkout::where(function ($query) use ($uuid) {
				$query->where('guest_uid', $uuid);
				$query->orWhere('user_id', auth()->user()->id);
			})
			->where('status', 'cart')
			->orderBy('id', 'desc')->first();
		
		
		$checkout->user_id = auth()->user()->id;
		$checkout->address = $this->address;
		$checkout->city = $this->city;
		$checkout->state = $this->state;
		$checkout->status = 'purchase_orders';
		$checkout->is_purchase_orders = '1';
		$checkout->save();
		
		//send email
		Notification::route('mail', $user->email)
			->notify(new PurchaseOrderEmail($checkout));
				
		//...
		request()->session()->flash('success', 'Purchased Order Request Send');
		return redirect()->route('ab.orders');	
	}
	
	public function deleteProduct($product){
		$checkout = CheckoutProducts::find($product);
		if($checkout){
			$checkout->delete();
		}
		sumCheckout();
		
	}
	
	
	public function render()
    {
        return view('livewire.checkout-action', ['checkoutProducts'=>$this->checkoutData]);
    }


}
