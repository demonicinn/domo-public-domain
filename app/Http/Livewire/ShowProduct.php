<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Arrangement;
use App\Models\Checkout;
use App\Models\CheckoutProducts;
use App\Models\Payment;
use App\Models\PromoCode;
use Cookie;

class ShowProduct extends Component
{ 
	public $zip_code, $type='wind_arrangment', $allArrangements;
	
	public function mount()
	{
		if(request()->segment(1)=='featured-products'){
			$allArrangements = Arrangement::where('is_featured', '1')->where('arrangements.date', '>=', date('Y-m-d'))->orderBy('updated_at', 'desc')->first();
		}
		else {
			$allArrangements = Arrangement::where('id', request()->id)->where('arrangements.date', '>=', date('Y-m-d'))->where('slug', request()->slug)->first();
		}
		
		if(!$allArrangements){
			return redirect()->route('product.index');
		}
		
		$this->allArrangements = $allArrangements;
	}
	
	public function rules() {
        return [
            'zip_code' => 'required|min:5',
        ];
    }
	public function setType($type){
		$this->type = $type;
	}
	
	public function checkout()
	{
		$this->validate();
		
		if(auth()->check()==true && !auth()->user()->email_verified_at){
		    return redirect()->route('profile');
		}
		
		if($this->zip_code){
			$res = getLatLong($this->zip_code);
			if(@$res && ($res['latitude'] || $res['longitude'] || $res['address'])){
				$lat = @$res['latitude'];
				$lng = @$res['longitude'];
				$address = @$res['address'];
				
				$arrangementLocationCheck = Checkout::selectRaw('checkouts.*, (((acos(sin(('.$lat.'*pi()/180)) * sin((`latitude`*pi()/180))+cos(('.$lat.'*pi()/180)) * cos((`latitude`*pi()/180)) * cos((('.$lng.'- `longitude`)*pi()/180))))*180/pi())*60*1.1515*1.609344) AS distance')
					->join('checkout_products', 'checkouts.id', '=', 'checkout_products.checkouts_id')
					->orderBy('checkout_products.id', 'desc')
					->having('distance', '<=', '150')
					//->where('checkouts.status', 'paid')
					->where('checkout_products.arrangements_id', $this->allArrangements->id)
					->get()
					->unique('checkout_products.arrangements_id');
				//dd($arrangementLocationCheck);
				//...
				if(count($arrangementLocationCheck) > 0){
					request()->session()->flash('error', 'Not available to your area.');
				}
				else{
					$uuid = Cookie::get('guest_uid');
					
					$checkUser = Checkout::where(function ($query) use ($uuid) {
						$query->where('guest_uid', $uuid);
						if(auth()->check()==true){
							$query->orWhere('user_id', auth()->user()->id);
						}
					})
					->where('status', 'cart')
					->first();
					
					//...
					$fee = ($this->type=='wind_arrangment') ? $this->allArrangements->wind_arrangment : $this->allArrangements->percussion_arrangment;
					$store = new Checkout;				
					if(@$checkUser){
						$store->exists = true;
						$store->id = $checkUser->id;
					}
					else {
						$store->guest_uid = $uuid;
						if(auth()->check()==true){
							$store->user_id = auth()->user()->id;
						}
					}
					$store->type = $this->type=='wind_arrangment' ? 'wind' : 'percussion';
					$store->zipcode = $this->zip_code;
					$store->latitude = $lat;
					$store->longitude = $lng;
					$store->address = $address;
					$store->save();
					
					//...
					$checkoutproducts = CheckoutProducts::where('checkouts_id', $store->id)->where('arrangements_id', $this->allArrangements->id)->first();
					$product = new CheckoutProducts;
					if(@$checkoutproducts){
						$product->exists = true;
						$product->id = $checkoutproducts->id;
					}
					$product->checkouts_id  = $store->id;
					$product->arrangements_id  = $this->allArrangements->id;
					$product->arrangementtype = $this->type;
					$product->price = $fee;
					$product->save();
					
					sumCheckout();
					
					//...
					$this->zip_code = '';
					$totalCart = count(cartProducts()->products);
					
					$this->dispatchBrowserEvent('checkCartCount', ['count' => $totalCart]);
					
					request()->session()->flash('success', "Arrangement added to your card");
					//return redirect()->route('checkout.index');
				}
			}
			else {
				request()->session()->flash('error', 'Invalid Zip Code.');
			}
		}
	
	}

    public function render()
    {
		return view('livewire.show-product');
    }
}
