<?php

namespace App\Http\Livewire\Auth;

use Livewire\Component;
use Auth;

class Login extends Component
{
	public $email, $password, $redirect;
	
	public function rules() {
        return [
            'email' => 'required|string|max:100|email',
            'password' => 'required',        
        ];
    }
	
	public function login(){
		$this->validate();
		$email = $this->email;
		$password = $this->password;
		$redirect = $this->redirect;
		$credentials = ['email'=>$email, 'password'=>$password];
		
		if(!Auth::attempt($credentials)){
			request()->session()->flash('error', 'Invalid credentials');
        }
		else{
			return redirect()->route("checkout.index");
		}
	}
	
    public function render()
    {
        return view('livewire.auth.login');
    }
}
