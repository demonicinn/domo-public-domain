<?php

namespace App\Http\Livewire;

use Livewire\Component;

class HeaderCart extends Component
{

	public $cart = 0;
	public $cartProductsCount = 0;
	public $cartProducts = [];
	public $is_cartList = false;

    public function mount()
	{
		$this->cart = cart();
		$cartProducts = cartProducts();
		
		if(@$cartProducts){
			$this->cartProductsCount = count($cartProducts->products);		
		}
		//dd($cartProducts);
		$this->cartProducts = cartProducts();
	}
	
	public function cartList(){
		if($this->is_cartList){
			$this->is_cartList = false;
		}
		else {
			$this->is_cartList = true;
		}
	}
	
	
    public function render()
    {
        return view('livewire.header-cart');
    }
}
