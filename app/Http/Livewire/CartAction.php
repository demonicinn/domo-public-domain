<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Cookie;
use App\Models\Checkout;
use App\Models\CheckoutProducts;


class CartAction extends Component
{
	
	public function deleteProduct($product){
		$checkout = CheckoutProducts::find($product);
		if($checkout){
			$checkout->delete();
		}
		sumCheckout();
		
		$uuid = Cookie::get('guest_uid');
	
		$checkout = Checkout::where(function ($query) use ($uuid) {
			$query->where('guest_uid', $uuid);
			if(auth()->check()==true){
				$query->orWhere('user_id', auth()->user()->id);
			}
		})
		->where('status', 'cart')
		->first();
		
		if(count($checkout->products)<=0){
		    $checkout->delete();
			return redirect()->route('product.index');
		}
	}
	
	
    public function render()
    {
		$uuid = Cookie::get('guest_uid');
	
		$checkout = Checkout::where(function ($query) use ($uuid) {
			$query->where('guest_uid', $uuid);
			if(auth()->check()==true){
				$query->orWhere('user_id', auth()->user()->id);
			}
		})
		->where('status', 'cart')
		->first();
		
        return view('livewire.cart-action', ['checkoutProducts'=>$checkout]);
    }
}
