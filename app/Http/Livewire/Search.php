<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;

use App\Models\Difficulty;
use App\Models\Instrumentation;
use App\Models\Composer;
use App\Models\WindArranger;
use App\Models\PercussionArranger;
use App\Models\Arrangement;
use App\Models\Payment;
use App\Models\ArrangementType;


class Search extends Component
{
	use WithPagination;
	public $search;
	public $zipcode;
	public $latitude;
	public $longitude;
	public $difficulty = [];
	public $instrumentation = [];
	public $composer = [];
	public $wind_arranger = [];
	public $percussion_arranger = [];
	public $arrangement_type = [];
	
	public $is_difficulty, $is_instrumentation, $is_composer, $is_wind_arranger, $is_percussion_arranger, $is_arrangementtype = false;
	

	public function mount(){
		$this->search = request()->search;
		$this->zipcode = request()->zipcode;
		// if(request()->instrumentation){
			// $this->instrumentation[] = request()->instrumentation;
		// }
		if(request()->arrangement){
			$types = ArrangementType::where('title', request()->arrangement)->first();
			if($types){
				$this->arrangement_type[] = $types->id;
				$this->is_arrangementtype = true;
			}
		}
	}
	
	public function sortBy($type){
		if($this->$type){
			$this->$type = false;
		}
		else {
			$this->$type = true;
		}
	}
	
	public function resetFilter(){
		$this->search = '';
	    $this->zipcode = '';
	    $this->difficulty = [];
	    $this->instrumentation = [];
	    $this->composer = [];
	    $this->wind_arranger = [];
	    $this->percussion_arranger = [];
	    $this->arrangement_type = [];
	}
	

	
    public function render()
    {
	
	// $arrangements = Arrangement::leftjoin('payments', 'arrangements.id', '=', 'payments.arrangement_id');
		$arrangements = Arrangement::select('arrangements.*')
			->leftJoin('checkout_products', 'arrangements.id', '=', 'checkout_products.arrangements_id')
			->leftJoin('checkouts', 'checkouts.id', '=', 'checkout_products.checkouts_id')
			
			->leftJoin('difficulties', 'difficulties.id', '=', 'arrangements.difficulty')
			->leftJoin('instrumentations', 'instrumentations.id', '=', 'arrangements.instrumentation')
			->leftJoin('composers', 'composers.id', '=', 'arrangements.composer')
			->leftJoin('wind_arrangers', 'wind_arrangers.id', '=', 'arrangements.windarranger')
			->leftJoin('percussion_arrangers', 'percussion_arrangers.id', '=', 'arrangements.percussionarranger')
			->leftJoin('arrangement_types', 'arrangement_types.id', '=', 'arrangements.arrangementtype')
			
			->distinct('checkout_products.arrangements_id');
			
			if($this->search){
				$search = $this->search;
				$arrangements = $arrangements->where(function ($query) use ($search) {
					$query->where('arrangements.title', 'like', '%' .$search. '%');
					$query->orWhere('difficulties.title', 'like', '%' .$search. '%');
					$query->orWhere('instrumentations.title', 'like', '%' .$search. '%');
					$query->orWhere('composers.title', 'like', '%' .$search. '%');
					$query->orWhere('wind_arrangers.title', 'like', '%' .$search. '%');
					$query->orWhere('percussion_arrangers.title', 'like', '%' .$search. '%');
					$query->orWhere('arrangement_types.title', 'like', '%' .$search. '%');
				});
			}
			if($this->zipcode){
			
				$res = getLatLong($this->zipcode);
				// dd($res);
				// dd($res['longitude'], $res['latitude']);
				$lat = $res['latitude'];
				$lng = $res['longitude'];
				$arrangements = $arrangements->where('checkouts.zipcode', $this->zipcode);
				// dd($arrangements);
				
				$arrangements = $arrangements->selectRaw('(((acos(sin(('.$lat.'*pi()/180)) * sin((`latitude`*pi()/180))+cos(('.$lat.'*pi()/180)) * cos((`latitude`*pi()/180)) * cos((('.$lng.'- `longitude`)*pi()/180))))*180/pi())*60*1.1515) AS distance');
				
				
				
				//dd($arrangements);
				//$arrangements = $arrangements->having('distance', '>=', 10);
				
				
			}
			
			if($this->difficulty){
				$arrangements = $arrangements->whereIn('arrangements.difficulty', $this->difficulty);
			}
			if($this->instrumentation){
				$arrangements = $arrangements->whereIn('arrangements.instrumentation', $this->instrumentation);
			}
			if($this->composer){
				$arrangements = $arrangements->whereIn('arrangements.composer', $this->composer);
			}
			if($this->wind_arranger){
				$arrangements = $arrangements->whereIn('arrangements.windarranger', $this->wind_arranger);
			}
			if($this->percussion_arranger){
				$arrangements = $arrangements->whereIn('arrangements.percussionarranger', $this->percussion_arranger);
			}
			if($this->arrangement_type){
				$arrangements = $arrangements->whereIn('arrangements.arrangementtype', $this->arrangement_type);
			}
			
		$arrangements = $arrangements->where('arrangements.status', '1')
			->where('arrangements.is_delete', '0')
			->where('arrangements.date', '>=', date('Y-m-d'))
			->orderBy('arrangements.id', 'desc')->paginate(10);
		 //dd($arrangements);
        return view('livewire.search', ['arrangements'=>$arrangements]);
    }
}
