<?php

namespace App\Http\Middleware;

use Closure;

class CheckBanned
{
	public function handle($request, Closure $next)
    {
        if (auth()->check() && auth()->user()->status!='1') {

            if (auth()->user()->status == '2') {
                $message = 'Your account has been Blocked. Please contact administrator.';
				$request->session()->flash('error', $message);
            } else {
                $message = 'Your account has been suspended. Please contact administrator.';
				$request->session()->flash('error', $message);
            }
			auth()->logout();

            return redirect()->route('login')->withMessage($message);
        }

        return $next($request);
    }
}
