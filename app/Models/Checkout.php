<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Checkout extends Model
{
    use HasFactory;
	
	protected $fillable = [
		'user_id',
		'guest_uid',
        'arrangement_id',
        'type',
        'zipcode',
        'latitude',
        'longitude',
        'address',
        'city',
        'state',
		'tax',
		'total_price',
		'promocode_id',
		'coupon',
		'discount',
		'pay',
    ];

	
	public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
	
	public function products()
    {
        return $this->hasMany('App\Models\CheckoutProducts', 'checkouts_id');
    }
	
	public function promoCode()
    {
        return $this->belongsTo('App\Models\PromoCode', 'promocode_id');
    }

}
