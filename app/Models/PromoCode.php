<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PromoCode extends Model
{
    use HasFactory;
	
	 protected $fillable = [
		'promocode',
        'start_date',
        'expire_date',
        'coupon_type',
        'discount',
        'uses_limit',
        'users',
        'new_users',
        'status',
        'is_delete',
    ];
}
