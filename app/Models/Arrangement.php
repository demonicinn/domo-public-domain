<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Arrangement extends Model
{
    use HasFactory;
	
	protected $fillable = [
		'title',
		'slug',
        'description',
        'difficulty',
        'instrumentation',
        'composer',
        'windarranger',
        'percussionarranger',
        'arrangementtype',
        'date',
        'image',
        'wind_arrangment',
        'wpdf',
        'wmp3',
        'percussion_arrangment',
        'wppdf',
        'wpmp3',
        'is_featured',
        'status',
        'is_delete',
    ];
	
	public function composerTO()
    {
        return $this->belongsTo('App\Models\Composer', 'composer');
    }
	
	public function windarrangerTo()
    {
        return $this->belongsTo('App\Models\WindArranger', 'windarranger');
    }
	
	public function difficultyTo()
    {
        return $this->belongsTo('App\Models\Difficulty', 'difficulty');
    }
	
	public function percussionarrangerTO()
    {
        return $this->belongsTo('App\Models\PercussionArranger', 'percussionarranger');
    }
	
	public function instrumentationTo()
    {
        return $this->belongsTo('App\Models\Instrumentation', 'instrumentation');
    }
	public function arrangementTypeTo()
    {
        return $this->belongsTo('App\Models\ArrangementType', 'arrangementtype');
    }
	
	
	//...
	
	public static function percussion_arrangers()
    {
        return PercussionArranger::all()->where('status', '1')->where('is_delete', '0');
	}
	
	public static function wind_arrangers()
    {
        return WindArranger::all()->where('status', '1')->where('is_delete', '0');
	}
	
	public static function composers()
    {
        return Composer::all()->where('status', '1')->where('is_delete', '0');
	}
	
	public static function instrumentations()
    {
        return Instrumentation::all()->where('status', '1')->where('is_delete', '0');
	}
	
	public static function difficulties()
    {
        return Difficulty::all()->where('status', '1')->where('is_delete', '0');
	}
	
	public static function arrangementtypes()
    {
        return ArrangementType::all()->where('status', '1')->where('is_delete', '0');
	}
	
	
	
}
