<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CheckoutProducts extends Model
{
    use HasFactory;
	
	protected $fillable = [
		'checkouts_id',
		'arrangements_id',
        'arrangementtype',
        'price',
    ];
	
	
	public function arrangementTo()
    {
        return $this->belongsTo('App\Models\Arrangement', 'arrangements_id');
    }
	
	public function checkout()
    {
        return $this->belongsTo('App\Models\Checkout', 'checkouts_id');
    }
}
