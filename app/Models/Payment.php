<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;
	
	protected $fillable = [
		'user_id',
        'arrangement_id',
        'type',
		'price',
		'latitude',
		'longitude',
		'address',
		'instructions',
		'transaction_id',
		'balance_transaction',
		'customer',
		'currency',
		'zipcode',
		'status',
		'total_price',
		'promocode_id',
		'coupon',
		'discount',
		'is_cancel',
		'is_refund',
    ];
	
	public function arrangementTo()
    {
        return $this->belongsTo('App\Models\Arrangement', 'arrangement_id');
    }
	
	public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }
	
}
