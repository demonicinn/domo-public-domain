<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UsStates extends Model
{
    use HasFactory;
	
	protected $fillable = [
		'state_code',
        'state_name',
        'tax',
    ];
}
