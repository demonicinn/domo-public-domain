<?php

namespace App\Actions\Fortify;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Laravel\Fortify\Contracts\CreatesNewUsers;

use Illuminate\Support\Facades\Notification;
use App\Notifications\WelcomeEmail;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {

        Validator::make($input, [
            'first_name' => ['required', 'string', 'max:100'],
            'last_name' => ['required', 'string', 'max:100'],
            'zipcode' => ['required', 'string', 'max:10'],
            'school' => ['required', 'string', 'max:255'],
            'email' => [
                'required',
                'string',
                'email',
                'max:100',
                Rule::unique(User::class),
            ],
			'phone_number' => ['required', 'numeric', 'digits_between:10,12'],
            'password' => $this->passwordRules(),
        ])->validate();

		//send email
		Notification::route('mail', $input['email'])
		->notify(new WelcomeEmail($input));
		
        return User::create([
			'role' => $input['role'] ?? 'user',
            'first_name' => $input['first_name'],
            'last_name' => $input['last_name'],
            'email' => $input['email'],
            'zipcode' => $input['zipcode'],
            'school' => $input['school'],
            'phone_number' => $input['phone_number'],
            'password' => Hash::make($input['password']),
        ]);		
		
    }
}
