<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class Contactus extends Notification
{
    use Queueable;
	public $request;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
		$res = $this->request;
        return (new MailMessage)
					->greeting('Hello, Admin!')
                    ->line('Support Form Query.')
                    ->line('Name: '. $res['first_name'].' '.$res['last_name'])
                    ->line('Email: '. $res['email'])
                    ->line('Phone Number: '. $res['phone_number'])
                    ->line('Zip Code: '. $res['zipcode'])
                    ->line('School: '. $res['school'])
                    ->line('Subject: '. $res['subject']);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
