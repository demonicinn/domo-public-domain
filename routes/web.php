<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\PagesController;
use App\Http\Controllers\ProfileController;

//ab
use App\Http\Controllers\Ab\PagesController as ABPagesController;

//admin
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Admin\UploadController;

//products
use App\Http\Controllers\ProductController;
//checkout
use App\Http\Controllers\CheckoutController;

//payment
use App\Http\Controllers\PaymentController;

//orders
use App\Http\Controllers\Admin\OrderController;
//states
use App\Http\Controllers\Admin\StatesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//home
Route::get('/', [PagesController::class, 'home'])->name('home');

//aboutus
Route::get('about-us', [PagesController::class, 'aboutus'])->name('aboutus');

//aboutus
Route::get('faqs', [PagesController::class, 'faqs'])->name('faqs');

//contactus
Route::get('contact-us', [PagesController::class, 'contactus'])->name('contactus');

//products
Route::get('/products', [ProductController::class, 'index'])->name('product.index');
Route::get('/featured-products', [ProductController::class, 'showProduct'])->name('product.feature');

Route::get('/product/{id}/{slug}', [ProductController::class, 'showProduct'])->name('product.arrangement');

//checkout 
	Route::get('/cart', [CheckoutController::class, 'cart'])->name('cart.index');
	Route::get('/checkout', [CheckoutController::class, 'index'])->name('checkout.index');
	Route::post('/checkout/store', [CheckoutController::class, 'store'])->name('checkout.store');
		
//contactUsForm mail admin
Route::post('mail', [PagesController::class, 'contactUsForm'])->name('admin.contactUsForm');


//contact us
Route::get('/contact-us', [ABPagesController::class, 'contactus'])->name('support');

//flush cache
Route::get('/cache-clear', function() {
	Artisan::call('config:cache');
	Artisan::call('cache:clear');
	return "Cache is cleared";
});

Route::get('/migrate', function() {
	Artisan::call('migrate');
	return "Migrate";
});

Route::get('/storage-link', function() {
	Artisan::call('storage:link');
	return "storage:link";
});



//------------------------------------------------------
//------------------------After Login-------------------
//------------------------------------------------------
Route::middleware(['auth', 'verified'])->group(function () {

	//profile page
	Route::get('/profile', [ProfileController::class, 'index'])->name('profile');	
	
	
	Route::get('/zipfile/{order}', [OrderController::class, 'ordersZipFile'])->name('ab.zipfile');
	
	Route::group(['middleware' => ['role:user']], function(){		
		
		// checkout 
		// Route::get('/checkout', [CheckoutController::class, 'index'])->name('checkout.index');
		// Route::post('/checkout/store', [CheckoutController::class, 'store'])->name('checkout.store');
		
		//payment 
		Route::post('/payment/store', [PaymentController::class, 'store'])->name('payment.store');

		//added url prefix
		Route::prefix('ab')->group(function () {
			//profile page
			Route::get('/', [ProfileController::class, 'index'])->name('ab');
			Route::get('/profile', [ProfileController::class, 'index'])->name('ab.profile');
			Route::post('/profile', [ProfileController::class, 'update'])->name('ab.profile.update');
			
			//password
			Route::get('/profile/password', [ProfileController::class, 'password'])->name('ab.profile.password');
			Route::post('/profile/password', [ProfileController::class, 'passwordUpdate'])->name('ab.profile.passwordupdate');
			
			
			//Arrangements
			Route::get('/arrangements', [ABPagesController::class, 'arrangements'])->name('ab.arrangements');
			Route::get('/arrangements/{order}', [ABPagesController::class, 'arrangementShow'])->name('ab.arrangements.show');
			
			//orders
			Route::get('/purchase-orders', [ABPagesController::class, 'orders'])->name('ab.orders');
			Route::get('/purchase-orders/{order}/invoice', [OrderController::class, 'ordersPurchaseInvoice'])->name('ab.purchase_orders.invoice');
			Route::get('/purchase-orders/{order}', [ABPagesController::class, 'orderShow'])->name('ab.orders.show');
			
			//support
			Route::get('/support', [ABPagesController::class, 'support'])->name('ab.support');

		});
	});
	
	

	//added url prefix
	Route::prefix('admin')->group(function () {
		Route::group(['middleware' => ['role:admin']], function(){
			
			//profile page
			Route::get('/', [ProfileController::class, 'index'])->name('admin');
			Route::get('/profile', [ProfileController::class, 'index'])->name('admin.profile');
			Route::post('/profile', [ProfileController::class, 'adminProfileupdate'])->name('admin.profile.update');
			
			//password
			Route::get('/profile/password', [ProfileController::class, 'password'])->name('admin.profile.password');
			Route::post('/profile/password', [ProfileController::class, 'passwordUpdate'])->name('admin.profile.passwordUpdate');
			
			//admin
			//users
			Route::get('/users', [UsersController::class, 'index'])->name('admin.users');
			//edit
			Route::get('/users/edit/{user}', [UsersController::class, 'edit'])->name('admin.users.edit');
			Route::post('/users/update/{user}', [UsersController::class, 'update'])->name('admin.users.update');
			//delete
			Route::delete('/users/{user}', [UsersController::class, 'destroy'])->name('admin.users.destroy');
			
			//difficulty
			Route::resource('/difficulty', App\Http\Controllers\Admin\DifficultyController::class, ['as' => 'admin']);
			
			//instrumentation
			Route::resource('/instrumentation', App\Http\Controllers\Admin\InstrumentationController::class, ['as' => 'admin']);
			
			//composer
			Route::resource('/composer', App\Http\Controllers\Admin\ComposerController::class, ['as' => 'admin']);
			
			//windarranger
			Route::resource('/wind-arranger', App\Http\Controllers\Admin\WindArrangerController::class, ['as' => 'admin']);
			
			//percussionarranger
			Route::resource('/percussion-arranger', App\Http\Controllers\Admin\PercussionArrangerController::class, ['as' => 'admin']);
			
			
			//uploads
			Route::post('/upload', [UploadController::class, 'upload'])->name('admin.upload');
			
			//Arrangements
			Route::resource('/arrangements', App\Http\Controllers\Admin\ArrangementController::class, ['as' => 'admin']);
			
			//orders
			Route::get('/orders', [OrderController::class, 'index'])->name('admin.orders.index');
			Route::get('/orders/{order}', [OrderController::class, 'show'])->name('admin.orders.show');
			
			//admin.mapOfPurchase
			Route::get('/map-of-purchase', [OrderController::class, 'mapOfPurchase'])->name('admin.mapOfPurchase');
			
			//purchase_orders
			Route::get('/purchase-orders', [OrderController::class, 'ordersPurchase'])->name('admin.purchase_orders.index');
			Route::get('/purchase-orders/{order}/invoice', [OrderController::class, 'ordersPurchaseInvoice'])->name('admin.purchase_orders.invoice');
			Route::post('/purchase-orders/generate', [OrderController::class, 'ordersPurchaseGenerate'])->name('admin.purchase_orders.generate');
			Route::post('/purchase-orders/payment_completed', [OrderController::class, 'ordersPurchasePaymentComplete'])->name('admin.purchase_orders.payment_completed');
			Route::get('/purchase-orders/{order}', [OrderController::class, 'ordersPurchaseShow'])->name('admin.purchase_orders.show');
			
			//promocode
			Route::resource('/promocode', App\Http\Controllers\Admin\PromoCodeController::class, ['as' => 'admin']);
			
			//cancelPayment
			Route::post('/paymentCancel', [PaymentController::class, 'paymentCancel'])->name('paymentCancel');
			
			//arrangements type
			Route::resource('/arrangement-type', App\Http\Controllers\Admin\ArrangementTypeController::class, ['as' => 'admin']);
			
			//states
			Route::resource('/states', App\Http\Controllers\Admin\StatesController::class, ['as' => 'admin'])->only('index', 'update');


		});
	});
});








