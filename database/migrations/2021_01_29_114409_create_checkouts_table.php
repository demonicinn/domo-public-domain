<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCheckoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checkouts', function (Blueprint $table) {
			$table->increments('id');
			
			$table->integer('user_id')->nullable();
			$table->string('guest_uid')->nullable();
			$table->enum('type', ['wind', 'percussion'])->default('wind');
			
			$table->string('zipcode', 10)->nullable();
			$table->text('latitude')->nullable();
			$table->text('longitude')->nullable();
			$table->text('address')->nullable();
			$table->string('city')->nullable();
			$table->string('state')->nullable();
			$table->text('instructions')->nullable();
			
			$table->string('promocode')->nullable();
			$table->string('promocode_type')->nullable();
			$table->string('discount')->nullable();			
			
			$table->decimal('tax', 8,2)->nullable();
			$table->decimal('price', 8,2)->nullable();
			$table->decimal('total_price', 8,2)->nullable();
			$table->enum('invoice_generated', ['0', '1'])->default('0');
			$table->enum('is_purchase_orders', ['0', '1'])->default('0');
			$table->enum('status', ['cart', 'paid', 'purchase_orders'])->default('cart');
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checkouts');
    }
}
