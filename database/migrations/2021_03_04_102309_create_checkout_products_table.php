<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCheckoutProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checkout_products', function (Blueprint $table) {
            $table->id();
			
			$table->integer('checkouts_id')->unsigned();
            $table->foreign('checkouts_id')->references('id')->on('checkouts')->onDelete('cascade');
			
			$table->integer('arrangements_id')->unsigned();
            $table->foreign('arrangements_id')->references('id')->on('arrangements')->onDelete('cascade');
			
			$table->enum('arrangementtype', ['wind_arrangment', 'percussion_arrangment'])->default('wind_arrangment');
			
			$table->decimal('price', 8,2)->nullable();
			
            $table->timestamps();
        });
    }
	
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checkout_products');
    }
}
