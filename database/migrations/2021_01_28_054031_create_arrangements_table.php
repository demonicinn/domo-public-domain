<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArrangementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('arrangements', function (Blueprint $table) {
			$table->increments('id');
			$table->string('title', 150)->unique();
			$table->string('slug', 150)->nullable();
			$table->text('description')->nullable();
			
			$table->integer('difficulty')->nullable();
			$table->integer('instrumentation')->nullable();
			$table->integer('composer')->nullable();
			$table->integer('windarranger')->nullable();
			$table->integer('percussionarranger')->nullable();
			$table->integer('arrangementtype')->nullable();
			
			$table->date('date')->nullable();
			$table->text('image')->nullable();
			$table->decimal('wind_arrangment', 8, 2)->nullable();
			$table->text('wpdf')->nullable();
			$table->text('wmp3')->nullable();
			$table->decimal('percussion_arrangment', 8, 2)->nullable();
			$table->text('wppdf')->nullable();
			$table->text('wpmp3')->nullable();
			$table->enum('is_featured', ['0', '1'])->default('0');
			$table->enum('status', ['0', '1'])->default('0');
			$table->enum('is_delete', ['0', '1'])->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('arrangements');
		}
}
