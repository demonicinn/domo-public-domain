<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
			$table->enum('role', ['admin', 'user']);
            $table->string('first_name', 100);
            $table->string('last_name', 100);
			$table->string('email', 100)->unique();
            $table->string('zipcode', 10)->nullable();
            $table->string('school', 255)->nullable();
            $table->string('phone_number', 20)->nullable();
			$table->string('customer_id', 30)->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
			
			$table->enum('status', ['0', '1', '2'])->default('1');
			$table->enum('is_delete', ['0', '1'])->default('0');
			
            $table->rememberToken();
            $table->timestamps();
        });
		
		///insert admin
		DB::table('users')->insert([
			'role' => 'admin',
			'first_name' => 'Admin',
			'last_name' => 'YP',
			'email' => 'admin@yopmail.com',
			'email_verified_at' => date('Y-m-d H:i:s'),
			'password' => Hash::make('123456')
		]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
