
/**
*
* send ajax requests
*/
function ajaxRequest(formData, postUrl, callback) {
	var res;
	jQuery.ajax({
        headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },
        url: postUrl,
        type: 'POST',
        data : formData,
        contentType: false,
        cache : false,
        processData: false,
        success: function (response) {
        	res = response;
        	callback(res);
        }
    });
}


jQuery(document).ajaxStart(function() {
	jQuery('#loading').html('<div class="loaders"></div>');
});

jQuery(document).ajaxStop(function(){
	jQuery('#loading').html('');
});